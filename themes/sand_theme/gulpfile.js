var gulp = require("gulp"),
    babel = require("gulp-babel"),
    uglify = require("gulp-uglify"),
    rename = require("gulp-rename");

gulp.task("default", ["watch"]);

gulp.task("watch", function () {
    gulp.watch("./js/expand/*.js", ["minify"]);
    gulp.watch("./js/expand/libraries/*.js", ["minify"]);
});

gulp.task("minify", function () {
    gulp.src([
        "./js/expand/*.js",
        "./js/expand/libraries/*.js"
    ])
        .pipe(babel({
            presets:["es2015"]
        }))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest("js/"));
});
