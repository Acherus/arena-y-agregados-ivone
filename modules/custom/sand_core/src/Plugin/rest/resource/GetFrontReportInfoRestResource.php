<?php

namespace Drupal\sand_core\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\sand_core\Services\AuditServices;
use Drupal\Component\Serialization\Json;
use Drupal\sand_core\Services\ReportsServices;
use Drupal\rest\ResourceResponse;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;

/**
 * Provides a resource to get information
 *
 * @RestResource(
 *   id = "get_front_reports",
 *   label = @Translation("Get front reports data"),
 *   uri_paths = {
 *     "canonical" = "/api/core/get/front-reports/{reportType}",
 *     "https://www.drupal.org/link-relations/create" = "/api/core/get/data/{reportType}"
 *   }
 * )
 */
class GetFrontReportInfoRestResource extends ResourceBase {

  const GET_HISTORY = 'getHistory';
  const GET_REPORTS = 'getReports';
  const GET_MATERIALS = 'getMaterials';
  const BUILD_REPORT_XLS = 'buildReportXLS';
  const BUILD_REPORT_CSV = 'buildReportCSV';

  /**
   * Get generate report service.
   * 
   * @var AuditServices $generate
   */
  protected $auditService;

  /**
   * Get reports services.
   * 
   * @var ReportsServices $reportsService
   */
  protected $reportsService;

  /**
   * HTTP Status code.
   * 
   * @var int $status
   */
  protected $status;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param Drupal\express_packets\Services\ExpressPacketsServiceInterface $expressPacketsService
   *   Express packets service.
   */
  public function __construct(array $configuration, 
                              $plugin_id, 
                              $plugin_definition, 
                              array $serializer_formats, 
                              LoggerInterface $logger, 
                              AuditServices $auditService,
                              ReportsServices $reportsService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->auditService = $auditService;
    $this->reportsService = $reportsService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('tol'),
      $container->get('sand_core.audit_services'),
      $container->get('sand_core.repot_query')
    );
  }

  public function get(Request $request, $reportType) {
    try {
      $filters = $request->query->get('filter') 
        ? (array) json_decode(base64_decode(urldecode($request->query->get('filter'))))
        : [];
      $page = $request->query->get('page');
      \Drupal::logger('filterss')->notice(print_r([$filters, $request->query->get('filter'), urldecode($request->query->get('filter')), base64_decode(urldecode($request->query->get('filter'))) ], 1));
      $this->status = Response::HTTP_OK;

      switch ($reportType) {
        case self::GET_MATERIALS:
          $materials = $this->reportsService->getMaterials();
          $response = ["" => ""];
          foreach ($materials as $key => $value) {
            $response[$value->material] = $value->material;
          }
          break;
        default:
          $this->status = Response::HTTP_NOT_FOUND;
          $response = [
            'status' => Response::HTTP_NOT_FOUND,
            'message' => 'not data'
          ];
          break;
      }
    }
    catch(\Exception $e) {
      $this->status = Response::HTTP_INTERNAL_SERVER_ERROR;
      $response = [
        'status' => $e->getCode(),
        'message' => 'not data'
      ];
    }

		return new Response(
      Json::encode($response),
      $this->status
    );
  }

  public function post($reportType, $data, Request $request) {
    $this->status = Response::HTTP_OK;

    if ($data == null) $data = [];

    try {
      \Drupal::logger('filterss post')->notice(print_r([$reportType, $data], 1));
      $page = $request->query->get('page');

      switch ($reportType) {
        case self::BUILD_REPORT_CSV:
          $filters = !empty($data) ? urlencode(base64_encode((json_encode($data)))) : '';
          $route = Url::fromRoute('sand_core.download_report', [
            'filter' => $filters,
            'type' => 'csv',
          ])->toString();

          $response = [
            'route' => $route,
          ];
          \Drupal::logger('route')->notice(print_r($response, 1));
          break;
        case self::BUILD_REPORT_XLS:
          $filters = !empty($data) ? urlencode(base64_encode((json_encode($data)))) : '';
          $route = Url::fromRoute('sand_core.download_report', [
            'filter' => isset($filters) ? $filters : '',
          ])->toString();
          $response = [
            'route' => $route,
          ];
          break;
        case self::GET_HISTORY: 
          $response = $this->auditService->getHistoryData($data, $page);
          break;
        case self::GET_REPORTS:
          $response = $this->reportsService->getDataToShow($data, $page);
          break;
        default:
          $this->status = Response::HTTP_NOT_FOUND;
          $response = [
            'status' => Response::HTTP_NOT_FOUND,
            'message' => 'not data'
          ];
          break;
      }
    }
    catch (\Exception $e) {
      $this->status = Response::HTTP_INTERNAL_SERVER_ERROR;
      $response = [
        'status' => $e->getCode(),
        'message' => $e->getMessage(),
      ];
    }
    // No caché.
    $response = new ModifiedResourceResponse($response, $this->status);

    return $response;
  }

}
