<?php

namespace Drupal\sand_core\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\sand_core\Services\GenerateReportService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides a resource to get validation information from msisdn.
 *
 * @RestResource(
 *   id = "generate_report_rest_resource",
 *   label = @Translation("Generate report rest resource"),
 *   uri_paths = {
 *     "https://www.drupal.org/link-relations/create" = "/api/core/generate-report"
 *   }
 * )
 */
class GenrateReportRestResource extends ResourceBase {

  /**
   * Get generate report service.
   * 
   * @var GenerateReportService $generate
   */
  protected $generate;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param Drupal\express_packets\Services\ExpressPacketsServiceInterface $expressPacketsService
   *   Express packets service.
   */
  public function __construct(array $configuration, 
                              $plugin_id, 
                              $plugin_definition, 
                              array $serializer_formats, 
                              LoggerInterface $logger, 
                              GenerateReportService $generate) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->generate = $generate;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('tol'),
      $container->get('sand_core.generate_report')
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param string $msisdn
   *   Msisdn.
   * @param string $package
   *   Package type.
   *
   * Returns validation information from msisdn.
   */
  public function post($payload) {
    try {
      $report = $file = "";
      switch($payload['report']) {
        case "report_by_client":
          $report = 'getReportByClient';
          break;
        case "report_by_material":
          $report = "getReportByMaterialAndMonth";
          break;
        case "report_by_advance":
          $report = "getReportByAdvance";
          break;
      }
      $file = $this->generate->{$report}($payload['filter'], $payload['type']);
      $pos = strpos($file, 'sites');
      $file = substr($file, $pos-1);
    }
    catch(\Exception $e) {
    }

    $cacheMetadata = new CacheableMetadata();

    // No caché.
    $cacheMetadata->setCacheMaxAge(0);
    $response = new ResourceResponse(['file' => $file]);
    $response->addCacheableDependency($cacheMetadata);

    return $response;
  }

}
