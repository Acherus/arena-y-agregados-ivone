<?php

namespace Drupal\sand_core\Plugin\Block;

use Drupal\Core\ {
  Block\BlockBase,
  Form\FormBuilder,
  Plugin\ContainerFactoryPluginInterface,
  Session\AccountInterface,
  Routing\CurrentRouteMatch,
  Controller\TitleResolver
};

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 *
 * @Block(
 *   id = "header_simple",
 *   admin_label = @Translation("header simple")
 * )
 */
class HeaderBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Generate forms and load that forms in a variable to render after.
   * 
   * @var FormBuilder $formBuilder
   */
  protected $formBuilder;

  /**
   * Get user interface to validate if the user is logged or not.
   * 
   * @var AccountInterface $user
   */
  protected $user;

  protected $request;

  protected $routeMatch;

  protected $titleResolver;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, 
                              $plugin_id, 
                              $plugin_definition, 
                              FormBuilder $form_builder,
                              AccountInterface $user,
                              RequestStack $request,
                              CurrentRouteMatch $routeMatch,
                              TitleResolver $titleResolver) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->user = $user;
    $this->request = $request->getCurrentRequest();
    $this->routeMatch = $routeMatch;
    $this->titleResolver = $titleResolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, 
                                array $configuration, 
                                $plugin_id, 
                                $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('current_user'),
      $container->get('request_stack'),
      $container->get('current_route_match'),
      $container->get('title_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Validate if user is logged.
    $userName = $userLogin = $pageTitle = NULL;

    if ($this->user->isAnonymous()) {
      $userLogin = $this->formBuilder->getForm('Drupal\sand_core\Form\UserLoginExtendForm');
    }
    else {
      $pageTitle = $this->titleResolver->getTitle($this->request, $this->routeMatch->getRouteObject());
      $userName = $this->user->getAccountName();
    }

    return [
      '#theme' => 'header-simple',
      '#user_login_form' => $userLogin,
      '#isAnonymous' => $this->user->isAnonymous(),
      '#userName' => $userName,
      '#pageTitle' => $pageTitle,
      '#attached' => [
        'library' => [
          'sand_core/sand_core',
        ],
      ],
    ];
  }

  public function getCacheMaxAge() {
    return 0;
  }
}
