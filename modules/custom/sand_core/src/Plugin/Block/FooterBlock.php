<?php

namespace Drupal\sand_core\Plugin\Block;

use Drupal\Core\ {
  Block\BlockBase,
  Plugin\ContainerFactoryPluginInterface,
  Session\AccountInterface,
  Path\CurrentPathStack
};
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @Block(
 *   id = "footer_simple",
 *   admin_label = @Translation("footer simple")
 * )
 */
class FooterBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Generate forms and load that forms in a variable to render after.
   * 
   * @var FormBuilder $formBuilder
   */
  protected $formBuilder;

  /**
   * Get user interface to validate if the user is logged or not.
   * 
   * @var AccountInterface $user
   */
  protected $user;

  /**
   * Get path stack.
   * 
   * @var CurrentPathStack $path
   */
  protected $path;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, 
                              $plugin_id, 
                              $plugin_definition, 
                              AccountInterface $user,
                              CurrentPathStack $path) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->user = $user;
    $this->path = $path;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, 
                                array $configuration, 
                                $plugin_id, 
                                $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('path.current')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $class = 'middle-footer';
    if ($this->path->getPath() != '/home-sand') {
      $class = 'full-footer';
    }
    return [
      '#theme' => 'footer-simple',
      '#class' => $this->user->isAnonymous() ? 'full-footer' : $class,
      '#attached' => [
        'library' => [
          'sand_core/sand_core',
        ],
      ],
      '#cache' =>  [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }
}
