<?php

namespace Drupal\sand_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 *
 * @Block(
 *   id = "menu_logged-simple",
 *   admin_label = @Translation("menu logged simple")
 * )
 */
class LoggedMenuBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Generate forms and load that forms in a variable to render after.
   * 
   * @var FormBuilder $formBuilder
   */
  protected $formBuilder;

  /**
   * Get user interface to validate if the user is logged or not.
   * 
   * @var AccountInterface $user
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, 
                              $plugin_id, 
                              $plugin_definition, 
                              FormBuilder $form_builder,
                              AccountInterface $user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->user = $user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, 
                                array $configuration, 
                                $plugin_id, 
                                $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if ($this->user->hasPermission('despachos menu')) {
      $menu['out'] = [
        'label' => 'Despachos',
        'path' => Url::fromRoute('sand_core.salidas')->toString(),
      ];
    }
    if ($this->user->hasPermission('clientes menu')) {
      $menu['clients'] = [
        'label' => 'Clientes',
        'path' => Url::fromRoute('sand_core.clients_list')->toString(),
      ];
    }
    if ($this->user->hasPermission('anticipos menu')) {
      $menu['payment'] = [
        'label' => 'Anticipos',
        'path' => Url::fromRoute('sand_core.pagos')->toString(),
      ];
    }
    if ($this->user->hasPermission('materiales menu')) {
      $menu['material'] = [
        'label' => 'Materiales',
        'path' => Url::fromRoute('sand_core.material_list')->toString(),
      ];
    }
    if ($this->user->hasPermission('reportes menu')) {
      $menu['reports'] = [
        'label' => 'Reportes',
        'path' => Url::fromRoute('sand_core.home_reports')->toString(),
      ];
    }
    if ($this->user->hasPermission('control menu')) {
      $menu['control'] = [
        'label' => 'Control',
        'path' => Url::fromRoute('sand_core.logs_list')->toString(),
        'class' => 'brder-bottom',
      ];
    }

    if ($this->user->hasPermission('history menu')) {
      $menu['historial'] = [
        'label' => 'Historial de certificados',
        'path' => Url::fromRoute('sand_core.history_form')->toString(),
        'class' => 'brder-bottom',
      ];
    }

    if ($this->user->hasPermission('Modify payment view')) {
      $menu['modify_payment'] = [
        'label' => 'Modificar pago',
        'path' => Url::fromRoute('sand_core.modify_payment')->toString(),
        'class' => 'brder-bottom',
      ];
    }

    $logOut = [
      '#type' => 'link',
      '#title' => 'Cerrar sesión',
      '#url' => Url::fromRoute('user.logout'),
    ];

    return [
      '#theme' => 'menu-logged-simple',
      '#isAnonymous' => $this->user->isAnonymous(),
      '#userName' => $this->user->getAccountName(),
      '#menuElements' => $menu,
      '#logOut' => $logOut,
      '#attached' => [
        'library' => [
          'sand_core/sand_core',
        ],
      ],
    ];
  }

  /**
   * @return int 0 | Max cache age.
   */
  public function getCacheMaxAge() {
    return 0;
  }
}
