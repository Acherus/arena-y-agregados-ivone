<?php

namespace Drupal\sand_core\Form;

use Drupal\user\Form\UserLoginForm as UserLoginBase;
use Drupal\Core\Form\FormStateInterface;

class UserLoginExtendForm extends UserLoginBase {

  public function getFormId() {
    return 'user_login_extend_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form = parent::buildForm($form, $form_state);

    $form['name']['#title'] = 'Usuario';
    $form['pass']['#title'] = 'Clave';

    $form['close'] = [
      '#markup' => '<span class="close-pop-up"></span>'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_state->setRedirect('<front>');
  }

}