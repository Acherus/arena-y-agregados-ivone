<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Drupal\sand_core\Services\AuditServices;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MaterialesForm extends FormBase {

  // Define data base table name;
  const MATERIALES = 'core_materiales';

  /**
   * Connection to data base.
   * 
   * @var Connection $connection
   */
  protected $connection;

  /**
   * @var AuditServices $audit
   */
  protected $audit;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, AuditServices $audit) {
    $this->connection = $connection;
    $this->audit = $audit;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('sand_core.audit_services')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sand_core_materiales';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateinterface $form_state) {
    // Validate if is a material change or new material.
    try {
      if (isset($_GET['material'])) {
        // Get configuration data;
        $data = $this->connection->select(self::MATERIALES, 'cm')
          ->fields('cm')
          ->condition('id', $_GET['material'], '=')
          ->execute()
          ->fetch();
      }
      else {
        $data = [];
      }
    }
    catch(\Exception $e) {
      $data = [];
    }

    $form = [];
    $form['#tree'] = TRUE;

    $form['returnContainer'] = [
      '#type' => 'container',
      '#id' => 'return-container',
    ];

    $form['returnContainer']['linkReturn'] = [
      '#type' => 'link',
      '#title' => $this->t('Lista de materiales'),
      '#url' => Url::fromRoute('sand_core.material_list'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
        ],
      ],
    ];

    // Define element render.
    $form['data'] = [
      'id' => [
        '#type' => 'hidden',
        '#value' => is_array($data)
          ? 'new' : $data->id,
      ],
      'material' => [
        '#type' => 'textfield',
        '#title' => $this->t('Material'),
        '#default_value' => is_array($data)
          ? '' : $data->material,
      ],
      'valor_unitario' => [
        '#type' => 'textfield',
        '#title' => $this->t('Valor unitario'),
        '#default_value' => is_array($data)
          ? '' : $data->valor_unitario,
      ],
      'val_alter' => [
        '#type' => 'textfield',
        '#title' => $this->t('Valor alternativo'),
        '#default_value' => is_array($data)
          ? '' : $data->val_alter,
      ],
    ];

    // Form submit button.
    $form['actions']['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Guardar'),
      '#attributes' => [
        'class' => [
          'btn-basic',
        ]
      ]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateinterface $form_state) {
    // Get form information.
    $element = $form_state->getValue('data');

    // Save data.
    try {
      if ($element['id'] == 'new') {
        // Save new data.
        unset($element['id']);
        $element['material'] = strtoupper($element['material']);
        $value = $this->connection->insert(self::MATERIALES)
          ->fields($element)
          ->execute();

        $form_state->setRedirect('sand_core.materiales', [
          'material' => $value
        ]);
        $this->audit->pushLog('insert', 'Se crea el material '. $element['material'], 'create_material');
      }
      else {
        // Update old data.
        $id = $element['id'];
        unset($element['id']);
        $this->connection->update(self::MATERIALES)
          ->fields($element)
          ->condition('id', $id, '=')
          ->execute();

        $this->audit->pushLog('update', 'Se actualiza el material '. $element['material'], 'update_material');        
      }
      drupal_set_message('Información guardada.');
    }
    catch (\Exception $e) {
      drupal_set_message('Error al  guardar la información.', 'error');
    }

    $form_state->setRedirect('sand_core.material_list');
  }

}