<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\ {
  FormBase,
  FormStateInterface
};
use Drupal\Core\Url;
use Drupal\Core\Session\AccountInterface;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RemisionForm extends FormBase {

  /**
   * Get user interface to validate if the user is logged or not.
   * 
   * @var AccountInterface $user
   */
  protected $user;

  public function __construct(AccountInterface $user) {
    $this->user = $user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
       $container->get('current_user')
    );
  }

  public function getFormId() {
    return 'sand_core_remision_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $form = $data = [];
    $form['#tree'] = TRUE;


    $tempstore = \Drupal::service('tempstore.private');
    $store = $tempstore->get('toSave');

    if (isset($_GET['data'])) {
      $data = (array) json_decode(base64_decode(urldecode($_GET['data'])));
      if (!isset($data['review'])) {
        $store->set('remision', $data);
      }
    }

    $form['returnContainer'] = [
      '#type' => 'container',
      '#id' => 'return-container',
    ];

    if (isset($_GET['history'])) {
      $form['returnContainer']['linkReturn'] = [
        '#type' => 'link',
        '#title' => $this->t('Historial'),
        '#url' => Url::fromRoute('sand_core.history_form'),
        '#attributes' => [
          'class' => [
            'btn-basic',
            'list-table',
          ],
        ],
      ];
    }
    else {
      $form['returnContainer']['linkReturn'] = [
        '#type' => 'link',
        '#title' => $this->t('Certificado ANM'),
        '#url' => Url::fromRoute('sand_core.cert_origin', ['data' => $_GET['data'], 'status' => 1]),
        '#attributes' => [
          'class' => [
            'btn-basic',
            'list-table',
          ],
        ],
      ];
    }

    if (isset($data['review']) && $data['review'] == 1) {
      $form['returnContainer']['print'] = [
        '#type' => 'label',
        '#title' => $this->t('Imprimir'),
        '#attributes' => [
          'class' => [
            'btn-basic',
            'list-table',
          ],
          'id' => 'print-cert-btn',
          'onclick' => 'javascript:window.print();',
        ],
      ];
    }
    else {
      $form['returnContainer']['print'] = [
        '#type' => 'submit',
        '#value' => $this->t('Imprimir'),
        '#attributes' => [
          'class' => [
            'btn-basic',
            'list-table',
          ],
          'id' => 'print-cert-btn',
          'onclick' => 'javascript:window.print();',
        ],
        '#submit' => [[$this, 'saveData']],
      ];
    }

    $form['wrapper'] = [
      '#type' => 'container',
      '#id' => 'print-wrapper',
    ];

    if (isset($_GET['status']) && $_GET['status'] == 0) {
      $form['wrapper']['disabled'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['disabled-mark','dis-remision'],
        ],
      ];
      $form['wrapper']['disabled']['anulado'] = [
        '#type' => 'label',
        '#title' => 'ANULADO',
        '#attributes' => [
          'class' => ['anulado'],
        ],
      ];
    }

    $form['wrapper']['head'] = [
      '#type' => 'container',
      '#id' => 'header-print',
    ];

    $form['wrapper']['head']['left'] = [
      '#type' => 'container',
      '#id' => 'left-head',
    ];

    $form['wrapper']['head']['left']['logo'] = [
      '#markup' => '<span class="logo"></span>'
    ];

    $form['wrapper']['head']['left']['nit'] = [
      '#type' => 'label',
      '#title' => $this->t('NIT. 900.801.033-5'),
    ];

    $form['wrapper']['head']['rigth'] = [
      '#type' => 'container',
      '#id' => 'rigth-head',
    ];

    $form['wrapper']['head']['rigth']['remision'] = [
      '#type' => 'label',
      '#title' => $this->t('REMISIÓN No'),
    ];

    $form['wrapper']['head']['rigth']['number-wrapper'] = [
      '#type' => 'container',
      '#id' => 'number-wrapper',
    ];

    $form['wrapper']['head']['rigth']['number-wrapper']['label'] = [
      '#type' => 'label',
      '#title' => isset($data['remision']) ? $data['remision'] : '',
    ];

    $form['wrapper']['head']['down'] = [
      '#type' => 'container',
      '#id' => 'down-head',
    ];

    $form['wrapper']['head']['down']['address'] = [
      '#type' => 'label',
      '#title' => $this->t('Vereda Fusungá, Kilometro 20 - Soacha - Cundinamarca'),
    ];

    $form['wrapper']['head']['down']['phones'] = [
      '#type' => 'label',
      '#title' => $this->t('Teléfonos: 6225492 - 6226133 - 3185896381'),
    ];

    $form['wrapper']['table'] = [
      '#type' => 'table',
      '#id' => 'remision-table',
    ];

    $form['wrapper']['table']['first']['dateLabel'] = [
      '#type' => 'label',
      '#title' => $this->t('Fecha:'),
    ];

    $date = isset($data['fecha']) 
      ? explode('-', $data['fecha'])
      : [];
    $form['wrapper']['table']['first']['day'] = [
      '#type' => 'number',
      '#attributes' => [
        'placeholder' => 'DD',
      ],
      '#min' => 1,
      '#max' => 31,
      '#id' => 'day',
      '#default_value' => isset($date[2]) ? $date[2] : '',
      '#wrapper_attributes' => [
        'class' => [
          'day-wrapper'
        ]
      ],
    ];

    $form['wrapper']['table']['first']['month'] = [
      '#type' => 'number',
      '#attributes' => [
        'placeholder' => 'MM',
      ],
      '#min' => 1,
      '#max' => 12,
      '#id' => 'month',
      '#default_value' => isset($date[1]) ? $date[1] : '',
      '#wrapper_attributes' => [
        'class' => [
          'month-wrapper'
        ]
      ],
    ];

    $form['wrapper']['table']['first']['year'] = [
      '#type' => 'number',
      '#attributes' => [
        'placeholder' => 'AAAA',
      ],
      '#id' => 'year',
      '#min' => 1990,
      '#max' => 2090,
      '#default_value' => isset($date[0]) ? $date[0] : '',
      '#wrapper_attributes' => [
        'class' => [
          'year-wrapper'
        ]
      ],
    ];

    $form['wrapper']['table']['mid_first']['placaWrapper'] = [
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => ['placa-wrapper']
      ],
    ];

    $form['wrapper']['table']['mid_first']['placaWrapper']['placaLabel'] = [
      '#type' => 'label',
      '#title' => $this->t('Placa:'),
    ];

    $form['wrapper']['table']['mid_first']['placaWrapper']['placa'] = [
      '#type' => 'textfield',
      '#size' => 10,
      '#wrapper_attributes' => [
        'class' => [
          'placa-wrapper',
          'input-wrapper',
        ],
      ],
      '#default_value' => isset($data['placa']) 
        ? $data['placa'] : ''
    ];

    $form['wrapper']['table']['second']['rz_wrapper'] = [
      '#wrapper_attributes' => [
        'colspan' => 5,
      ],
    ];

    $form['wrapper']['table']['second']['rz_wrapper']['razon_social'] = [
      '#type' => 'label',
      '#title' => $this->t('Razón social:'),
    ];

    $form['wrapper']['table']['second']['rz_wrapper']['rz_value'] = [
      '#type' => 'textfield',
      '#wrapper_attributes' => [
        'class' => [
          'input-wrapper',
        ],
      ],
      '#default_value' => isset($data['razonSocial']) 
        ? $data['razonSocial'] : '',
      '#attributes' => [
        'class' => [
          'rz-input',
        ],
      ],
    ];

    $form['wrapper']['table']['third']['conductor_wrapper'] = [
      '#wrapper_attributes' => [
        'colspan' => 5,
      ],
    ];

    $form['wrapper']['table']['third']['conductor_wrapper']['conductorLabel'] = [
      '#type' => 'label',
      '#title' => $this->t('Nombre del conductor:'),
    ];

    $form['wrapper']['table']['third']['conductor_wrapper']['conductor'] = [
      '#type' => 'textfield',
      '#wrapper_attributes' => [
        'class' => [
          'input-wrapper',
        ],
      ],
      '#default_value' => isset($data['conductor']) 
        ? $data['conductor'] : '',
      '#attributes' => [
        'class' => [
          'conductor-input',
        ],
      ],
    ];


    $form['wrapper']['table']['fourth']['m_wrapper'] = [
      '#wrapper_attributes' => [
        'colspan' => 5,
      ],
    ];

    $form['wrapper']['table']['fourth']['m_wrapper']['materialLabel'] = [
      '#type' => 'label',
      '#title' => $this->t('Tipo de material:'),
    ];

    $form['wrapper']['table']['fourth']['m_wrapper']['material'] = [
      '#type' => 'textfield',
      '#wrapper_attributes' => [
        'class' => [
          'input-wrapper',
        ],
      ],
      '#attributes' => [
        'class' => [
          'material-input',
        ],
      ],
      '#default_value' => isset($data['material']) 
        ? $data['material'] : '',
    ];

    $form['wrapper']['table']['fifth']['cantidad_wrapper'] = [
      '#wrapper_attributes' => [
        'colspan' => 5,
      ],
    ];

    $form['wrapper']['table']['fifth']['cantidad_wrapper']['cantidadLabel'] = [
      '#type' => 'label',
      '#title' => $this->t('Volumen(m&#x00B3):'),
    ];

    $form['wrapper']['table']['fifth']['cantidad_wrapper']['cantidad'] = [
      '#type' => 'number',
      '#wrapper_attributes' => [
        'class' => [
          'input-wrapper',
        ],
      ],
      '#attributes' => [
        'class' => [
          'cantidad-input',
        ],
      ],
      '#default_value' => isset($data['metros']) 
        ? $data['metros'] : '',
    ];

    $form['wrapper']['table']['sixth']['dispath_wrapper'] = [
      '#wrapper_attributes' => [
        'colspan' => 5,
      ],
    ];
    
    $form['wrapper']['table']['sixth']['dispath_wrapper']['dipathByLabel'] = [
      '#type' => 'label',
      '#title' => $this->t('Despachado por:'),
    ];

    $form['wrapper']['table']['sixth']['dispath_wrapper']['dispathValue'] = [
      '#type' => 'textfield',
      '#wrapper_attributes' => [
        'class' => [
          'input-wrapper',
        ],
      ],
      '#attributes' => [
        'class' => [
          'dispath-input',
        ],
      ],
    ];

    if (isset($data['despachado'])) {
      $form['wrapper']['table']['sixth']['dispath_wrapper']['dispathValue']['#default_value'] = $data['despachado'];
    }
    else {
      $form['wrapper']['table']['sixth']['dispath_wrapper']['dispathValue']['#default_value'] = $this->user->getAccountName();
    }

    $form['wrapper']['table']['seventh']['sign_wrapper'] = [
      '#wrapper_attributes' => [
        'colspan' => 5,
      ],
    ];

    $form['wrapper']['table']['seventh']['sign_wrapper']['signLabel'] = [
      '#type' => 'label',
      '#title' => $this->t('Firma:'),
      '#attributes' => [
        'class' => [
          'sign-label',
        ],
      ],
    ];

    $config = \Drupal::config('sand_core.config');
    $imgConfg = $config->get('signs_wrapper');
    $user = \Drupal::currentUser()->id();
    if (isset($imgConfg[$user]) 
        && !empty($imgConfg[$user]['image'][0])
        && !isset($data['sign_remision'])) {
      if ($file = File::load($imgConfg[$user]['image'][0])) {
        $store->set('sign_remision', $file->getFileUri());
        $form['wrapper']['table']['seventh']['sign_wrapper']['signValue'] = [
          '#default_value' => '',
          '#wrapper_attributes' => [
            'colspan' => 4,
            'class' => [
              'input-white',
              'sign-img',
            ],
          ],
          '#theme' => 'image',
          '#style_name' => 'medium',
          '#uri' => $file->getFileUri(),
        ];
      }
    }
    elseif (isset($data['sign_remision'])) {
      $form['wrapper']['table']['seventh']['sign_wrapper']['signValue'] = [
        '#default_value' => '',
        '#wrapper_attributes' => [
          'colspan' => 4,
          'class' => [
            'input-white',
            'sign-img',
          ],
        ],
        '#theme' => 'image',
        '#style_name' => 'medium',
        '#uri' => $data['sign_remision'],
      ];
    }
    else {
      $form['wrapper']['table']['seventh']['sign_wrapper']['signValue'] = [
        '#type' => 'label',
        '#title'=> $this->t(''),
        '#attributes' => [
          'class' => [
            'input-wrapper',
            'sign',
          ],
        ],
      ];
    }

    $form['wrapper']['table']['eighth']['total_wrapper'] = [
      '#wrapper_attributes' => [
        'colspan' => 5,
      ],
    ];

    $form['wrapper']['table']['eighth']['total_wrapper']['totalLabel'] = [
      '#type' => 'label',
      '#title' => $this->t('Valor total:'),
    ];

    $form['wrapper']['table']['eighth']['total_wrapper']['total'] = [
      '#type' => 'number',
      '#wrapper_attributes' => [
        'class' => [
          'input-wrapper',
        ],
      ],
      '#attributes' => [
        'class' => [
          'total-input',
        ],
      ],
      '#default_value' => isset($data['valor']) 
        ? $data['valor'] : '',
    ];

    $form['wrapper']['down_ptr'] = [
      '#type' => 'container',
      '#id' => 'down-head-ptr',
    ];

    $form['wrapper']['down_ptr']['address'] = [
      '#type' => 'label',
      '#title' => $this->t('Vereda Fusungá, Kilometro 20 - Soacha - Cundinamarca'),
    ];

    $form['wrapper']['down_ptr']['phones'] = [
      '#type' => 'label',
      '#title' => $this->t('Teléfonos: 6225492 - 6226133 - 3185896381'),
    ];

    return $form;
  }

  /**
   * Save Data.
   */
  public function saveData(array &$form, FormStateInterface $form_state) {
    $tempstore = \Drupal::service('tempstore.private');
    $store = $tempstore->get('toSave');
    $db = \Drupal::service('database');
    $fields = [];
    $toSave = [];

    $year = $form_state->getValue(['wrapper', 'table', 'first', 'year']);
    $month = $form_state->getValue(['wrapper', 'table', 'first', 'month']);
    $day = $form_state->getValue(['wrapper', 'table', 'first', 'day']);
    $date = "$year-$month-$day";

    try {
      $fields['id_reg'] = $store->get('remision')['remision'];
      $toSave = [
        'remision' => $store->get('remision')['remision'],
        'sign_remision' => $store->get('sign_remision'),
        'fecha' => $date,
        'placa' => strtoupper($form_state->getValue(['wrapper','table','mid_first','placaWrapper','placa'])),
        'razonSocial' => strtoupper($form_state->getValue(['wrapper','table','second','rz_wrapper','rz_value'])),
        'conductor' => strtoupper($form_state->getValue(['wrapper','table','third','conductor_wrapper','conductor'])),
        'material' => strtoupper($form_state->getValue(['wrapper','table','fourth','m_wrapper','material'])),
        'metros' => $form_state->getValue(['wrapper','table','fifth','cantidad_wrapper','cantidad']),
        'despachado' => strtoupper($form_state->getValue(['wrapper','table','sixth','dispath_wrapper','dispathValue'])),
        'valor' => $form_state->getValue(['wrapper','table','eighth','total_wrapper','total']),
      ];

      $toSave = urlencode(base64_encode(json_encode($toSave)));
      $fields['data_reg'] = $toSave;
      //$fields['date_creation'] = date('Y-m-d');

      $db->update('sand_core_cert_report_history_data')
        ->fields($fields)
        ->condition('id_reg', $store->get('remision')['remision'], '=')
        ->execute();
    }
    catch (\Exception $e) {
    }
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}