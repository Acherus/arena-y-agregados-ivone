<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ReportePorAnticipoForm extends FormBase {

  /**
   * Cache page.
   * 
   * @var KillSwitch $connection
   */
  protected $kill;

  /**
   * {@inheritdoc}
   */
  public function __construct(KillSwitch $kill) {
    $this->kill = $kill;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('page_cache_kill_switch')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sand_core_reports';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form,FormStateInterface $form_state) {
    $this->kill->trigger();
    $form = [];
    $form['#tree'] = TRUE;

    $form['returnContainer'] = [
      '#type' => 'container',
      '#id' => 'return-container',
    ];

    $form['returnContainer']['linkReturn'] = [
      '#type' => 'link',
      '#title' => $this->t('Menú de reportes'),
      '#url' => Url::fromRoute('sand_core.home_reports'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
        ],
      ],
    ];

    $form['wrapperFilters'] = [
      '#type' => 'container',
      '#id' => 'filters-wrapper',
    ];

    $form['wrapperFilters']['initTime'] = [
      '#type' => 'date',
      '#title' => $this->t('Fecha de inicio'),
      '#max' => date('Y-m-d'),
      '#attributes' => [
        'class' => [
          'filter',
        ],
      ],
    ];

    $form['wrapperFilters']['endTime'] = [
      '#type' => 'date',
      '#title' => $this->t('Fecha de fin'),
      '#max' => date('Y-m-d'),
      '#attributes' => [
        'class' => [
          'filter',
        ],
      ],
    ];

    $form['wrapperFilters']['nit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cliente'),
      '#autocomplete_route_name' => 'sand_core.client_field',
      '#ajax' => [
        'callback' => [$this, 'clientCallback'],
        'event' => 'change',
        'wrapper' => 'ajax-wrapper',
      ],
      '#attributes' => [
        'class' => [
          'filter',
        ],
      ],
      '#size' => 30,
    ];

    $form['downloadWrapper'] = [
      '#type' => 'container',
      '#id' => 'download-wrapper',
    ];

    $form['downloadWrapper']['downloadLabel'] = [
      '#type' => 'label',
      '#title' => $this->t('Descargar reporte:'),
      '#attributes' => [
        'class' => [
          'download-label'
        ],
      ],
    ];

    $form['downloadWrapper']['generateXls'] = [
      '#type' => 'submit',
      '#value' => $this->t('XLS'),
      '#id' => 'xlsx',
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
        ],
      ],
    ];

    $form['downloadWrapper']['generateCsv'] = [
      '#type' => 'submit',
      '#id' => 'csv',
      '#value' => $this->t('CSV'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
        ],
      ],
    ];

    $form['#theme'] = 'report-by-advance';
    $form['#attached']['library'] = 'sand_core/report_by_advance';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form,FormStateInterface $form_state) {
  }
}