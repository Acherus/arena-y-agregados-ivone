<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class TestForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_form_chef_btg';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form,FormStateInterface $form_state) {
    \Drupal::service('page_cache_kill_switch')->trigger();

    $form['cliente'] = [
      '#type' => 'select',
      '#title' => $this->t('test lineas'),
      '#options' => [
        'opt1' => 'Option 1',
        'opt2' => 'Option 2',
        'opt3' => 'Option 3',
      ],
      '#ajax' => [
        'callback' => [$this, 'testSelectCallback'],
        'event' => 'change',
        'wrapper' => 'ajax-wrapper',
      ],
    ];

    $id = $form_state->getValue('cliente');
    $options = [];

    switch($id) {
      case 'opt1':
        $options = [
          '10' => '10',
          '11' => '11',
          '12' => '12',
        ];
        break;
      case 'opt2':
        $options = [
          '7' => '7',
          '8' => '8',
          '9' => '9',
        ];
        break;
      case 'opt3':
        $options = [
          '4' => '4',
          '5' => '5',
          '6' => '6',
        ];
        break;
      default:
        $options = [
          '1' => '1',
          '2' => '2',
          '3' => '3',
        ];
        break;
    }

    $form['wrapper'] = [
      '#type' => 'container',
      '#id' => 'ajax-wrapper'
    ];

    $form['wrapper']['radios'] = [
      '#type' => 'radios',
      '#options' => $options,
      '#ajax' => [
        'callback' => [$this, 'testRadiosCallbcack'],
        'wrapper' => 'sub-wrapper',
      ],
    ];

    $form['wrapper']['equipos'] = [
      '#type' => 'container',
      '#id' => 'sub-wrapper'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function testSelectCallback(array $form, FormStateInterface $form_state) {
    return $form['wrapper'];
  }

  /**
   * {@inheritdoc}
   */
  public function testRadiosCallbcack(array $form, FormStateInterface $form_state) {
    $id = $form_state->getValue('radios');
    $texto = '';
    
    switch($id) {
      case '1':
      case '2':
        $texto = '1 y 2';
        break;
      case '3':
        $texto = 'es 3';
        break;
      case '4':
      case '5':
        $texto = '4 y 5';
        break;
      case '6':
        $texto = 'es 6';
        break;
      case '7':
      case '8':
        $texto = '7 y 8';
        break;
      case '9':
        $texto = 'es 9';
        break;
      case '10':
      case '11':
        $texto = '10 y 11';
        break;
      case '12':
        $texto = 'es 12';
        break;
    }

    $form['wrapper']['equipos']['text'] = [
      '#markup' => "es el número $texto",
    ];

    return $form['wrapper']['equipos'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form,FormStateInterface $form_state) {
  }
}