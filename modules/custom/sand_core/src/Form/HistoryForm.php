<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\sand_core\Services\AuditServices;
use Drupal\Core\Url;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;

class HistoryForm extends FormBase {

  /**
   * Connection to data base.
   * 
   * @var Connection $connection
   */
  protected $service;

  /**
   * Get module configuration.
   * 
   * @var ConfigFactoryInterface $config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(AuditServices $service, ConfigFactoryInterface $config) {
    $this->service = $service;
    $this->config = $config->get('sand_core.config');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sand_core.audit_services'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sand_core_logs';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['#attached']['library'][] = 'sand_core/history_script';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    

  }
  
}