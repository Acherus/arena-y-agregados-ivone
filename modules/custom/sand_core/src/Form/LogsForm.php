<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\sand_core\Services\AuditServices;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LogsForm extends FormBase {

  /**
   * Connection to data base.
   * 
   * @var Connection $connection
   */
  protected $service;

  /**
   * Get module configuration.
   * 
   * @var ConfigFactoryInterface $config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(AuditServices $service, ConfigFactoryInterface $config) {
    $this->service = $service;
    $this->config = $config->get('sand_core.config');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sand_core.audit_services'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sand_core_logs';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['#tree'] = TRUE;
    $filter = [];

    if (isset($_GET['filter'])) {
      $filter = (array) json_decode(base64_decode(urldecode($_GET['filter'])));
      $data = $this->service->getAuditInfo($filter);
    }
    else {
      $data = $this->service->getAuditInfo();      
    }

    $form['returnContainer'] = [
      '#type' => 'container',
      '#id' => 'return-container',
    ];

    $form['returnContainer']['linkReturn'] = [
      '#type' => 'link',
      '#title' => $this->t('Página principal'),
      '#url' => Url::fromRoute('<front>'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
        ],
      ],
    ];

    $form['wrapperFilters'] = [
      '#type' => 'container',
      '#id' => 'filters-wrapper',
    ];

    $form['wrapperFilters']['initTime'] = [
      '#type' => 'date',
      '#title' => $this->t('Fecha de inicio'),
      '#max' => date('Y-m-d'),
      '#attributes' => [
        'class' => [
          'filter',
        ],
      ],
      '#default_value' => isset($filter['initTime']) 
        ? $filter['initTime'] : '',
      
    ];

    $form['wrapperFilters']['endTime'] = [
      '#type' => 'date',
      '#title' => $this->t('Fecha de fin'),
      '#max' => date('Y-m-d'),
      '#attributes' => [
        'class' => [
          'filter',
        ],
      ],
      '#default_value' => isset($filter['endTime']) 
        ? $filter['endTime'] : '',
    ];

    $form['wrapperFilters']['client_operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Operación'),
      '#attributes' => [
        'class' => [
          'filter',
        ],
      ],
      '#options' => [
        'create_client' => 'Crear cliente',
        'update_client' => 'Actualizar cliente',
        'outs' => 'Salida de materiales',
        'pay' => 'Pagos y anticipos',
        'create_material' => 'Crear material',
        'update_material' => 'Actualizar material',
      ],
      '#default_value' => isset($filter['client_operation']) 
        ? $filter['client_operation'] : '',
    ];

    $form['wrapperFilters']['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Tipo'),
      '#attributes' => [
        'class' => [
          'filter',
        ],
      ],
      '#options' => [
        'insert' => 'Inserto',
        'update' => 'Actualizo',
      ],
      '#default_value' => isset($filter['type']) 
        ? $filter['type'] : '',
    ];

    $form['wrapperFilters']['filterIt'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filtrar'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
          'filter-it'
        ],
      ],
      '#submit' => [[$this, 'filterData']],
    ];

    $form['wrapperFilters']['clearFilters'] = [
      '#type' => 'submit',
      '#value' => $this->t('Limpiar filtros'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
          'clear-filters'
        ],
      ],
      '#submit' => [[$this, 'clearFilter']],
    ];

    $form['wrapper'] = [
      '#type' => 'container',
      '#id' => 'table-wrapper',
    ];

    $form['wrapper']['table'] = [
      '#type' => 'table',
      '#header' => ['No.','Tipo', 'Responsable', 'Operación', 'descripción', 'Fecha'],
      '#id' => 'sand-core-logs-table'
    ];

    $chunk = $this->config->get('listPager') !== NULL 
      ? $this->config->get('listPager') : 20;
    $printData = array_chunk($data, $chunk, TRUE);
    $countPages = count($printData);
    $page = $form_state->get('page') !== NULL 
      ? $form_state->get('page') 
      : 0;

    foreach ($printData[$page] as $key => $log) {
      if (!array_key_exists($log['id'], $form['wrapper']['table'])) {
        $form['wrapper']['table'][$log['id']]['No.'] = [
          '#type' => 'label',
          '#title' => $log['id'],
        ];
  
        $form['wrapper']['table'][$log['id']]['tipo'] = [
          '#type' => 'label',
          '#title' => $log['type'],
        ];
  
        $form['wrapper']['table'][$log['id']]['responsable'] = [
          '#type' => 'label',
          '#title' => $log['userName'],
        ];
  
        $form['wrapper']['table'][$log['id']]['client_operation'] = [
          '#type' => 'label',
          '#title' => $log['client_operation'],
        ];

        $form['wrapper']['table'][$log['id']]['description'] = [
          '#type' => 'label',
          '#title' => $log['description'],
        ];

        $form['wrapper']['table'][$log['id']]['fecha'] = [
          '#type' => 'label',
          '#title' => $log['fecha_registro'],
        ];
      }
    }

    if ($countPages > 1) {
      $form['pager_container'] = [
        '#type' => 'container',
        '#id' => 'pager-wrapper',
      ];

      for ($pager = 0; $pager < $countPages; $pager++) {
        $form['wrapper'][$pager] = [
          '#type' => 'submit',
          '#value' => $pager + 1,
          '#id' => $pager,
          '#submit' => [[$this, 'pagerSubmit']],
          '#ajax' => [
            'callback' => [$this, 'pagerCallback'],
            'wrapper' => 'table-wrapper',
          ],
          '#attributes' => [
            'class' => [
              'pager'
            ],
          ],
        ];

        if ($pager == $page) {
          $form['wrapper'][$pager]['#attributes']['class'][] = 'pager-selected';
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function filterData(array $form,FormStateInterface $form_state) {
    $filters = $form_state->getValue('wrapperFilters');
    $data = [];

    if (!empty($filters['initTime'])) {
      $data['initTime'] = $filters['initTime'];
    }

    if (!empty($filters['endTime'])) {
      $data['endTime'] = $filters['endTime'];
    }

    if (!empty($filters['type'])) {
      $data['type'] = $filters['type'];
    }

    if (!empty($filters['client_operation'])) {
      $data['client_operation'] = $filters['client_operation'];
    }

    $form_state->setRedirect('sand_core.logs_list', [
      'filter' => urlencode(base64_encode((json_encode($data))))
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function clearFilter(array $form,FormStateInterface $form_state) {
    $form_state->setRedirect('sand_core.logs_list');
  }

  /**
   * Return table.
   */
  public function pagerCallback(array $form,FormStateInterface $form_state) {
    return $form['wrapper'];
  }

  /**
   * Set current page.
   */
  public function pagerSubmit(array $form,FormStateInterface $form_state) {
    $el = $form_state->getTriggeringElement()['#id'];
    $form_state->set('page', $el);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }
}