<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
class ReportsHomeForm extends FormBase {

    /**
   * Get user interface to validate if the user is logged or not.
   * 
   * @var AccountInterface $user
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  public function __construct(AccountInterface $user) {
    $this->user = $user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user')
    );
  }

  public function getFormId() {
    return 'sand_core_reports_home';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $form = [];
    $form['#tree'] = TRUE;

    $form['wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['card-wrapper']
      ],
    ];

    if ($this->user->hasPermission('volumenes mensual menu')) {
      $form['wrapper']['menu_outs'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'menu-container',
            'outs',
          ],
        ],
      ];

      $form['wrapper']['menu_outs']['out_link'] = [
        '#type' => 'link',
        '#title' => $this->t('Consolidado volumenes de despachos mensual'),
        '#url' => Url::fromRoute('sand_core.reporte_mensual'),
        '#attributes' => [
          'class' => [
            'link-class',
          ],
        ],
      ];
    }

    if ($this->user->hasPermission('despachos anticipo menu')) {
      $form['wrapper']['menu_dispath'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'menu-container',
            'dispath',
          ],
        ],
      ];

      $form['wrapper']['menu_dispath']['dispath_link'] = [
        '#type' => 'link',
        '#title' => $this->t('Control de despachos con anticipo'),
        '#url' => Url::fromRoute('sand_core.reporte_anticipos'),
        '#attributes' => [
          'class' => [
            'link-class',
          ],
        ],
      ];
    }

    if ($this->user->hasPermission('reporte cliente menu')) {
      $form['wrapper']['menu_client'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'menu-container',
            'client',
          ],
        ],
      ];

      $form['wrapper']['menu_client']['client_link'] = [
        '#type' => 'link',
        '#title' => $this->t('Reporte por cliente'),
        '#url' => Url::fromRoute('sand_core.reports_client'),
        '#attributes' => [
          'class' => [
            'link-class',
          ],
        ],
      ];
    }

    if ($this->user->hasPermission('salida materiales menu')) {
      $form['wrapper']['menu_report'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'menu-container',
            'report',
          ],
        ],
      ];

      $form['wrapper']['menu_report']['report_link'] = [
        '#type' => 'link',
        '#title' => $this->t('Control de salida de materiales'),
        '#url' => Url::fromRoute('sand_core.reports'),
        '#attributes' => [
          'class' => [
            'link-class',
          ],
        ],
      ];
    }

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $fom_state) {
 
  }
}