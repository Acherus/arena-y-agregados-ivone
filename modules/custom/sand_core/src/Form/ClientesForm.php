<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sand_core\Services\QueryServices;
use Drupal\Core\Url;
use Drupal\sand_core\Services\AuditServices;
use Drupal\sand_core\Services\ReportsServices;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ClientesForm extends FormBase {

  /**
   * Query services.
   * 
   * @var QueryServices $services
   */
  protected $services;

  /**
   * @var AuditServices $audit
   */
  protected $audit;

  /**
   * Connection to data base.
   * 
   * @var ReportsServices $connection
   */
  protected $service;

  /**
   * {@inheritdoc}
   */
  public function __construct(QueryServices $services, AuditServices $audit, ReportsServices $service) {
    $this->services = $services;
    $this->audit = $audit;
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sand_core.query_services'),
      $container->get('sand_core.audit_services'),
      $container->get('sand_core.repot_query')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sand_core_clientes';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateinterface $form_state) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    // Validate if is a material change or new material.

    try {
      if (isset($_GET['cliente'])) {
        // Get configuration data;

        $data = $this->services->getAllClientInformation($_GET['cliente']);
      }
      else {
        $data = [];
      }
    }
    catch(\Exception $e) {
      $data = [];
    }

    $form = [];
    $form['#tree'] = TRUE;
    $add = $form_state->get('add') ==! NULL 
      ? $form_state->get('add') : 1;
    $add_driver = $form_state->get('addDriver') ==! NULL 
      ? $form_state->get('addDriver') : 1;
    $delete = $form_state->get('del') ==! NULL 
      ? $form_state->get('del') : [];

    if (isset($_GET['id']) && !empty($_GET['id'])) {
      $form_state->set('update', TRUE);
      $clientId = $_GET['id'];
    }
    else {
      $form_state->set('update', FALSE);
    }

    $form['returnContainer'] = [
      '#type' => 'container',
      '#id' => 'return-container',
    ];

    $form['returnContainer']['linkReturn'] = [
      '#type' => 'link',
      '#title' => $this->t('Lista de clientes'),
      '#url' => Url::fromRoute('sand_core.clients_list'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
        ],
      ],
    ];

    $form['cliente'] = [
      '#type' => 'container',
      '#id' => 'ajax-wrapper'
    ];

    $form['idenfier'] = [
      '#type' => 'hidden',
      '#value' => empty($data) 
        ? 'new' : $data['clientData']->id,
    ];

    $form['cliente']['id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Número del documento'),
      '#default_value' => empty($data) 
        ? '' : $data['clientData']->id, 
    ];

    $form['cliente']['doc_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Tipo de documento'),
      '#options' => [
        'NIT' => $this->t('NIT'),
        'CC' => $this->t('CC'),
      ],
      '#default_value' => empty($data) 
        ? '' : $data['clientData']->doc_type, 
    ];

    $form['cliente']['razon_social'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Razón social'),
      '#default_value' => empty($data)
        ? '' : $data['clientData']->razon_social, 
    ];

    if (!empty($data)) {
      $form['cliente']['saldo_wrapper'] = [
        '#type' => 'container',
        '#id' => 'saldo-wrapper',
      ];

      $materiales = $this->service->getMaterials();
      $form_state->set('materiales', $materiales);
      $options = [];

      foreach ($materiales as $material) {
        $options[$material->material] = $material->material;
      }

      $form['cliente']['saldo_wrapper']['material'] = [
        '#type' => 'select',
        '#title' => $this->t('Material'),
        '#options' => $options,
        '#attributes' => [
          'class' => [
            'material-cl',
          ],
        ],
        '#ajax' => [
          'wrapper' => 'saldo-wrapper',
          'callback' => [$this, 'materialCallback'],
          'event' => 'change',
        ]
      ];

      $saldoCalculado = (float)$data['clientData']->cantidad_ant * (float)$materiales[0]->valor_unitario;
      $saldoCalculadoIVA = (float)$saldoCalculado + ((float)$saldoCalculado * 0.19);

      $saldo = !empty($data['clientData']->saldo)
        ? $saldoCalculadoIVA
        : 0;
      $form_state->set('saldoVal', $data['clientData']->cantidad_ant);

      $form['cliente']['saldo_wrapper']['saldo'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Saldo dinero a material'),
        '#default_value' => $saldo,
        '#disabled' => TRUE,
        '#wrapper_attributes' => [
          'class' => [
            'saldo-div'
          ],
        ],
      ];

      $form['cliente']['cantidad_ant'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Saldo material m<sup>3</sup>'),
        '#disabled' => TRUE,
        '#default_value' => $data['clientData']->cantidad_ant,
      ];
    }

    $form['cliente']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#default_value' => empty($data) 
        ? '' : $data['clientData']->email,
    ];

    $form['cliente']['direccion'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dirección'),
      '#default_value' => empty($data) 
        ? '' : $data['clientData']->direccion,
    ];

    if (isset($data) && !empty($data)) {
      foreach ($data['phones'] as $key => $value) {
        if (!in_array($key, $delete)) {
          $form['cliente']['telefonos'][$value->id]['id'] = [
            '#type' => 'hidden',
            '#value' => $value->id
          ];
          $form['cliente']['telefonos'][$value->id]['telefono'] = [
            '#type' => 'number',
            '#title' => $this->t('teléfono'),
            '#default_value' => empty($data) 
              ? '' : $value->telefono, 
          ];
        }
      }
    }

    // Add new elements.
    for ($x = 0; $x < $add; $x++) {
      if (!in_array("new_$x", $delete)) {
        $form['cliente']['telefonos']["new_$x"]['id'] = [
          '#type' => 'hidden',
          '#value' => "new",
        ];
        $form['cliente']['telefonos']["new_$x"]['telefono'] = [
          '#type' => 'number',
          '#title' => $this->t('Teléfono'),
        ];
      }
    }

    $form['cliente']['more_phone'] = [
      '#type' => 'submit',
      '#value' => $this->t('¿Adicionar otro teléfono?'),
      '#submit' => [[$this, 'ajaxAddSubmit']],
      '#ajax' => [
        'callback' => [$this, 'ajaxCallback'],
        'wrapper' => 'ajax-wrapper'
      ]
    ];

    if (isset($data) && !empty($data)) {
      foreach ($data['conductores'] as $key => $value) {
        if (!in_array($key, $delete)) {
          $form['cliente']['conductores'][$value->id]['id'] = [
            '#type' => 'hidden',
            '#value' => $value->id
          ];

          $form['cliente']['conductores'][$value->id]['nombres_conductor'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Nombre del conductor'),
            '#default_value' => empty($data) 
              ? '' : $value->nombres_conductor, 
          ];

          $form['cliente']['conductores'][$value->id]['apellidos_conductor'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Apellidos del conductor'),
            '#default_value' => empty($data) 
              ? '' : $value->apellidos_conductor, 
          ];

          $form['cliente']['conductores'][$value->id]['placa'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Placa del vehiculo'),
            '#default_value' => empty($data) 
              ? '' : $value->placa, 
          ];
        }
      }
    }

    // Add new elements.
    for ($x = 0; $x < $add_driver; $x++) {
      if (!in_array("new_$x", $delete)) {
        $form['cliente']['conductores']["new_$x"]['id'] = [
          '#type' => 'hidden',
          '#value' => "new",
        ];
        $form['cliente']['conductores']["new_$x"]['nombres_conductor'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Nombre del conductor'),
        ];
        $form['cliente']['conductores']["new_$x"]['apellidos_conductor'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Apellidos del conductor'),
        ];
        $form['cliente']['conductores']["new_$x"]['placa'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Placa del vehiculo'),
        ];
      }
    }

    $form['cliente']['more_drivers'] = [
      '#type' => 'submit',
      '#value' => $this->t('¿Adicionar otro conductor?'),
      '#submit' => [[$this, 'ajaxAddDriverSubmit']],
      '#ajax' => [
        'callback' => [$this, 'ajaxCallback'],
        'wrapper' => 'ajax-wrapper'
      ]
    ];

    $form['actions']['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Guardar'),
      '#attributes' => [
        'class' => [
          'btn-basic'
        ]
      ]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxDeleteSubmit(array $form, FormStateinterface $form_state) {
    $toDelete = $form_state->get('del') ==! NULL 
      ? $form_state->get('del') : [];
    array_push($toDelete, $form_state->getTriggeringElement()['#name']);
    $form_state->set('del', $toDelete);
    $form_state->setRebuild();
  }
  

  /**
   * {@inheritdoc}
   */
  public function ajaxAddSubmit(array $form, FormStateinterface $form_state) {
    $add = $form_state->get('add') ==! NULL 
      ? $form_state->get('add') : 1;
    $form_state->set('add', $add + 1);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxAddDriverSubmit(array $form, FormStateinterface $form_state) {
    $add = $form_state->get('addDriver') ==! NULL 
      ? $form_state->get('addDriver') : 1;
    $form_state->set('addDriver', $add + 1);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxCallback(array $form, FormStateinterface $form_state) {
    return $form['cliente'];
  }

  /**
   * {@inheritdoc}
   */
  public function materialCallback(array &$form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    $materiales = $form_state->get('materiales');
    $saldo = $form_state->get('saldoVal');
    $material = $form_state->getValue(['cliente','saldo_wrapper','material']);

    // Get materials.
    foreach ($materiales as $value) {
      if ($value->material == $material) {
        $info = $value;
      }
    }

    $form['cliente']['saldo_wrapper']['saldo']['#value'] = (float)$saldo * (float)$info->valor_unitario;

    $form_state->setRebuild();
    return $form['cliente']['saldo_wrapper'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateinterface $form_state) {
    // Get form information.
    $clientInfo = $form_state->getValue('cliente');
    if (isset($clientInfo['saldo_wrapper'])) {
      $clientInfo['saldo'] = $clientInfo['saldo_wrapper']['saldo'];
      unset($clientInfo['saldo_wrapper']);
    }

    $this->services->pushClientInformation($clientInfo, $form_state->getValue('idenfier'));

    $push = $this->services->getPushStatus();
    if ($push['ok']) {
      drupal_set_message('Información guardada correctamente.');
      $this->audit->pushLog($push['type'], 'Se crea el cliente '. $clientInfo['id'], $push['client_operation']);
    }
    else {
      drupal_set_message('Error al guardar la información.', 'error');
    }
    $form_state->setRedirect('sand_core.clients_list');
  }

}