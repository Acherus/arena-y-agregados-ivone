<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\sand_core\Services\AuditServices;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\HtmlCommand;

class GenerateOutForm extends FormBase {

  const MATERIALES = 'core_materiales',
        CLIENTES = 'core_clientes',
        OUT = 'core_despachos',
        CONDUCTORES = 'core_conductores',
        MOVEMENTS = 'core_movements';

  /**
   * Connection to data base.
   * 
   * @var Connection $connection
   */
  protected $db;

  /**
   * Private tempstore.
   * 
   * @var PrivateTempStoreFactory $store
   */
  protected $store;

  /**
   * Get user object.
   * 
   * @var AccountInterface $connection
   */
  protected $user;

  /**
   * @var AuditServices $audit
   */
  protected $audit;

  /**
   * Get module configuration.
   * 
   * @var ConfigFactoryInterface $config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, 
                              PrivateTempStoreFactory $tempstore,
                              AccountInterface $user,
                              AuditServices $audit,
                              ConfigFactoryInterface $config) {
    $this->db = $connection;
    $this->store = $tempstore->get('outMaterial');
    $this->user = $user;
    $this->audit = $audit;
    $this->config = $config->getEditable('sand_core.config');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('tempstore.private'),
      $container->get('current_user'),
      $container->get('sand_core.audit_services'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sand_core_generate_out';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $form = [];
    $form['#tree'] = TRUE;
    $form_state->set('valor', 0);
    $this->store->set('valor', 0);

    $form['returnContainer'] = [
      '#type' => 'container',
      '#id' => 'return-container',
    ];

    $form['returnContainer']['linkReturn'] = [
      '#type' => 'link',
      '#title' => $this->t('Página principal'),
      '#url' => Url::fromRoute('<front>'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
        ],
      ],
    ];

    $form['cliente'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cliente'),
      '#autocomplete_route_name' => 'sand_core.client_field',
      '#ajax' => [
        'callback' => [$this, 'clientCallback'],
        'event' => 'change',
        'wrapper' => 'ajax-wrapper',
      ],
      '#required' => TRUE,
    ];

    $form['wrapper'] = [
      '#type' => 'container',
      '#id' => 'ajax-wrapper',
    ];

    $form['wrapper']['razon_social'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Razón social'),
      '#disabled' => TRUE,
      '#required' => TRUE,
    ];

    $form['wrapper']['placa'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placa'),
      '#required' => TRUE,
      '#autocomplete_route_name' => 'sand_core.placa_field',
      '#ajax' => [
        'callback' => [$this, 'placaCallback'],
        'event' => 'change',
        'wrapper' => 'ajax-wrapper',
      ],
    ];

    $form['wrapper']['conductor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Conductor'),
      '#required' => TRUE,
      '#autocomplete_route_name' => 'sand_core.conductor_field',
      '#ajax' => [
        'callback' => [$this, 'conductorCallback'],
        'event' => 'change',
        'wrapper' => 'ajax-wrapper',
      ],
      '#required' => TRUE,
    ];

    $form['wrapper']['saldo'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Saldo disponible'),
      '#disabled' => TRUE,
      '#required' => TRUE,
    ];

    $form['wrapper']['saldo_metros'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Saldo disponible m<sup>3<sup>'),
      '#disabled' => TRUE,
      '#required' => TRUE,
    ];

    $materiales = $this->db->select(self::MATERIALES, 'cm')
      ->fields('cm')
      ->execute()
      ->fetchAll();
    $form_state->set('materiales', $materiales);
    $options = [];

    foreach ($materiales as $material) {
      $options[$material->id] = $material->material;
    }

    $form['wrapper_material'] = [
      '#type' => 'container',
      '#id' => 'ajax-wrapper-material',
    ];
    
    $form['wrapper_material']['material'] = [
      '#type' => 'select',
      '#title' => $this->t('Material'),
      '#options' => $options,
      '#ajax' => [
        'callback' => [$this, 'materialCallback'],
        'event' => 'change',
        'wrapper' => 'ajax-wrapper-material',
      ],  
    ];

    $form['wrapper_material']['tipo_pago'] = [
      '#type' => 'select',
      '#title' => $this->t('Tipo de pago'),
      '#options' => [
        'efectivo' => 'Efectivo',
        'credito' => 'Crédito',
        'debito' => 'Debito',
      ],
    ];

    $form['wrapper_material']['val_alt'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Salida con valor alternativo'),
      '#ajax' => [
        'callback' => [$this, 'materialCallback'],
        'event' => 'change',
        'wrapper' => 'ajax-wrapper-material',
        'disable-refocus' => TRUE,
      ],
    ];

    $form['wrapper_material']['metros_cubicos'] = [
      '#type' => 'number',
      '#title' => $this->t('Metros cubicos'),
      '#ajax' => [
        'callback' => [$this, 'materialCallback'],
        'event' => 'change',
        'wrapper' => 'ajax-wrapper-material',
      ],     
    ];

    $form['wrapper_material']['valor_salida'] = [
      '#type' => 'number',
      '#title' => $this->t('Valor de salida'),
      '#disabled' => TRUE,
    ];

    $form['wrapper_material']['iva'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('+IVA'),
      '#ajax' => [
        'callback' => [$this, 'materialCallback'],
        'event' => 'change',
        'wrapper' => 'ajax-wrapper-material',
        'disable-refocus' => TRUE,
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generar salida'),
      '#attributes' => [
        'class' => [
          'btn-basic',
        ],
      ],
    ];

    $form['#attached']['library'][] = 'sand_core/out_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function clientCallback(array $form, FormStateInterface $form_state) {
    $id = $form_state->getValue('cliente');
    $materiales = $form_state->get('materiales');
    $material = $form_state->getValue(['wrapper_material','material']);
    $info = [];

    // Get materials.
    foreach ($materiales as $value) {
      if ($value->id == $material) {
        $info = $value;
      }
    }

    $data = $this->db->select(self::CLIENTES, 'cc')
      ->fields('cc', ['id', 'saldo', 'razon_social', 'en_deuda', 'cantidad_ant', 'doc_type'])
      ->condition('id', $id, '=')
      ->execute()
      ->fetch();

    if (!empty($data)) {
      $form_state->set('user_data', $data);
      $this->store->set('user_data', $data);
      $form['wrapper']['razon_social']['#value'] = $data->razon_social;
      $form['wrapper']['saldo']['#value'] = $info->valor_unitario * $data->cantidad_ant;
      $form['wrapper']['saldo_metros']['#value'] = $data->cantidad_ant;
    }
    $form_state->setRebuild();
    return $form['wrapper'];
  }

  /**
   * {@inheritdoc}
   */
  public function conductorCallback(array $form, FormStateInterface $form_state) {
    $id = explode('-', $form_state->getValue(['wrapper', 'conductor']));
    $materiales = $form_state->get('materiales');
    $material = $form_state->getValue(['wrapper_material','material']);
    $userData = $this->store->get('user_data');
    $info = [];

    // Get materials.
    foreach ($materiales as $value) {
      if ($value->id == $material) {
        $info = $value;
      }
    }

    $data = $this->db->select(self::CONDUCTORES, 'cc')
      ->fields('cc', ['id', 'placa', 'nombres_conductor', 'apellidos_conductor'])
      ->condition('id', trim($id[1]), '=')
      ->execute()
      ->fetch();

    if (!empty($data)) {
      $this->store->set('driver', $data);
      $form['wrapper']['placa']['#value'] = $data->placa;
      
    }

    $user = $this->store->get('user_data');
    if ($user !== NULL) {
      $form['wrapper']['razon_social']['#value'] = $user->razon_social;
      $form['wrapper']['saldo']['#value'] = $user->saldo;
      $form['wrapper']['saldo_metros']['#value'] = $user->cantidad_ant;
    }
    $saldo = $info->valor_unitario * $userData->cantidad_ant;

    $form_state->setRebuild();

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#ajax-wrapper', $form['wrapper']));
    $response->addCommand(new InvokeCommand(NULL, 'updateSaldo', [$saldo]));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function placaCallback(array $form, FormStateInterface $form_state) {
    $id = $form_state->getValue(['wrapper', 'placa']);
    $materiales = $form_state->get('materiales');
    $material = $form_state->getValue(['wrapper_material','material']);
    $userData = $this->store->get('user_data');
    $info = [];

    // Get materials.
    foreach ($materiales as $value) {
      if ($value->id == $material) {
        $info = $value;
      }
    }

    $data = $this->db->select(self::CONDUCTORES, 'cc')
      ->fields('cc', ['id', 'placa', 'nombres_conductor', 'apellidos_conductor'])
      ->condition('placa', $id, '=')
      ->execute()
      ->fetch();

    if (!empty($data)) {
      $this->store->set('driver', $data);
      $form['wrapper']['conductor']['#value'] = $data->nombres_conductor . ' ' . $data->apellidos_conductor .' - ' . $data->id;
    }

    $user = $this->store->get('user_data');
    if ($user !== NULL) {
      $form['wrapper']['razon_social']['#value'] = $user->razon_social;
      $form['wrapper']['saldo']['#value'] = $user->saldo;
      $form['wrapper']['saldo_metros']['#value'] = $user->cantidad_ant;
    }
    $saldo = $info->valor_unitario * $userData->cantidad_ant;

    $form_state->setRebuild();

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#ajax-wrapper', $form['wrapper']));
    $response->addCommand(new InvokeCommand(NULL, 'updateSaldo', [$saldo]));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function materialCallback(array &$form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    $materiales = $form_state->get('materiales');
    $info = [];
    $cantidad = $form_state->getValue(['wrapper_material','metros_cubicos']);
    $iva = $form_state->getValue(['wrapper_material','iva']);
    $material = $form_state->getValue(['wrapper_material','material']);
    $altVal = $form_state->getValue(['wrapper_material','val_alt']);
    $data = $this->store->get('user_data');

    // Get materials.
    foreach ($materiales as $value) {
      if ($value->id == $material) {
        $info = $value;
      }
    }

    if ($altVal != 1) {
      // Set default value.
      $valor = (float)$cantidad * (float)$info->valor_unitario;
      if (isset($iva) && $iva == 1) {
        $form['wrapper_material']['valor_salida']['#value'] = $valor + ($valor * 0.19);
      }
      else {
        $form['wrapper_material']['valor_salida']['#value'] = $valor;
      }
      $form['wrapper_material']['valor_salida']['#disabled'] = TRUE;
      $form['wrapper_material']['tipo_pago']['#value'] = $form_state->getValue(['wrapper_material', 'tipo_pago']);
    }
    else {
      $form['wrapper_material']['valor_salida']['#disabled'] = FALSE;
      $form['wrapper_material']['valor_salida']['#value'] = (float)$cantidad * (float)$info->val_alter;
    }

    $saldo = $info->valor_unitario * $data->cantidad_ant;

    $form_state->setRebuild();

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#ajax-wrapper-material', $form['wrapper_material']));
    $response->addCommand(new InvokeCommand(NULL, 'updateSaldo', [$saldo]));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function altValue(array &$form, FormStateInterface $form_state) {
    $materiales = $form_state->get('materiales');
    $info = [];
    $cantidad = $form_state->getValue(['wrapper_material','metros_cubicos']);
    $iva = $form_state->getValue(['wrapper_material','iva']);
    $material = $form_state->getValue(['wrapper_material','material']);
    $altVal = $form_state->getValue(['wrapper_material','val_alt']);
    // Get materials.
    foreach ($materiales as $value) {
      if ($value->id == $material) {
        $info = $value;
      }
    }

    // Set default value.
    $valor = (float)$cantidad * (float)$info->valor_unitario;
    if (isset($iva) && $iva == 1) {
      $form['wrapper_material']['valor_salida']['#value'] = $valor + ($valor * 0.19);
    }
    else {
      $form['wrapper_material']['valor_salida']['#value'] = $valor;
    }

    $form['wrapper_material']['tipo_pago']['#value'] = $form_state->getValue(['wrapper_material', 'tipo_pago']);
    $form_state->setRebuild();
    return $form['wrapper_material'];
  }

  /**
   * {@inheritdoc}
   */
  function validateForm(array &$form, FormStateInterface $form_state) {
    $userInput = $form_state->getValue('wrapper_material');

    $metros = $form_state->getValue(['wrapper_material', 'metros_cubicos']);
    $altVal = $form_state->getValue(['wrapper_material','val_alt']);
    $materialId = $form_state->getValue(['wrapper_material','material']);
    $iva = $form_state->getValue(['wrapper_material','iva']);
    $userData = $this->store->get('user_data');
    $valor = isset($valAlt) ? $valAlt : $valor;

    if ($iva == 1 && !preg_match('/([0-9])-([0-9])/', $userData->id)) {
      $form_state->setErrorByName('iva', 'Solo los clientes con NIT pueden realizar salidas con IVA.');
    }

    foreach ($form_state->get('materiales') as $value) {
      if ($value->id == $materialId) {
        $material = $value;
      }
    }

    if ($altVal != 1) {
      // Set default value.
      $valor = (float)$metros * (float)$material->valor_unitario;
      if (isset($iva) && $iva == 1) {
        $valor = $valor + ($valor * 0.19);
      }
    }
    else {
      $valAlt = (float)$metros * (float)$material->val_alter;
      $valor = (float)$metros * (float)$material->valor_unitario;
      if (isset($iva) && $iva == 1) {
        $valor = $valor + ($valor * 0.19);
      }
    }

    if (empty($userInput['metros_cubicos'])) {
      $form_state->setErrorByName('metros', 'Campo metros cubicos es requerido.');
    }

    if (empty($userInput['material'])) {
      $form_state->setErrorByName('metros', 'Campo material es requerido.');
    }

    if ($userInput['tipo_pago'] == 'debito' && $userData->cantidad_ant - $metros < 0) {
      $form_state->setErrorByName('valor', 'El usuario no cuenta con saldo suficiente para realizar la transacción.');
    }

    /*if ($userData->en_deuda == 1 && $userInput['tipo_pago'] == 'debito') {
      $form_state->setErrorByName('valor', 'El usuario no se encuentra al día para realizar una salida a crédito.');
    }*/
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $metros = $form_state->getValue(['wrapper_material', 'metros_cubicos']);
    $materialId = $form_state->getValue(['wrapper_material','material']);
    $userData = $this->store->get('user_data');
    $driverData = $this->store->get('driver');
    $tipoPago = $form_state->getValue(['wrapper_material', 'tipo_pago']);
    $iva = $form_state->getValue(['wrapper_material','iva']);
    $date = new \DateTime();
    $date->setTimeStamp(\Drupal::time()->getCurrentTime());
    $date->setTimeZone(new \DateTimeZone('America/Bogota'));
    $material = '';
    $conductor = trim(explode('-',$form_state->getValue(['wrapper_material', 'conductor']))[0]);
    $placa = $form_state->getValue(['wrapper_material', 'placa']);
    $altVal = $form_state->getValue(['wrapper_material','val_alt']);

    foreach ($form_state->get('materiales') as $value) {
      if ($value->id == $materialId) {
        $material = $value;
      }
    }

    if ($tipoPago == 'debito' && $userData->doc_type == 'NIT') {
      $iva = 1;
    }

    if ($altVal != 1) {
      // Set default value.
      $valor = (float)$metros * (float)$material->valor_unitario;
      if ((isset($iva) && $iva == 1)) {
        $valor = $valor + ($valor * 0.19);
      }
    }
    else {
      $valAlt = (float)$metros * (float)$material->val_alter;
      $valor = (float)$metros * (float)$material->valor_unitario;
      if (isset($iva) && $iva == 1) {
        $valor = $valor + ($valor * 0.19);
      }
    }

    $valor_material_unidad = $iva == 1
      ? $material->valor_unitario + ($material->valor_unitario * 0.19) 
      : $material->valor_unitario;

    try {
      $consecutive = $this->config->get('consecutive');
      $remision = $this->config->get('remision');

      $this->config->set('consecutive', $consecutive + 1)
        ->set('remision', $remision + 1)
        ->save();
      $this->db->insert(self::OUT)
        ->fields([
          'id_cliente' => $userData->id,
          'razon_social' => $userData->razon_social,
          'placa' => $driverData->placa,
          'valor_total' => (float) $valor,
          'material' => (string) $material->material,
          'volumen' => (float )$metros,
          'fecha' => $date->format('Y-m-d H:i:s'),
          'responsable' => $this->user->getUsername(),
          'valor_unidad' => $valor_material_unidad,
          'tipo_pago' => $tipoPago,
          'id_remision' => $remision,
          'id_anm' => $consecutive,
          'iva' => $iva,
        ])
        ->execute();

      if ($tipoPago == 'credito' ) {
        $saldo = $userData->saldo - (float) $valor;
        $fields = [
          'saldo' => $userData->saldo - (float) $valor,
          'en_deuda' => 1,
          'cantidad_ant' => (int) $userData->cantidad_ant - (int) $metros
        ];
      }
      elseif ($tipoPago == 'debito') {
        $newValor = $userData->saldo / $userData->cantidad_ant;
        $newValor = $newValor * $metros;
        $saldo = (float) $userData->saldo - (float) $newValor;
        $fields = [
          'saldo' => $saldo,
          'cantidad_ant' => (int) $userData->cantidad_ant - (int) $metros
        ];
      }
      else {
        $saldo = $userData->saldo;
      }

      if ($tipoPago != 'efectivo') {
        $this->db->update(self::CLIENTES)
        ->fields($fields)
        ->condition('id', $userData->id, '=')
        ->execute();
      }

      if ($tipoPago == 'credito' || $tipoPago == 'debito') {
        $saldo_material = (int) $userData->cantidad_ant - (int) $metros;
      }
      else {
        $saldo_material = $userData->cantidad_ant;
      }

      $mVal = isset($valAlt) ? $valAlt : $valor;
      $this->db->insert(self::MOVEMENTS)
      ->fields([
        'id_cliente' => $userData->id,
        'razon_social' => $userData->razon_social,
        'fecha' => $date->format('Y-m-d H:i:s'),
        'id_vale' => $remision,
        'material' => (string) $material->material,
        'volumen' => (float) $metros,
        'valor_total' => (float) $mVal,
        'saldo' => $saldo,
        'anticipo' => 0,
        'tipo_salida' => $tipoPago,
        'valor_anticipo' => $userData->saldo,
        'saldo_material' => $saldo_material,
      ])
      ->execute();

      $remisionVal = $iva == 1 ? ($valor / 1.19) : $valor;

      $urlData = urlencode(base64_encode(json_encode([
        'razonSocial' => $userData->razon_social,
        'id' => $userData->id,
        'material' => $material->material,
        'metros' => $metros,
        'fecha' => $date->format('Y-m-d'),
        'consecutive' => $consecutive,
        'remision' => $remision,
        'conductor' => $driverData->nombres_conductor . ' ' . $driverData->apellidos_conductor,
        'placa' => $driverData->placa,
        'valor' => $mVal,
      ])));

      $fields = [
        'id_reg' => $remision,
        'id_cert' => $consecutive,
        'data_cert' => $urlData,
        'data_reg' => $urlData,
        'date_creation' => date('Y-m-d'),
        'placa' => $driverData->placa,
      ];
      $this->db->insert('sand_core_cert_report_history_data')
        ->fields($fields)
        ->execute();

      $form_state->setRedirect('sand_core.remision_print', ['data' => $urlData, 'status' => 1]);

      $description = 'Se genera salida del material @material por el valor @value y cantidad @cant al cliente @client';
      $tokens = [
        '@material' => $material->material,
        '@value' => $valor,
        '@cant' => $metros,
        '@client' => $userData->razon_social,
      ];
      $this->audit->pushLog('insert', $this->t($description, $tokens), 'outs');
      drupal_set_message('Información guardar exitosamente.');
    }
    catch (\Exception $e) {
      drupal_set_message('Error al guardar la información.', 'error');      
    }
  }
}