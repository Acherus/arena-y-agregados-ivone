<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MaterialListForm extends FormBase{

  // Define data base table name;
  const MATERIALES = 'core_materiales';

  /**
   * Connection to data base.
   * 
   * @var Connection $connection
   */
  protected $connection;

  /**
   * Get module configuration.
   * 
   * @var ConfigFactoryInterface $config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, ConfigFactoryInterface $config) {
    $this->connection = $connection;
    $this->config = $config->get('sand_core.config');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sand_core_material_list';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    \Drupal::service('page_cache_kill_switch')->trigger();

    $form = [];
    $form['#tree'] = TRUE;
    $data = $this->connection->select(self::MATERIALES, 'cm')
      ->fields('cm')
      ->execute()
      ->fetchAll();

    $form['returnContainer'] = [
      '#type' => 'container',
      '#id' => 'return-container',
    ];

    $form['returnContainer']['linkReturn'] = [
      '#type' => 'link',
      '#title' => $this->t('Página principal'),
      '#url' => Url::fromRoute('<front>'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
        ],
      ],
    ];

    $form['addMaterial'] = [
      '#type' => 'link',
      '#title' => $this->t('agregar'),
      '#url' => Url::fromRoute('sand_core.materiales'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
        ],
      ],
    ];
    $count = 1;

    $form['wrapper'] = [
      '#type' => 'container',
      '#id' => 'table-wrapper',
    ];

    $form['wrapper']['table'] = [
      '#type' => 'table',
      '#header' => ['NO.','Nombre', 'Valor', 'Valor alternativo', ''],
    ];

    $chunk = $this->config->get('listPager') !== NULL 
      ? $this->config->get('listPager') : 20;
    $printData = array_chunk($data, $chunk, TRUE);
    $countPages = count($printData);
    $page = $form_state->get('page') !== NULL 
      ? $form_state->get('page') 
      : 0;

    foreach ($printData[$page] as $key => $material) {
      $form['wrapper']['table'][$key]['NO.'] = [
        '#type' => 'label',
        '#title' => $count,
      ];

      $form['wrapper']['table'][$key]['nombre'] = [
        '#type' => 'label',
        '#title' => $material->material,
      ];

      $form['wrapper']['table'][$key]['valor'] = [
        '#type' => 'label',
        '#title' => $material->valor_unitario,
      ];

      $form['wrapper']['table'][$key]['valor_alt'] = [
        '#type' => 'label',
        '#title' => $material->val_alter,
      ];

      $form['wrapper']['table'][$key]['editar'] = [
        '#type' => 'link',
          '#title' => $this->t('Editar'),
          '#url' => Url::fromRoute('sand_core.materiales', [
            'material' => $material->id
          ]),
          ];

      $count++;
    }

    if ($countPages > 1) {
      $form['pager_container'] = [
        '#type' => 'container',
        '#id' => 'pager-wrapper',
      ];

      for ($pager = 0; $pager < $countPages; $pager++) {
        $form['wrapper'][$pager] = [
          '#type' => 'submit',
          '#value' => $pager + 1,
          '#id' => $pager,
          '#submit' => [[$this, 'pagerSubmit']],
          '#ajax' => [
            'callback' => [$this, 'pagerCallback'],
            'wrapper' => 'table-wrapper',
          ],
          '#attributes' => [
            'class' => [
              'pager'
            ],
          ],
        ];

        if ($pager == $page) {
          $form['wrapper'][$pager]['#attributes']['class'][] = 'pager-selected';
        }
      }
    }

    $form['#theme'] = 'material-list';
    $form['#attached'] = [
      'library' => [
        'sand_core/sand_core',
      ],
    ];

    return $form;
  }

  /**
   * Return table.
   */
  public function pagerCallback(array $form,FormStateInterface $form_state) {
    return $form['wrapper'];
  }

  /**
   * Set current page.
   */
  public function pagerSubmit(array $form,FormStateInterface $form_state) {
    $el = $form_state->getTriggeringElement()['#id'];
    $form_state->set('page', $el);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }
}