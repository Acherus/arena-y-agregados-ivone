<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Drupal\sand_core\Services\AuditServices;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

class PagosAnticiposForm extends FormBase {

  const CLIENTES = 'core_clientes',
        PAGOS = 'core_pagos',
        MOVEMENTS = 'core_movements',
        MATERIALES = 'core_materiales';

  /**
   * Connection to data base.
   * 
   * @var \Drupal\Core\Database\Connection $db
   */
  protected $db;

  /**
   * @var AuditServices $audit
   */
  protected $audit;

  /**
   * Get module configuration.
   * 
   * @var ConfigFactoryInterface $config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, AuditServices $audit, ConfigFactoryInterface $config) {
    $this->db = $connection;
    $this->audit = $audit;
    $this->config = $config->getEditable('sand_core.config');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('sand_core.audit_services'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sand_core_pagos_anticipos';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $form = [];
    $form['#tree'] = TRUE;

    $form['returnContainer'] = [
      '#type' => 'container',
      '#id' => 'return-container',
    ];

    $form['returnContainer']['linkReturn'] = [
      '#type' => 'link',
      '#title' => $this->t('Página principal'),
      '#url' => Url::fromRoute('<front>'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
        ],
      ],
    ];

    $form['cliente'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cliente'),
      '#autocomplete_route_name' => 'sand_core.client_field',
      '#required' => TRUE,
    ];

    $materiales = $this->db->select(self::MATERIALES, 'cm')
      ->fields('cm')
      ->execute()
      ->fetchAll();
    $form_state->set('materiales', $materiales);
    $options = [];

    foreach ($materiales as $material) {
      $options[$material->id] = $material->material;
    }

    $form['wrapper_material'] = [
      '#type' => 'container',
      '#id' => 'ajax-wrapper-material',
    ];
    
    $form['wrapper_material']['material'] = [
      '#type' => 'select',
      '#title' => $this->t('Material'),
      '#options' => $options,
      '#ajax' => [
        'callback' => [$this, 'materialCallback'],
        'event' => 'change',
        'wrapper' => 'ajax-wrapper-material',
      ],  
    ];

    $form['wrapper_material']['metros_cubicos'] = [
      '#type' => 'number',
      '#title' => $this->t('Cantidad en m<sup>3</sup>'),
      '#ajax' => [
        'callback' => [$this, 'materialCallback'],
        'event' => 'change',
        'wrapper' => 'ajax-wrapper-material',
      ],
      '#required' => TRUE,
    ];

    $form['wrapper_material']['valor'] = [
      '#type' => 'number',
      '#title' => $this->t('Valor'),
      '#disabled' => TRUE,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Guardar',
      '#attributes' => [
        'class' => [
          'btn-basic'
        ]
      ]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function materialCallback(array &$form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    $materiales = $form_state->get('materiales');
    $info = [];
    $cantidad = $form_state->getValue(['wrapper_material','metros_cubicos']);
    $material = $form_state->getValue(['wrapper_material','material']);

    // Get materials.
    foreach ($materiales as $value) {
      if ($value->id == $material) {
        $info = $value;
      }
    }

    // Set default value.
    $valor = (float)$cantidad * (float)$info->valor_unitario;
    $form['wrapper_material']['valor']['#value'] = $valor + ($valor * 0.19);


    $form_state->setRebuild();
    return $form['wrapper_material'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $client = $form_state->getValue('cliente');
    $saldoMaterial = $form_state->getValue(['wrapper_material','metros_cubicos']);
    $material = $form_state->getValue(['wrapper_material','material']);
    $materiales = $form_state->get('materiales');
    $doc_type = $this->db->select(self::CLIENTES, 'cc')
      ->fields('cc', ['doc_type'])
      ->condition('id', $client, '=')
      ->execute()->fetch()->doc_type;

    // Get materials.
    foreach ($materiales as $value) {
      if ($value->id == $material) {
        $info = $value;
      }
    }

    try {
      if ($doc_type != 'NIT') {
        throw new \Exception('Usuario no puede realizar anticipos (tipo de documento)', 510);
      }
      // Set default value.
      $valor = (float)$saldoMaterial * (float)$info->valor_unitario;
      $value = $doc_type == 'NIT' ? $valor + ($valor * 0.19) : $valor;

      $vale = $this->config->get('vale');
      $this->config->set('vale', $vale + 1)
        ->save();
      $userInfo = $this->db->select(self::CLIENTES, 'cc')
        ->fields('cc', ['saldo', 'en_deuda', 'razon_social', 'cantidad_ant'])
        ->condition('id', $client, '=')
        ->execute()
        ->fetch();

      $clientBalance = $userInfo->saldo == NULL || empty($userInfo->saldo) 
        ? 0 : $userInfo->saldo;

      $date = new \DateTime();
      $date->setTimeStamp(\Drupal::time()->getCurrentTime());
      $date->setTimeZone(new \DateTimeZone('America/Bogota'));
      $this->db->insert(self::PAGOS)
        ->fields([
          'id_vale' => $vale,
          'id_cliente' => $client,
          'fecha' => $date->format('Y-m-d H:i:s'),
          'valor' => $value,
          'observaciones' => 'pago',
        ])
        ->execute();
      $description = 'Se realiza un anticipo para el cliente @client por un valor de @value';
      $tokens = [
        '@client' => $client,
        '@value' => $value,
      ];
      $this->audit->pushLog('insert', $this->t($description, $tokens), 'pay');


      $saldo = $clientBalance + $value;
      $enDedua = $saldo >= 0 ? 0 : 1;

      $this->db->update(self::CLIENTES)
        ->fields([
          'saldo' => $saldo,
          'en_deuda' => $enDeuda,
          'cantidad_ant' => (int) $saldoMaterial + (int)  $userInfo->cantidad_ant,
        ])
        ->condition('id', $client, '=')
        ->execute();

      if ($clientBalance >= 0 && $value > 1) {
        $this->db->insert(self::MOVEMENTS)
          ->fields([
            'id_cliente' => $client,
            'razon_social' => $userInfo->razon_social,
            'fecha' => $date->format('Y-m-d H:i:s'),
            'id_vale' => 0,
            'material' => '',
            'volumen' => 0,
            'valor_total' => 0,
            'anticipo' => 1,
            'valor_anticipo' => $value,
            'saldo' => $saldo,
            'saldo_material' => (int) $saldoMaterial + (int)  $userInfo->cantidad_ant,
            'tipo_salida' => 'anticipo',
            'volumen_anticipo' => $saldoMaterial,
          ])
          ->execute();
      }

      $description = 'Se actualiza el saldo a el cliente @client. Saldo: @saldo';
      $tokens = [
        '@client' => $client,
        '@saldo' => $saldo,
      ];
      $this->audit->pushLog('update', $this->t($description, $tokens), 'pay');
      
      drupal_set_message('Información guardada exitosamente.');
    }
    catch (\Exception $e) {
      $message = $e->getCode() != 510 
        ? 'Error al guardar la información.' 
        : $e->getMessage();
      drupal_set_message($message, 'error');
    }
  }

}