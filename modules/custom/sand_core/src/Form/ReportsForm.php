<?php

namespace Drupal\sand_core\Form;

use Drupal\sand_core\Services\ReportsServices;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ReportsForm extends FormBase {

  /**
   * Connection to data base.
   * 
   * @var ReportsServices $connection
   */
  protected $service;

  /**
   * Get module configuration.
   * 
   * @var ConfigFactoryInterface $config
   */
  protected $config;

  /**
   * Cache page.
   * 
   * @var ReportsServices $connection
   */
  protected $kill;

  /**
   * {@inheritdoc}
   */
  public function __construct(ReportsServices $service, 
                              ConfigFactoryInterface $config,
                              KillSwitch $kill) {
    $this->service = $service;
    $this->config = $config->get('sand_core.config');
    $this->kill = $kill;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sand_core.repot_query'),
      $container->get('config.factory'),
      $container->get('page_cache_kill_switch')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sand_core_reports';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form,FormStateInterface $form_state) {
    $this->kill->trigger();
    $form = [];
    $form['#tree'] = TRUE;

    $form['#attached']['library'][] = 'sand_core/reports_script';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form,FormStateInterface $form_state) {
  }
}