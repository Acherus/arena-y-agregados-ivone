<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Drupal\sand_core\Services\AuditServices;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

class ModifyPayForm extends FormBase {

  const CLIENTES = 'core_clientes',
        PAGOS = 'core_pagos',
        MOVEMENTS = 'core_movements',
        MATERIALES = 'core_materiales';

  /**
   * Connection to data base.
   * 
   * @var \Drupal\Core\Database\Connection $db
   */
  protected $db;

  /**
   * @var AuditServices $audit
   */
  protected $audit;

  /**
   * Get module configuration.
   * 
   * @var ConfigFactoryInterface $config
   */
  protected $config;

  /**
   * Private tempstore.
   * 
   * @var PrivateTempStoreFactory $store
   */
  protected $store;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, 
                              AuditServices $audit,
                              ConfigFactoryInterface $config,
                              PrivateTempStoreFactory $store) {
    $this->db = $connection;
    $this->audit = $audit;
    $this->config = $config->getEditable('sand_core.config');
    $this->store = $store->get('modify_material');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('sand_core.audit_services'),
      $container->get('config.factory'),
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sand_core_pagos_anticipos';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $form = [];
    $form['#tree'] = TRUE;

    $form['returnContainer'] = [
      '#type' => 'container',
      '#id' => 'return-container',
    ];

    $form['returnContainer']['linkReturn'] = [
      '#type' => 'link',
      '#title' => $this->t('Página principal'),
      '#url' => Url::fromRoute('<front>'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
        ],
      ],
    ];

    $form['cliente'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cliente'),
      '#autocomplete_route_name' => 'sand_core.client_field',
      '#ajax' => [
        'callback' => [$this, 'clientCallback'],
        'event' => 'change',
        'wrapper' => 'ajax-wrapper',
      ],
      '#required' => TRUE,
    ];

    $form['wrapper'] = [
      '#type' => 'container',
      '#id' => 'ajax-wrapper',
    ];    

    $form['wrapper']['razon_social'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Razón social'),
      '#disabled' => TRUE,
      '#required' => TRUE,
    ];

    $form['wrapper']['saldo'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Saldo actual'),
      '#disabled' => TRUE,
      '#required' => TRUE,
    ];

    $form['wrapper']['saldo_metros'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Saldo actual m<sup>3<sup>'),
      '#disabled' => TRUE,
      '#required' => TRUE,
    ];

    $form['change']['saldo_pesos'] = [
      '#type' => 'number',
      '#title' => $this->t('Saldo nuevo del cliente $'),
    ];

    $form['change']['saldo_material'] = [
      '#type' => 'number',
      '#title' => $this->t('Saldo nuevo del cliente m<sup>3<sup>'),
    ];

    $form['change']['id_registro'] = [
      '#type' => 'number',
      '#title' => $this->t('ID del registro a modificar'),
    ];

    $form['change']['saldo_registro'] = [
      '#type' => 'number',
      '#title' => $this->t('Saldo nuevo del registro $'),
    ];

    $form['change']['volumen_registro'] = [
      '#type' => 'number',
      '#title' => $this->t('Saldo nuevo del registro m<sup>3<sup>'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Guardar',
      '#attributes' => [
        'class' => [
          'btn-basic'
        ]
      ]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function clientCallback(array $form, FormStateInterface $form_state) {
    $id = $form_state->getValue('cliente');

    $data = $this->db->select(self::CLIENTES, 'cc')
      ->fields('cc', ['id', 'saldo', 'razon_social', 'en_deuda', 'cantidad_ant'])
      ->condition('id', $id, '=')
      ->execute()
      ->fetch();

    if (!empty($data)) {
      $form_state->set('user_data', $data);
      $this->store->set('user_data', $data);
      $form['wrapper']['razon_social']['#value'] = $data->razon_social;
      $form['wrapper']['saldo']['#value'] = $data->saldo;
      $form['wrapper']['saldo_metros']['#value'] = $data->cantidad_ant;

      $query = $this->db->select(self::MOVEMENTS, 'mv');
      $query->fields('mv', [
        'id',
        'valor_anticipo',
        'volumen_anticipo'
      ])
        ->condition('anticipo', 1, '=')
        ->condition('id_cliente', $data->id, '=');
      $query->range(0,3);
      $query->orderBy('id', 'DESC');

      $lastMovements = $query->execute()->fetchAll();

      if (!empty($lastMovements)) {
        $form['wrapper']['table'] = [
          '#type' => 'table',
          '#header' => ['ID registro','Valor del anticipo', 'Volumen del anticipo'],
        ];

        foreach ($lastMovements as $key => $movement) {
          $form['wrapper']['table'][$key]['ID registro'] = [
            '#type' => 'label',
            '#title' => $movement->id,
          ];

          $form['wrapper']['table'][$key]['Volumen del anticipo'] = [
            '#type' => 'label',
            '#title' => $movement->valor_anticipo,
          ];

          $form['wrapper']['table'][$key]['Valor del anticipo'] = [
            '#type' => 'label',
            '#title' => $movement->volumen_anticipo,
          ];
        }
      }
    }

    $form_state->setRebuild();
    return $form['wrapper'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $client = $form_state->getValue('cliente');

    // Saldo de cliente nuevos.
    $saldo = $form_state->getValue(['change','saldo_pesos']);
    $saldoMaterial = $form_state->getValue(['change','saldo_material']);
    
    // Saldo para el registro.
    $idRegistro = $form_state->getValue(['change','id_registro']);
    $saldoRe = $form_state->getValue(['change','saldo_registro']);
    $saldoReMaterial = $form_state->getValue(['change','volumen_registro']);

    try {

      if (
        empty($saldo) &&
        empty($saldoMaterial) &&
        empty($idRegistro) &&
        empty($saldoRe) &&
        empty($saldoReMaterial)) {
        throw new \Exception('No hay datos que actualizar', 516);
      }

      if (!empty($saldo) && !empty($saldoMaterial) ) {
        $query = $this->db->update(self::CLIENTES);
        $query->fields([
          'saldo' => $saldo,
          'cantidad_ant' => $saldoMaterial
        ])
          ->condition('id', $client, '=');

        $query->execute();

        $description = 'Se actualiza el saldo a el cliente @client. Saldo: @saldo Saldo: @mcubicos';
        $tokens = [
          '@client' => $client,
          '@saldo' => $saldo,
          '@mcubicos' => $saldoMaterial,
        ];
        $this->audit->pushLog('update', $this->t($description, $tokens), 'Update Data');

        drupal_set_message('Se ha actualizado la información del cliente.', 'warning');
      }

      if (!empty($idRegistro) && !empty($saldoRe) && !empty($saldoReMaterial)) {
        $query = $this->db->update(self::MOVEMENTS);
        $query->fields([
          'valor_anticipo' => $saldoRe,
          'volumen_anticipo' => $saldoReMaterial,
        ])
          ->condition('id', $idRegistro, '=');

        $query->execute();

        $description = 'Se actualiza el registro @id del cliente @client. Saldo: @saldo Saldo: @mcubicos';
        $tokens = [
          '@id' => $id,
          '@client' => $client,
          '@saldo' => $saldoRe,
          '@mcubicos' => $saldoReMaterial,
        ];
        $this->audit->pushLog('update', $this->t($description, $tokens), 'Update Register');

        drupal_set_message('Se ha actualizado la información del registro.', 'warning');
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('catch')->notice(print_r($e->getMessage(), 1));
      $message = 'Error al guardar la información.';
      $message = $e->getCode() == 516 ? $e->getMessage() : $message;

      drupal_set_message($message, 'error');
    }
  }

}