<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\sand_core\Services\QueryServices;

class DisableClientForm extends FormBase {
  // Define data base table name;
  const CLIENTES = 'core_clientes';
  const TELEFONOS = 'core_telefonos';
  const CONDUCTORES = 'core_conductores';

  /**
   * Connection to data base.
   * 
   * @var Connection $connection
   */
  protected $connection;

  /**
   * Get module configuration.
   * 
   * @var ConfigFactoryInterface $config
   */
  protected $config;

  /**
   * Query services.
   * 
   * @var QueryServices $services
   */
  protected $services;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, 
                              ConfigFactoryInterface $config,
                              QueryServices $services) {
    $this->connection = $connection;
    $this->config = $config->get('sand_core.config');
    $this->services = $services;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('config.factory'),
      $container->get('sand_core.query_services')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sand_core_disable_client';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $form = [];

    if (!isset($_GET['cliente']) || empty($_GET['cliente'])) {
      return new RedirectResponse(Url::fromRoute('sand_core.clients_list')->toString());
    }

    $data = $this->services->getAllClientInformation($_GET['cliente']);
    $clientData = $data['clientData'];

    $tokens = [
      '@name' => $clientData->razon_social,
      '@id' => $clientData->id
    ];

    $form['message'] = [
      '#markup' => $this->t('<p class="message-delete">¿Desea deshabilitar al cliente @name con el id @id?<p>', $tokens),
    ];

    $form ['to_disable'] = [
      '#type' => 'hidden',
      '#value' => $clientData->id
    ];

    $form['actions'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['action-wrapper']
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Deshabilitar',
      '#attributes' => [
        'class' => [
          'btn-basic',
        ],
      ],
    ];

    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancelar'),
      '#url' => Url::fromRoute('sand_core.clients_list'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
          'gray-btn'
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $id = $form_state->getValue('to_disable');

    try {
      $query = $this->connection->update(self::CLIENTES);
      $query->condition('id', $id, '=')
        ->fields([
          'state' => 0,
        ])
        ->execute();

      drupal_set_message($this->t('El usuario con el id @id ha sido deshabilitado', ['@id'=> $id]));
      $form_state->setRedirect('sand_core.clients_list');
    }
    catch (\Exception $e) {
      drupal_set_message($this->t('Ha ocurrido un error al deshabilitar el usuario con el id @id', ['@id'=> $id]), 'error');
    }
  }
}