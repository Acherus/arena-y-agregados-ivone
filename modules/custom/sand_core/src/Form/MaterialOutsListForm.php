<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;


class MaterialOutsListForm extends FormBase {
  const DESPACHOS = 'core_despachos',
        CLIENTES = 'core_clientes';

  /**
   * Connection to data base.
   * 
   * @var Connection $connection
   */
  protected $db;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection) {
    $this->db = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sand_core_material_outs_list';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $form = [];
    $form['#tree'] = TRUE;

    $form['outs'] = [
      '#type' => 'link',
      '#title' => $this->t('Generar salida'),
      '#url' => Url::fromRoute('sand_core.salidas'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
        ],
      ],
    ];

    $query = $this->db->select(self::DESPACHOS, 'cd')
      ->fields('cd');
    $query->innerJoin(self::CLIENTES, 'cc', 'cd.id_cliente = cc.id');
    $query->fields('cc');
    $data = $query->execute()
      ->fetchAll();
  
    $form['outs_table'] = [
      '#type' => 'table',
      '#header' => ['']
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }
}