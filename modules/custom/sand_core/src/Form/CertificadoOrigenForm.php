<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

class CertificadoOrigenForm extends FormBase {

  public function getFormId() {
    return 'sand_core_certificado_origen';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $form = $data = [];
    $form['#tree'] = TRUE;

    if (isset($_GET['data'])) {
      $data = (array) json_decode(base64_decode(urldecode($_GET['data'])));
      $tempstore = \Drupal::service('tempstore.private');
      $sRemision = $tempstore->get('data_remision');
      $sRemision->set('remision', $data['remision']);
    }

    $form['returnContainer'] = [
      '#type' => 'container',
      '#id' => 'return-container',
    ];

    if (isset($_GET['history'])) {
      $form['returnContainer']['linkReturn'] = [
        '#type' => 'link',
        '#title' => $this->t('Historial'),
        '#url' => Url::fromRoute('sand_core.history_form'),
        '#attributes' => [
          'class' => [
            'btn-basic',
            'list-table',
          ],
        ],
      ];
    }
    else {
      $form['returnContainer']['linkReturn'] = [
        '#type' => 'link',
        '#title' => $this->t('Pagina principal'),
        '#url' => Url::fromRoute('<front>'),
        '#attributes' => [
          'class' => [
            'btn-basic',
            'list-table',
          ],
        ],
      ];
    }
    

    if (isset($data['review']) && $data['review'] == 1) {
      $form['returnContainer']['print'] = [
        '#type' => 'label',
        '#title' => $this->t('Imprimir'),
        '#attributes' => [
          'class' => [
            'btn-basic',
            'list-table',
            'disabled',
          ],
          'id' => 'print-cert-btn'
        ],
      ];
    }
    else {
      $form['returnContainer']['print'] = [
        '#type' => 'submit',
        '#value' => $this->t('Imprimir'),
        '#attributes' => [
          'class' => [
            'btn-basic',
            'list-table',
            'disabled',
          ],
          'id' => 'print-cert-btn'
        ],
        '#submit' => [[$this, 'saveData']]
      ];
    }

    $tempstore = \Drupal::service('tempstore.private');
    $store = $tempstore->get('certificado');
    $store->set('anulado', $_GET['status']);
    if (isset($_GET['status']) && $_GET['status'] == 0) {
      $form['disabled'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['disabled-mark','dis-cert'],
        ],
      ];
      $form['disabled']['anulado'] = [
        '#type' => 'label',
        '#title' => 'ANULADO',
        '#attributes' => [
          'class' => ['anulado'],
        ],
      ];
    }
    // Table of cert.
    $form['table'] = [
      '#type' => 'table',
      '#id' => 'origin-cert',
    ];

    $form['table']['firstRow']['img'] = [
      '#type' => 'html_tag',
      '#tag' => 'img',
      '#attributes' => [
        'src' => drupal_get_path('module', 'sand_core') . '/images/anm.png',
      ],
      '#id' => 'anm-img',
      '#wrapper_attributes' => [
        'colspan' => 4,
      ],
    ];

    $form['table']['firstRow']['title-wrapper'] = [
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => [
          'head-doc',
        ],
      ],
    ];

    $form['table']['firstRow']['title-wrapper']['title'] = [
      '#type' => 'label',
      '#title' => $this->t('VICEPRESIDENCIA DE SEGUIMIENTO CONTROL Y SEGURIDAD MINERA'),
      '#attributes' => [
        'id' => 'title',
      ],
    ];

    $form['table']['firstRow']['title-wrapper']['group'] = [
      '#type' => 'label',
      '#title' => $this->t('Grupo de Regalías y Contraprestaciones Económicas'),
      '#attributes' => [
        'id' => 'description',
      ],
    ];

    $form['table']['secondRow']['title'] = [
      '#type' => 'label',
      '#title' => $this->t('CERTIFICADO DE ORIGEN EXPLOTADOR MINERO AUTORIZADO'),
      '#wrapper_attributes' => [
        'colspan' => 9,
        'class' => [
          'title-cert'
        ],
      ],
    ];

    $form['table']['thirdRow']['dateLabel'] = [
      '#type' => 'label',
      '#title' => $this->t('FECHA'),
      '#attributes' => [
        'id' => 'date-label',
      ],
      '#wrapper_attributes' => [
        'rowspan' => 2,
        'colspan' => 2,
        'class' => [
          'green-cell',
        ],
      ],
    ];

    $form['table']['thirdRow']['day'] = [
      '#type' => 'label',
      '#title' => $this->t('DD'),
      '#attributes' => [
        'class' => ['date-labels'],
        'id' => 'day-label',
      ],
      '#wrapper_attributes' => [
        'class' => [
          'date-l-wrapper'
        ],
      ],
    ];
    
    $form['table']['thirdRow']['month'] = [
      '#type' => 'label',
      '#title' => $this->t('MM'),
      '#attributes' => [
        'class' => ['date-labels'],
        'id' => 'month-label',
      ],
      '#wrapper_attributes' => [
        'class' => [
          'date-l-wrapper'
        ],
      ],
    ];

    $form['table']['thirdRow']['year'] = [
      '#type' => 'label',
      '#title' => $this->t('AAAA'),
      '#attributes' => [
        'id' => 'year-label',
        'class' => ['date-labels'],
      ],
      '#wrapper_attributes' => [
        'class' => [
          'date-l-wrapper'
        ],
      ],
    ];

    $form['table']['thirdRow']['consecutive'] = [
      '#type' => 'label',
      '#title' => $this->t(' No. Consecutivo del certificado de origen'),
      '#attributes' => [
        'id' => 'num-consecutive',
      ],
      '#wrapper_attributes' => [
        'rowspan' => 2,
        'colspan' => 2,
        'class' => [
          'green-cell',
        ],
      ],
    ];

    $form['table']['thirdRow']['consecutiveInput'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'id' => 'consecutive-input'
      ],
      '#wrapper_attributes' => [
        'rowspan' => 2,
        'colspan' => 2,
      ],
      '#default_value' => isset($data['consecutive']) 
        ? $data['consecutive'] : ''
    ];

    
    $date = isset($data['fecha']) 
      ? explode('-', $data['fecha'])
      : [];

    $form['table']['fourthRow']['day'] = [
      '#type' => 'number',
      '#attributes' => [
        'id' => 'day-input',
      ],
      '#wrapper_attributes' => [
        'class' => [
          'date-formater'
        ],
      ],
      '#default_value' => isset($date[2]) ? $date[2] : '',
    ];

    $form['table']['fourthRow']['month'] = [
      '#type' => 'number',
      '#attributes' => [
        'id' => 'month-input',
      ],
      '#wrapper_attributes' => [
        'class' => [
          'date-formater'
        ],
      ],
      '#default_value' => isset($date[1]) ? $date[1] : '',
    ];

    $form['table']['fourthRow']['year'] = [
      '#type' => 'number',
      '#attributes' => [
        'id' => 'year-input',
      ],
      '#wrapper_attributes' => [
        'class' => [
          'date-formater'
        ],
      ],
      '#default_value' => isset($date[0]) ? $date[0] : '',
    ];

    $form['table']['fifth']['title'] = [
      '#type' => 'label',
      '#title' => $this->t('INFORMACIÓN DEL PRODUCTOR DEL MINERAL'),
      '#wrapper_attributes' => [
        'colspan' => 9,
        'class' => [
          'title-cert'
        ],
      ],
    ];

    $form['table']['sixth']['authMiner'] = [
      '#wrapper_attributes' => [
        'rowspan' => 2,
        'colspan' => 5,
        'class' => [
          'green-cell',
        ],
      ],
    ];

    $form['table']['sixth']['authMiner']['title'] = [
      '#type' => 'label',
      '#title' => $this->t('EXPLOTADOR MINERO AUTORIZADO'),
      '#attributes' => [
        'class' => ['bold'],
      ],
    ];

    $form['table']['sixth']['authMiner']['description'] = [
      '#type' => 'label',
      '#title' => $this->t('(Seleccione una opción)'),
    ];

    $form['table']['sixth']['titular'] = [
      '#wrapper_attributes' => [
        'colspan' => 2,
        'class' => [
          'green-cell',
          'explode',
        ],
      ]
    ];
    $form['table']['sixth']['titular']['titularTitle'] = [
      '#type' => 'label',
      '#title' => $this->t('Titular Minero'),
      '#attributes' => [
        'class' => [
          'bold',
          'left',
        ],
      ],
    ];

    $form['table']['sixth']['titular']['titularOpt'] = [
      '#type' => 'checkbox',
      '#default_value' => TRUE,
      '#theme_wrappers' => [
        'form-element-checkbox'
      ],
      '#attributes' => [
        'class' => ['sand-check'],
      ],
      '#wrapper_attributes' => [
        'class' => ['input-explode'],
      ],
    ];

    $form['table']['sixth']['benefit'] = [
      '#wrapper_attributes' => [
        'colspan' => 2,
        'class' => [
          'green-cell',
          'explode',
        ],
      ]
    ];

    $form['table']['sixth']['benefit']['benefitTitle'] = [
      '#type' => 'label',
      '#title' => $this->t('Beneficiario de Área de Reserva Especial'),
      '#attributes' => [
        'class' => [
          'bold',
          'left',
          'bnf-esp',
        ],
      ],
    ];

    $form['table']['sixth']['benefit']['benefitOpt'] = [
      '#type' => 'checkbox',
      '#theme_wrappers' => [
        'form-element-checkbox'
      ],
      '#attributes' => [
        'class' => ['sand-check'],
      ],
      '#wrapper_attributes' => [
        'class' => ['input-explode'],
      ],
    ];

    $form['table']['seventh']['legalization'] = [
      '#wrapper_attributes' => [
        'colspan' => 2,
        'class' => [
          'green-cell',
          'explode',
        ],
      ]
    ];

    $form['table']['seventh']['legalization']['legalizationTitle'] = [
      '#type' => 'label',
      '#title' => $this->t('Solicitante de  Legalización'),
      '#attributes' => [
        'class' => [
          'bold',
          'left',
        ],
      ],
    ];

    $form['table']['seventh']['legalization']['legalizationOpt'] = [
      '#type' => 'checkbox',
      '#theme_wrappers' => [
        'form-element-checkbox'
      ],
      '#attributes' => [
        'class' => ['sand-check'],
      ],
      '#wrapper_attributes' => [
        'class' => ['input-explode'],
      ],
    ];

    $form['table']['seventh']['subcontract'] = [
      '#wrapper_attributes' => [
        'colspan' => 2,
        'class' => [
          'green-cell',
          'explode',
        ],
      ]
    ];

    $form['table']['seventh']['subcontract']['subcontractTitle'] = [
      '#type' => 'label',
      '#title' => $this->t('Subcontrato de  Formalización'),
      '#attributes' => [
        'class' => [
          'bold',
          'left',
        ],
      ],
    ];

    $form['table']['seventh']['subcontract']['subcontractOpt'] = [
      '#type' => 'checkbox',
      '#theme_wrappers' => [
        'form-element-checkbox'
      ],
      '#attributes' => [
        'class' => ['sand-check'],
      ],
      '#wrapper_attributes' => [
        'class' => ['input-explode'],
      ],
    ];

    $form['table']['eighth']['code'] = [
      '#type' => 'label',
      '#title' => $this->t('CÓDIGO EXPEDIENTE'),
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => [
          'title-white',
        ],
      ],
    ];

    $form['table']['eighth']['codeInput'] = [
      '#type' => 'textfield',
      '#default_value' => 'BK9-141',
      '#wrapper_attributes' => [
        'colspan' => 4,
        'class' => [
          'input-white',
          'bold',
        ],
      ],
    ];

    $form['table']['ninth']['name'] = [
      '#type' => 'label',
      '#title' => $this->t('NOMBRES Y APELLIDOS O RAZON SOCIAL DEL EXPLOTADOR MINERO AUTORIZADO'),
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => [
          'title-white',
        ],
      ],
    ];

    $form['table']['ninth']['nameInput'] = [
      '#type' => 'label',
      '#title' => 'IVONE MONTOYA BELLO - ARENAS Y AGREGADOS IVONE S.A.S',
      '#wrapper_attributes' => [
        'colspan' => 4,
        'class' => [
          'input-white',
          //'bold',
        ],
      ],
    ];

    $form['table']['tenth']['type'] = [
      '#type' => 'label',
      '#title' => $this->t('TIPO DE IDENTIFICACIÓN DEL EXPLOTADOR MINERO AUTORIZADOR'),
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => [
          'title-white',
        ],
      ],
    ];

    $form['table']['tenth']['nit'] = [
      '#wrapper_attributes' => [
        'class' => [
          'input-check-white',
        ],
      ],
    ];

    $form['table']['tenth']['nit']['label'] = [
      '#type' => 'label',
      '#title' => $this->t('NIT'),
      '#attributes' => [
        'class' => ['left', 'nit-label'],
      ],
    ];

    $form['table']['tenth']['nit']['check'] = [
      '#type' => 'checkbox',
      '#default_value' => TRUE,
      '#theme_wrappers' => [
        'form-element-checkbox'
      ],
    ];

    $form['table']['tenth']['cc'] = [
      '#wrapper_attributes' => [
        'class' => [
          'input-check-white',
        ],
      ],
    ];

    $form['table']['tenth']['cc']['label'] = [
      '#type' => 'label',
      '#title' => $this->t('CÉDULA'),
      '#attributes' => [
        'class' => ['left', 'cc-label'],
      ],
    ];

    $form['table']['tenth']['cc']['check'] = [
      '#type' => 'checkbox',
      '#default_value' => TRUE,
      '#theme_wrappers' => [
        'form-element-checkbox'
      ],
    ];

    $form['table']['tenth']['ce'] = [
      '#wrapper_attributes' => [
        'class' => [
          'input-check-white',
        ],
      ],
    ];

    $form['table']['tenth']['ce']['label'] = [
      '#type' => 'label',
      '#title' => $this->t('CÉDULA DE EXTRANJERÍA'),
      '#attributes' => [
        'class' => [
          'left',
          'ce',
        ],
      ],
    ];

    $form['table']['tenth']['ce']['check'] = [
      '#type' => 'checkbox',
      '#theme_wrappers' => [
        'form-element-checkbox'
      ],
    ];

    $form['table']['tenth']['rut'] = [
      '#wrapper_attributes' => [
        'class' => [
          'input-check-white',
        ],
      ],
    ];

    $form['table']['tenth']['rut']['label'] = [
      '#type' => 'label',
      '#title' => $this->t('RUT'),
      '#attributes' => [
        'class' => ['left', 'rut-label'],
      ],
    ];

    $form['table']['tenth']['rut']['check'] = [
      '#type' => 'checkbox',
      '#theme_wrappers' => [
        'form-element-checkbox'
      ],
    ];

    $form['table']['eleventh']['document'] = [
      '#type' => 'label',
      '#title' => $this->t('No. DOCUMENTO DE IDENTIDAD  DEL EXPLOTADOR MINERO AUTORIZADOR'),
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => [
          'title-white',
        ],
      ],
    ];

    $form['table']['eleventh']['documentInput'] = [
      '#type' => 'textfield',
      '#default_value' => 'C.C. 39.660.625 - NIT: 900.801.033-5',
      '#wrapper_attributes' => [
        'colspan' => 4,
        'class' => [
          'input-white',
          'bold',
        ],
      ],
    ];

    $form['table']['twelfth']['state'] = [
      '#type' => 'label',
      '#title' => $this->t('DEPARTAMENTO (S) DONDE REALIZA LA EXPLOTACIÓN'),
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => [
          'title-white',
        ],
      ],
    ];

    $form['table']['twelfth']['stateInput'] = [
      '#type' => 'textfield',
      '#default_value' => 'CUNDINAMARCA',
      '#wrapper_attributes' => [
        'colspan' => 4,
        'class' => [
          'input-white',
          'bold',
        ],
      ],
    ];

    $form['table']['thirteenth']['town'] = [
      '#type' => 'label',
      '#title' => $this->t('MUNICIPIO(S) DONDE SE REALIZA LA EXPLOTACION'),
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => [
          'title-white',
        ],
      ],
    ];

    $form['table']['thirteenth']['townInput'] = [
      '#type' => 'textfield',
      '#default_value' => 'SOACHA',
      '#wrapper_attributes' => [
        'colspan' => 4,
        'class' => [
          'input-white',
          'bold',
        ],
      ],
    ];

    $form['table']['fourteenth']['material'] = [
      '#type' => 'label',
      '#title' => $this->t('MINERAL EXPLOTADO'),
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => [
          'title-white',
        ],
      ],
    ];

    $form['table']['fourteenth']['materialInput'] = [
      '#type' => 'textfield',
      '#default_value' => isset($data['material']) 
        ? $data['material'] : '',
      '#wrapper_attributes' => [
        'colspan' => 4,
        'class' => [
          'input-white',
          'bold',
        ],
      ],
    ];

    $form['table']['fifteenth']['quantity'] = [
      '#type' => 'label',
      '#title' => $this->t('CANTIDAD MINERAL COMERCIALIZADO'),
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => [
          'title-white',
        ],
      ],
    ];

    $form['table']['fifteenth']['quantityInput'] = [
      '#type' => 'textfield',
      '#default_value' => isset($data['metros']) 
        ? $data['metros'] : '',
      '#wrapper_attributes' => [
        'colspan' => 4,
        'class' => [
          'input-white',
          'bold',
        ],
      ],
    ];

    $form['table']['sixteenth']['unit'] = [
      '#type' => 'label',
      '#title' => $this->t('UNIDAD DE MEDIDA'),
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => [
          'title-white',
        ],
      ],
    ];

    $form['table']['sixteenth']['unitInput'] = [
      '#type' => 'label',
      '#title' => $this->t('m&#x00B3'),
      '#attributes' => [
        'id' => 'unit'
      ],
      '#wrapper_attributes' => [
        'colspan' => 4,
        'class' => [
          'input-white',
          'bold'
        ],
      ],
    ];

    $form['table']['seventeenth']['title'] = [
      '#type' => 'label',
      '#title' => $this->t('INFORMACIÓN DEL COMPRADOR DEL MINERAL'),
      '#wrapper_attributes' => [
        'colspan' => 9,
        'class' => [
          'title-cert'
        ],
      ],
    ];

    $form['table']['eighteenth']['nameOut'] = [
      '#type' => 'label',
      '#title' => $this->t('NOMBRES Y APELLIDOS O RAZON SOCIAL'),
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => [
          'green-cell',
        ],
      ],
    ];

    $form['table']['eighteenth']['nameOutInput'] = [
      '#type' => 'textfield',
      '#default_value' => isset($data['razonSocial']) 
        ? $data['razonSocial'] : '',
      '#wrapper_attributes' => [
        'colspan' => 4,
        'class' => [
          'input-white',
        ],
      ],
    ];

    $form['table']['nineteenth']['nameOut'] = [
      '#type' => 'label',
      '#title' => $this->t('TIPO DE IDENTIFICACIÓN'),
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => [
          'green-cell',
        ],
      ],
    ];

    $form['table']['nineteenth']['nit'] = [
      '#wrapper_attributes' => [
        'class' => [
          'input-check-white',
        ],
      ],
    ];

    $form['table']['nineteenth']['nit']['label'] = [
      '#type' => 'label',
      '#title' => $this->t('NIT'),
      '#attributes' => [
        'class' => ['left', 'nit-label'],
      ],
    ];

    $form['table']['nineteenth']['nit']['check'] = [
      '#type' => 'checkbox',
      '#theme_wrappers' => [
        'form-element-checkbox'
      ],
      '#attributes' => [
        'class'=> [
          'to-watch',
        ],
        'id' => 'doc-client-nit',
      ],
    ];

    $form['table']['nineteenth']['cc'] = [
      '#wrapper_attributes' => [
        'class' => [
          'input-check-white',
        ],
      ],
    ];

    $form['table']['nineteenth']['cc']['label'] = [
      '#type' => 'label',
      '#title' => $this->t('CÉDULA'),
      '#attributes' => [
        'class' => ['left', 'cc-label'],
      ],
    ];

    $form['table']['nineteenth']['cc']['check'] = [
      '#type' => 'checkbox',
      '#theme_wrappers' => [
        'form-element-checkbox'
      ],
      '#attributes' => [
        'class'=> [
          'to-watch',
        ],
        'id' => 'doc-client-cc',
      ],
    ];

    $form['table']['nineteenth']['ce'] = [
      '#wrapper_attributes' => [
        'class' => [
          'input-check-white',
        ],
      ],
    ];

    $form['table']['nineteenth']['ce']['label'] = [
      '#type' => 'label',
      '#title' => $this->t('CÉDULA DE EXTRANJERÍA'),
      '#attributes' => [
        'class' => [
          'left',
          'ce',
        ],
      ],
    ];

    $form['table']['nineteenth']['ce']['check'] = [
      '#type' => 'checkbox',
      '#theme_wrappers' => [
        'form-element-checkbox'
      ],
      '#attributes' => [
        'class'=> [
          'to-watch',
        ],
        'id' => 'doc-client-ce',
      ],
    ];

    $form['table']['nineteenth']['rut'] = [
      '#wrapper_attributes' => [
        'class' => [
          'input-check-white',
        ],
      ],
    ];

    $form['table']['nineteenth']['rut']['label'] = [
      '#type' => 'label',
      '#title' => $this->t('RUT'),
      '#attributes' => [
        'class' => ['left', 'rut-label'],
      ],
    ];

    $form['table']['nineteenth']['rut']['check'] = [
      '#type' => 'checkbox',
      '#theme_wrappers' => [
        'form-element-checkbox'
      ],
      '#attributes' => [
        'class'=> [
          'to-watch',
        ],
        'id' => 'doc-client-rut',
      ],
    ];

    if (isset($data['cnit'])) {
      $form['table']['nineteenth']['nit']['check']['#default_value'] = $data['cnit'];
    }

    if (isset($data['ccc'])) {
      $form['table']['nineteenth']['cc']['check']['#default_value'] = $data['ccc'];
    }

    if (isset($data['cce'])) {
      $form['table']['nineteenth']['ce']['check']['#default_value'] = $data['cce'];
    }

    if (isset($data['crut'])) {
      $form['table']['nineteenth']['rut']['check']['#default_value'] = $data['crut'];
    }



    $form['table']['twentieth']['buyer'] = [
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => [
          'green-cell',
        ],
      ],
    ];

    $form['table']['twentieth']['buyer']['title'] = [
      '#type' => 'label',
      '#title' => $this->t('COMPRADOR'),
    ];

    $form['table']['twentieth']['buyer']['description'] = [
      '#type' => 'label',
      '#title' => $this->t('(Seleccione una oopción)'),
    ];

    $form['table']['twentieth']['dealerOpt'] = [
      '#type' => 'label',
      '#title' => $this->t('COMERCIALIZADOR'),
      '#id' => 'dealer',
      '#wrapper_attributes' => [
        'colspan' => 2,
        'class' => [
          'select-option',
          //'to-watch',
        ],
        'id' => 'client-reazon-dealer',
      ],
      '#ajax' => [
        'callback' => [$this, 'saveType'],
        'event' => 'onclick',
      ],
    ];

    $form['table']['twentieth']['customerOpt'] = [
      '#type' => 'label',
      '#title' => $this->t('CONSUMIDOR'),
      '#id' => 'customer',
      '#wrapper_attributes' => [
        'colspan' => 2,
        'class' => [
          'select-option',
          //'to-watch',
        ],
        'id' => 'client-reazon-customer',
      ],
      '#ajax' => [
        'callback' => [$this, 'saveType'],
        'event' => 'onclick',
      ],
    ];

    if (isset($data['anmType'])) {
      if ($data['anmType'] == 'customer') {
        $form['table']['twentieth']['customerOpt']['#wrapper_attributes']['class'][] = 'selected';
      }
      else {
        $form['table']['twentieth']['dealerOpt']['#wrapper_attributes']['class'][] = 'selected';
      }
    }

    $form['table']['twentyFirst']['document'] = [
      '#type' => 'label',
      '#title' => $this->t('No. DOCUMENTO DE IDENTIDAD'),
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => [
          'green-cell',
        ],
      ],
    ];

    $form['table']['twentyFirst']['documentOutInput'] = [
      '#type' => 'textfield',
      '#default_value' => isset($data['id']) 
      ? $data['id'] : '',
      '#attributes' => [
        'class' => [
          'to-watch',
        ],
        'id' => 'client-doc-input',
      ],
      '#wrapper_attributes' => [
        'colspan' => 4,
        'class' => [
          'input-white',
        ],
      ],
    ];

    $form['table']['twentySecond']['rucom'] = [
      '#type' => 'label',
      '#title' => $this->t('No. RUCOM'),
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => [
          'green-cell',
        ],
      ],
    ];

    $form['table']['twentySecond']['rucomInput'] = [
      '#type' => 'textfield',
      '#default_value' => 'RUCOM-',
      '#wrapper_attributes' => [
        'colspan' => 4,
        'class' => [
          'input-white',
        ],
      ],
    ];

    if (isset($data['rucom'])) {
      $form['table']['twentySecond']['rucomInput']['#default_value'] = $data['rucom'];
    }

    $form['table']['twentyThird']['sign'] = [
      '#type' => 'label',
      '#title' => $this->t('FIRMA DEL EXPLOTADOR MINERO AUTORIZADO'),
      '#wrapper_attributes' => [
        'colspan' => 5,
        'class' => [
          'title-cert',
          'bold',
        ],
      ],
    ];

    $config = \Drupal::config('sand_core.config');
    $image = $config->get('sign');

    if (!empty($image[0])) {
      if ($file = File::load($image[0])) {
        $build['image'] = [
          '#theme' => 'image_style',
          '#style_name' => 'medium',
          '#uri' => $file->getFileUri(),
        ];

        $sRemision->set('sign', file_url_transform_relative(file_create_url($file->getFileUri())));

        $form['table']['twentyThird']['signInput'] = [
          '#default_value' => '',
          '#wrapper_attributes' => [
            'colspan' => 4,
            'class' => [
              'input-white',
              'sign-img',
            ],
          ],
          '#id' => 'sign-img',
          '#theme' => 'image',
          '#style_name' => 'medium',
          '#uri' => $file->getFileUri(),
        ];
      }
    }
    else {
      $form['table']['twentyThird']['signInput'] = [
        '#type' => 'textfield',
        '#default_value' => '',
        '#wrapper_attributes' => [
          'colspan' => 4,
          'class' => [
            'input-white',
          ],
        ],
      ];
    }

    $form['table']['twentyfourth']['other'] = [
      '#type' => 'textfield',
      '#default_value' => '',
      '#wrapper_attributes' => [
        'colspan' => 7,
        'class' => [
          'input-white',
          'hidden-print',
          'space'
        ],
      ],
      '#disabled' => TRUE,
    ];


    $text = 'Remisión: ';
    $text .= isset($data['remision']) ? $data['remision'] : '';
    $form['table']['twentyfourth']['remision'] = [
      '#type' => 'label',
      '#title' => $text,
      '#wrapper_attributes' => [
        'colspan' => 4,
        'class' => [
          'input-white',
          'hidden-print',
        ],
      ],
    ];

    if (isset($data['review']) && $data['review']) {
      $form['wrapper-print'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'print-wrapper'
          ],
          'id' => 'print-wrapper'
        ],
      ];
  
      $form['wrapper-print']['print'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'print-btn'
          ],
          'id' => 'print-cert'
        ],
      ];

      $form['wrapper-print']['label'] = [
        '#type' => 'label',
        '#title' => $this->t('Imprimir'),
        '#attributes' => [
          'class' => [
            'print-label',
            'disabled',
          ],
        ],
      ];
    }

    $form['#attached']['library'][] = 'sand_core/cert_script';

    return $form;
  }


  public function saveData(array &$form, FormStateInterface $form_state) {
    $tempstore = \Drupal::service('tempstore.private');
    $store = $tempstore->get('toSave');
    $sRemision = $tempstore->get('data_remision');
    $db = \Drupal::service('database');
    $fields = [];
    $toSave = [];

    $year = $form_state->getValue(['table','fourthRow','year']);
    $month = $form_state->getValue(['table','fourthRow','month']);
    $day = $form_state->getValue(['table','fourthRow','day']);
    $date = "$year-$month-$day";

    try {
      $fields['id_cert'] = $form_state->getValue(['table','thirdRow','consecutiveInput']);
      $toSave = [
        'sign_remision' => $store->get('sign_remision'),
        'fecha' => $date,
        'razonSocial' => strtoupper($form_state->getValue(['table','eighteenth','nameOutInput'])),
        'material' => strtoupper($form_state->getValue(['table','fourteenth','materialInput'])),
        'metros' => $form_state->getValue(['table','fifteenth','quantityInput']),
        'cnit' => $form_state->getValue(['table','nineteenth','nit','check']),
        'ccc' => $form_state->getValue(['table','nineteenth','cc','check']),
        'cce' => $form_state->getValue(['table','nineteenth','ce','check']),
        'crut' => $form_state->getValue(['table','nineteenth','rut','check']),
        'anmType' => $_SESSION['anmType'],
        'id' => $form_state->getValue(['table','twentyFirst','documentOutInput']),
        'rucom' => $form_state->getValue(['table','twentySecond','rucomInput']),
        'remision' => $sRemision->get('remision'),
        'consecutive' => $form_state->getValue(['table','thirdRow','consecutiveInput'])
      ];

      $toSave = urlencode(base64_encode(json_encode($toSave)));
      $fields['data_cert'] = $toSave;
      //$fields['date_creation'] = date('Y-m-d');

      $db->update('sand_core_cert_report_history_data')
        ->fields($fields)
        ->condition('id_cert', $store->get('remision')['remision'], '=')
        ->execute();
    }
    catch (\Exception $e) {
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}