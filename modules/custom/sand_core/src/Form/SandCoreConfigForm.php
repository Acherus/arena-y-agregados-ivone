<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use  \Drupal\user\Entity\User;
class SandCoreConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sand_core_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'sand_core.config'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['#tree'] = TRUE;
    $config = $this->config('sand_core.config');
    $addSing = $form_state->get('addSign') ==! NULL 
      ? $form_state->get('addSign') : 1;
    $delete = $form_state->get('del') ==! NULL 
      ? $form_state->get('del') : [];

    $form['maxRegisters'] = [
      '#type' => 'number',
      '#title' => $this->t('Número maximo de registros'),
      '#description' => $this->t('Cantidad de registros en las listas del sitio.'),
      '#max' => 10000,
      '#default_value' => $config->get('maxRegisters') !== NULL
        ? $config->get('maxRegisters') : 60,
    ];

    $form['listPager'] = [
      '#type' => 'number',
      '#title' => $this->t('Número de registros por página.'),
      '#description' => $this->t('Cantidad de registros por página en las listas del sitio.'),
      '#max' => 20,
      '#default_value' => $config->get('listPager')
        ? $config->get('listPager') : 20,
    ];

    $form['consecutive'] = [
      '#type' => 'number',
      '#title' => $this->t('Consecutivo del certificado'),
      '#default_value' => $config->get('consecutive')
        ? $config->get('consecutive') : 0,
    ];

    $form['remision'] = [
      '#type' => 'number',
      '#title' => $this->t('Consecutivo de la remisión'),
      '#default_value' => $config->get('remision')
        ? $config->get('remision') : 0,
    ];

    $form['vale'] = [
      '#type' => 'number',
      '#title' => $this->t('Consecutivo de vale'),
      '#default_value' => $config->get('vale')
        ? $config->get('vale') : 0,
    ];

    $form['sign'] = [
      '#type' => 'managed_file',
      '#upload_location' => 'public://',
      '#title' => t('Firma digital certificado ANM'),
      '#upload_validators' => [
        'file_validate_extensions' => ['png jpeg jpg gif']
      ],
      '#description' => $this->t('Formatos permitidos: jpg, jpeg, png ó gif.'),
      '#default_value' => $config->get('sign') !== NULL
        ? $config->get('sign') : '',
    ];

    $signs = $config->get('signs_wrapper');

    $form['signs_container'] = [
      '#type' => 'details',
      '#title' => t('Firmas formato de remisión'),
      '#group' => 'bootstrap',
    ];

    $form['signs_container']['signs_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'signs-wrapper'],
    ];

    $ids = \Drupal::entityQuery('user')
      ->condition('status', 1)
      ->execute();
    $users = User::loadMultiple($ids);

    //dpm($users);

    $opt = [];
    foreach ($users as $key => $user) {
      $opt[$user->id()] = $user->getUsername();
    }

    $element = [
      'user_id' => [
        '#title' => $this->t('Select User'),
        '#type' => 'select',
        '#options' => $opt,
      ],
      'image' => [
        '#type' => 'managed_file',
        '#upload_location' => 'public://',
        '#title' => $this->t('Firma digital'),
        '#upload_validators' => [
          'file_validate_extensions' => ['png jpeg jpg gif']
        ],
        '#description' => $this->t('Formatos permitidos: jpg, jpeg, png ó gif.'),
      ],
      'delete' => [
        '#type' => 'submit',
        '#value' => $this->t('Eliminar'),
        '#submit' => [[$this, 'deleteElement']],
        '#name' => 'new_0',
        '#ajax' => [
          'callback' => [$this, 'signCallback'],
          'wrapper' => 'signs-wrapper',
        ],
      ],
    ];

    if (!empty($signs)) {
      foreach ($signs as $key => $value) {
        if (!in_array($key, $delete)) {
          $form['signs_container']['signs_wrapper'][$key] = $element;
          $form['signs_container']['signs_wrapper'][$key]['user_id']['#default_value'] = $value['user_id'];
          $form['signs_container']['signs_wrapper'][$key]['image']['#default_value'] = $value['image'];
          $form['signs_container']['signs_wrapper'][$key]['delete']['#name'] = $key;
        }
      }
    }
    else {
      $form['signs_container']['signs_wrapper']['new_0'] = $element;
    }

    // Add new elements.
    for ($x = 0; $x < $addSing; $x++) {
      if (!in_array("new_$x", $delete)) {
        $form['signs_container']['signs_wrapper']["new_$x"] = $element;
        $form['signs_container']['signs_wrapper']["new_$x"]['delete']['#id'] = "new_$x";
      }
    }

    $form['signs_container']['add_sign'] = [
      '#type' => 'submit',
      '#value' => $this->t('Añadir'),
      '#submit' => [[$this, 'addElement']],
      '#id' => 'new',
      '#ajax' => [
        'callback' => [$this, 'signCallback'],
        'wrapper' => 'signs-wrapper',
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteElement(array $form, FormStateinterface $form_state) {
    $toDelete = $form_state->get('del') ==! NULL 
      ? $form_state->get('del') : [];
    array_push($toDelete, $form_state->getTriggeringElement()['#name']);
    $form_state->set('del', $toDelete);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function addElement(array $form, FormStateinterface $form_state) {
    $add = $form_state->get('addSign') ==! NULL 
      ? $form_state->get('addSign') : 1;
    $form_state->set('addSign', $add + 1);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function signCallback(array $form, FormStateinterface $form_state) {
    return $form['signs_container']['signs_wrapper'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $image = $form_state->getValue('sign');
    if (!empty($image[0])) {
      $file = File::load($image[0]);
      $file->setPermanent();
      $file->save();
    }

    // Save signs.
    $signs = $form_state->getValue(['signs_container', 'signs_wrapper']);
    $toSave = [];
    foreach ($signs as $field) {
      $toSave[$field['user_id']] = $field;
      if (!empty($field['image'][0])) {
        $file = File::load($field['image'][0]);
        $file->setPermanent();
        $file->save();
      }
    }

    $this->config($this->getEditableConfigNames()[0])
      ->set('maxRegisters', $form_state->getValue('maxRegisters'))
      ->set('listPager', $form_state->getValue('listPager'))
      ->set('consecutive', $form_state->getValue('consecutive'))
      ->set('remision', $form_state->getValue('remision'))
      ->set('sign', $form_state->getValue('sign'))
      ->set('vale', $form_state->getValue('vale'))
      ->set('signs_wrapper', $toSave)
      ->save();
    
    parent::submitForm($form, $form_state);
  }
}