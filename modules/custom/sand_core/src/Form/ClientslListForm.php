<?php

namespace Drupal\sand_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ClientslListForm extends FormBase{

  // Define data base table name;
  const CLIENTES = 'core_clientes';
  const TELEFONOS = 'core_telefonos';
  const CONDUCTORES = 'core_conductores';

  /**
   * Connection to data base.
   * 
   * @var Connection $connection
   */
  protected $connection;

  /**
   * Get module configuration.
   * 
   * @var ConfigFactoryInterface $config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, ConfigFactoryInterface $config) {
    $this->connection = $connection;
    $this->config = $config->get('sand_core.config');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sand_core_clients_list';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    if (isset($_GET['filter'])) {
      $filter = (array) json_decode(base64_decode(urldecode($_GET['filter'])));
    }

    $form = [];
    $form['#tree'] = TRUE;
    // First query.
    $queryBasic = $this->connection->select(self::CLIENTES, 'cc')
      ->fields('cc');
    $queryBasic->condition('cc.state', 1, '=');
    $queryBasic->leftJoin(self::TELEFONOS,  'ct', 'cc.id = ct.id_cliente');
    $queryBasic->fields('ct', ['telefono']);

    // Sencond query.
    $secondQuery = $this->connection->select(self::CLIENTES, 'cc')
      ->fields('cc');
    $secondQuery->rightJoin(self::TELEFONOS,  'ct', 'cc.id = ct.id_cliente');
    $secondQuery->fields('ct', ['telefono']);

    if (!empty($filter['cliente'])) {
      $queryBasic->condition('cc.id', $filter['cliente'], '=');
      $secondQuery->condition('ct.id_cliente', $filter['cliente'], '=');
    }
    $query = $queryBasic->union($secondQuery);
    $query->orderBy('razon_social');
    $data = $query->execute()->fetchAll();

    $form['returnContainer'] = [
      '#type' => 'container',
      '#id' => 'return-container',
    ];

    $form['returnContainer']['linkReturn'] = [
      '#type' => 'link',
      '#title' => $this->t('Página principal'),
      '#url' => Url::fromRoute('<front>'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
        ],
      ],
    ];

    $form['returnContainer']['addClient'] = [
      '#type' => 'link',
      '#title' => $this->t('agregar'),
      '#url' => Url::fromRoute('sand_core.clientes'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
        ],
      ],
    ];
    $count = 1;

    $form['wrapperFilters'] = [
      '#type' => 'container',
      '#id' => 'filters-wrapper-cl',
    ];

    

    $form['wrapperFilters']['filterIt'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filtrar'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
          'filter-it'
        ],
      ],
      '#submit' => [[$this, 'filterData']],
    ];

    $form['wrapperFilters']['clearFilters'] = [
      '#type' => 'submit',
      '#value' => $this->t('Limpiar filtros'),
      '#attributes' => [
        'class' => [
          'btn-basic',
          'list-table',
          'clear-filters'
        ],
      ],
      '#submit' => [[$this, 'clearFilter']],
    ];
    $form['wrapperFilters']['cliente'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cliente'),
      '#autocomplete_route_name' => 'sand_core.client_field',
      '#wrapper_attributes' => [
        'class' => ['cl-field']
      ]
    ];

    $form['wrapper'] = [
      '#type' => 'container',
      '#id' => 'table-wrapper',
    ];

    $headers = [
      'NIT','Razón social', 'Email', 'Teléfono', '',
    ];

    $user = \Drupal::currentUser();
    $user_roles = $user->getRoles();
    $roles_permissions = user_role_permissions($user_roles);
    $hasPermisson = FALSE;

    foreach($roles_permissions as $role => $permisson) {
      if (in_array('Disable clientes view', $permisson) || $role == 'administrator') {
        $hasPermisson = TRUE;
      }
    }

    if ($hasPermisson) {
      array_push($headers, '');
    }

    $form['wrapper']['table'] = [
      '#type' => 'table',
      '#header' => $headers,
    ];

    

    $chunk = $this->config->get('listPager') !== NULL 
      ? $this->config->get('listPager') : 20;
    $printData = array_chunk($data, $chunk, TRUE);
    $countPages = count($printData);
    $page = $form_state->get('page') !== NULL 
      ? $form_state->get('page') 
      : 0;

    foreach ($printData[$page] as $key => $cliente) {
      if (!array_key_exists($cliente->id, $form['wrapper']['table'])) {
        $form['wrapper']['table'][$cliente->id]['NIT'] = [
          '#type' => 'label',
          '#title' => $cliente->id,
        ];
  
        $form['wrapper']['table'][$cliente->id]['razon_social'] = [
          '#type' => 'label',
          '#title' => $cliente->razon_social,
        ];
  
        $form['wrapper']['table'][$cliente->id]['email'] = [
          '#type' => 'label',
          '#title' => $cliente->email,
        ];

        $form['wrapper']['table'][$cliente->id]['telefono'] = [
          '#type' => 'label',
          '#title' => $cliente->telefono,
        ];

        $form['wrapper']['table'][$cliente->id]['editar'] = [
          '#type' => 'link',
            '#title' => $this->t('Editar'),
            '#url' => Url::fromRoute('sand_core.clientes', [
              'cliente' => $cliente->id
            ]),
        ];

        if ($hasPermisson) {
          $form['wrapper']['table'][$cliente->id]['deshabilitar'] = [
            '#type' => 'link',
              '#title' => $this->t('Deshabilitar'),
              '#url' => Url::fromRoute('sand_core.disable_client', [
                'cliente' => $cliente->id
              ]),
          ];
        }
  
        $count++;
      }
    }

    if ($countPages > 1) {
      $form['pager_container'] = [
        '#type' => 'container',
        '#id' => 'pager-wrapper',
      ];

      for ($pager = 0; $pager < $countPages; $pager++) {
        $form['wrapper'][$pager] = [
          '#type' => 'submit',
          '#value' => $pager + 1,
          '#id' => $pager,
          '#submit' => [[$this, 'pagerSubmit']],
          '#ajax' => [
            'callback' => [$this, 'pagerCallback'],
            'wrapper' => 'table-wrapper',
          ],
          '#attributes' => [
            'class' => [
              'pager'
            ],
          ],
        ];

        if ($pager == $page) {
          $form['wrapper'][$pager]['#attributes']['class'][] = 'pager-selected';
        }
      }
    }

    $form['#theme'] = 'material-list';
    $form['#attached'] = [
      'library' => [
        'sand_core/sand_core',
      ],
    ];

    return $form;
  }

  /**
   * Return table.
   */
  public function pagerCallback(array $form,FormStateInterface $form_state) {
    return $form['wrapper'];
  }

  /**
   * Set current page.
   */
  public function pagerSubmit(array $form,FormStateInterface $form_state) {
    $el = $form_state->getTriggeringElement()['#id'];
    $form_state->set('page', $el);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function filterData(array $form,FormStateInterface $form_state) {
    $filters = $form_state->getValue('wrapperFilters');
    $data = [];

    if (!empty($filters['cliente'])) {
      $data['cliente'] = $filters['cliente'];
    }

    $form_state->setRedirect('sand_core.clients_list', [
      'filter' => urlencode(base64_encode((json_encode($data))))
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function clearFilter(array $form,FormStateInterface $form_state) {
    $form_state->setRedirect('sand_core.clients_list');
  }
}