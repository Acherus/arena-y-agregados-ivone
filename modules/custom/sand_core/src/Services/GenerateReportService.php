<?php

namespace Drupal\sand_core\Services;

use Drupal\Core\File\FileSystem;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\sand_core\Services\ReportsServices;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class GenerateReportService {

  /**
   * Base directory.
   * 
   * @var string $base
   */
  protected $base;

  /**
   * Get report services.
   * 
   * @var ReportsServices $service
   */
  protected $service;

  /**
   * Get module handler.
   * 
   * @var ModuleHandler $moduleHandler
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(FileSystem $file,
                              ConfigFactory $config,
                              ReportsServices $service,
                              ModuleHandler $moduleHandler) {
    $systemSchema = $config->get('system.file');
    $this->base = $file->realpath($systemSchema->get('default_scheme') . "://");
    $this->service = $service;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Generate report by matherial and month.
   * 
   * @param array $filter
   * @param string $type
   * 
   * @return string file route with name.
   */
  public function getReportByMaterialAndMonth($filter, $type) {
    $columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC','AD', 'AE', 'AF'];

    $spreadsheet = new Spreadsheet();
    $spreadsheet->getDefaultStyle()->applyFromArray([
      'font' => [
        'name' => 'Cambria'
      ]
    ]);
    $sheet = $spreadsheet->getActiveSheet();

    // Get data with or without filter.
    $data = $this->service->materialsReport($filter);
    $totalE = $this->service->getTotalEnterpriseData($filter);
    $materials = $this->service->getMaterials();

    $sheet->setCellValue('A1', 'CONSOLIDADO VOLUMENES DE DESPACHOS MENSUALES');

    $headers = [];
    $headers[] = NULL;
    $start = 1;

    foreach ($materials as $material) {
      $headers[] = $material->material;
      $sheet->setCellValue($columns[$start] . 3, $material->material);
      $oldValue = $start;
      $start += 2;
      $sheet->mergeCells($columns[$oldValue].'3:'.$columns[$start-1].'3');
      $sheet->getStyle($columns[$oldValue].'3:'.$columns[$start-1].'3')
        ->getAlignment()
        ->setVertical(Alignment::VERTICAL_CENTER)
        ->setHorizontal(Alignment::HORIZONTAL_CENTER);
      $sheet->getStyle($columns[$oldValue].'3:'.$columns[$start-1].'3')->applyFromArray(
        [
          'borders' => [
            'allBorders' => [
              'borderStyle' => Border::BORDER_THIN,
                'color' => ['rgb' => '000000'],
              ],
            ]
        ]);
      $sheet->setCellValue($columns[$oldValue] . 4, 'Contado');
      $sheet->setCellValue($columns[$oldValue+1] . 4, 'Con IVA');
      $contadoCell = (string) $columns[$oldValue] . 4;
      $ivaCell = (string) $columns[$oldValue+1] . 4;

      $sheet->getStyle("$contadoCell:$ivaCell")
      ->applyFromArray(
        [
          'borders' => [
            'allBorders' => [
              'borderStyle' => Border::BORDER_THIN,
                'color' => ['rgb' => '000000'],
              ],
            ]
        ]);
    }

    $sheet->mergeCells('A1:'. $columns[$start].'2');
    $sheet->getStyle('A1:'. $columns[$start].'2')
        ->getAlignment()
        ->setVertical(Alignment::VERTICAL_CENTER)
        ->setHorizontal(Alignment::HORIZONTAL_CENTER);

    $sheet->getStyle('A1:'. $columns[$start].'2')->applyFromArray(
      [
        'borders' => [
          'allBorders' => [
            'borderStyle' => Border::BORDER_THIN,
              'color' => ['rgb' => '000000'],
            ],
          ]
      ]);

    $order = [];
    $totals = [];
    $totals[] = '';
    $total = 0;
    foreach($data['report'] as $key => $row) {
      $order[$key][] = date('d-m-Y', strtotime($key));
      foreach ($materials as $material) {
        if (isset($row[$material->material])) {
          $order[$key]['contado'.$material->material] = $row[$material->material]['contado'];
          $order[$key]['iva'.$material->material] = $row[$material->material]['iva'];
        }
        else {
          $order[$key]['contado'.$material->material] = '';
          $order[$key]['iva'.$material->material] = '';
        }
      }
      $order[$key]['valor_total'] = $row['valor_total'];
    }

    $rowsTotal = count($order) + 5;

    $start = 1;
    foreach ($materials as $material) {
      $totals[$material->material] = $data['materials'][$material->material];
      array_push($totals, '');
      $oldValue = $start;
      $start += 2;
      $sheet->mergeCells($columns[$oldValue].$rowsTotal.':'.$columns[$start-1].$rowsTotal);
      $sheet->getStyle($columns[$oldValue].$rowsTotal.':'.$columns[$start-1].$rowsTotal)
        ->getAlignment()
        ->setVertical(Alignment::VERTICAL_CENTER)
        ->setHorizontal(Alignment::HORIZONTAL_CENTER);

      $sheet->getStyle($columns[$oldValue].$rowsTotal.':'.$columns[$start].$rowsTotal)
        ->applyFromArray(
        [
          'borders' => [
            'allBorders' => [
              'borderStyle' => Border::BORDER_THIN,
                'color' => ['rgb' => '000000'],
              ],
            ]
        ]);
    }

    array_push($totals, $data['sum']);
    array_push($order, $totals);

    $sheet->fromArray($order, NULL, 'A5');

    $sheet->setCellValue('A4', 'Fecha');
    $sheet->getStyle('A4')->applyFromArray(
      [
        'borders' => [
          'allBorders' => [
            'borderStyle' => Border::BORDER_THIN,
              'color' => ['rgb' => '000000'],
            ],
          ]
      ]);

    $sheet->setCellValue($columns[$start].'4', 'Valor Total');
    $sheet->getStyle($columns[$start].'4')->applyFromArray(
      [
        'borders' => [
          'allBorders' => [
            'borderStyle' => Border::BORDER_THIN,
              'color' => ['rgb' => '000000'],
            ],
          ]
      ]);

    // Set materials styles.
    $endColumn = $columns[$start] . ($rowsTotal - 1);
    $sheet->getStyle("A5:$endColumn")
      ->applyFromArray(
        [
          'borders' => [
            'allBorders' => [
              'borderStyle' => Border::BORDER_THIN,
                'color' => ['rgb' => '000000'],
              ],
            ]
        ]
      );

    $data2 = $aux = $mSum = [];
    $headers[] = NULL;
    $start = 1;
    // Calculation of size for the enterprises header.
    foreach ($materials as $material) {
      $start += 2;
    }

    // Set header for enterprises data.
    $sheet->setCellValue('A' . ($rowsTotal+2), 'CLIENTES CON PAGO ANTICIPADO O A CREDITO');
    $sheet->mergeCells('A' . ($rowsTotal+2).':'. $columns[$start]. ($rowsTotal+2));
    $sheet->getStyle('A' . ($rowsTotal+2))
      ->applyFromArray(
        [
          'borders' => [
            'allBorders' => [
              'borderStyle' => Border::BORDER_THIN,
                'color' => ['rgb' => '000000'],
              ],
            ]
        ]
      )
      ->getAlignment()
      ->setVertical(Alignment::VERTICAL_CENTER)
      ->setHorizontal(Alignment::HORIZONTAL_CENTER);

    $mSum['total'] = 'Total';
    $mSum[] = '';
    $total = 0;

    // Building enterprises data structure for report.
    foreach ($totalE as $value2) {
      if ($value2->tipo_pago != 'efectivo') {
        if (!array_key_exists($value2->razon_social, $data2)) {
          $data2[$value2->razon_social] = [];
          $data2[$value2->razon_social]['rz'] = strtoupper($value2->razon_social);
        }
        foreach ($materials as $material) {
          if (!isset($data2[$value2->razon_social][$material->material])) {
            if ((string)$material->material == (string)$value2->material) {
              $data2[$value2->razon_social][$material->material] += (int) $value2->volumen_total;
              $data2[$value2->razon_social][$material->material.'_space'] = '';
            }
            else {
              $data2[$value2->razon_social][$material->material] = 0;
              $data2[$value2->razon_social][$material->material.'_space'] = '';
            }
          }
          else {
  
            if ($value2->material == $material->material) {
              $data2[$value2->razon_social][$material->material] +=  (int) $value2->volumen_total;
              $data2[$value2->razon_social][$material->material.'_space'] = '';
            }
          }
        }
  
        if (!isset($mSum[$value2->material])) {
          $mSum[$value2->material] = (float) $value2->volumen_total;
          $mSum[] = '';
        }
        else {
          $mSum[$value2->material] += (float) $value2->volumen_total;
        }
        $aux[$value2->razon_social] += (float) $value2->total;
        $total += $value2->total;
      }
    }

    // Building total report structure.
    $mSumT = [];
    $mSumT['total'] = 'Total';
    foreach ($materials as $material) {
      if (!isset($mSum[$material->material])) {
        if (!empty($totals[$material->material])) {
          $mSumT[$material->material] = $totals[$material->material];
        }
        else{
          $mSumT[$material->material] = 0;
        }
        $mSumT[] = '';
      }
      else {
        if (!empty($totals[$material->material])) {
          $mSumT[$material->material] = $mSum[$material->material] + $totals[$material->material];
        }
        else {
          $mSumT[$material->material] = $mSum[$material->material];
        }
        $mSumT[] = '';
      }
    }

    // Sum for get the total of materials.
    foreach ($aux as $key => $totalAux) {
      $data2[$key]['total'] = $totalAux;
    }
    array_push($mSumT, ($total + end($totals)));

    // Set enterprises data.
    $sheet->fromArray($data2, NULL, 'A' . ($rowsTotal+3));

    $oldRows = $rowsTotal;
    $rowsTotal = count($data2) + 4;
    $cMaterial = count($materials);

    // Set total data.
    $sheet->fromArray($mSumT, NULL, 'A' . ($oldRows + $rowsTotal));

    // apply styles for report.
    for ($x = 0; $x < $rowsTotal - 4; $x++) {
      $oldValue = $x;
      $sheet->getStyle('A'.($x+($oldRows + 3)))
        ->getAlignment()
        ->setVertical(Alignment::VERTICAL_CENTER)
        ->setHorizontal(Alignment::HORIZONTAL_CENTER);
      $start = 1;
      for ($z = 0;  $z < $cMaterial; $z++) {
        $oldValue = $start;
        $start += 2;
        $sheet->mergeCells($columns[$oldValue].($x+($oldRows + 3)).':'.$columns[$start-1].($x+($oldRows + 3)));
        $sheet->getStyle($columns[$oldValue].($x+($oldRows + 3)).':'.$columns[$start-1].($x+($oldRows + 3)))
          ->getAlignment()
          ->setVertical(Alignment::VERTICAL_CENTER)
          ->setHorizontal(Alignment::HORIZONTAL_CENTER);
      }
    }

    $endColumn = $columns[$start] . ($oldRows + $rowsTotal - 2);
    $sheet->getStyle("A".($oldRows + 2).":$endColumn")
      ->applyFromArray(
        [
          'borders' => [
            'allBorders' => [
              'borderStyle' => Border::BORDER_THIN,
                'color' => ['rgb' => '000000'],
              ],
            ]
        ]
      );

    $sheet->getStyle('A'.($rowsTotal + $oldRows))
        ->getAlignment()
        ->setVertical(Alignment::VERTICAL_CENTER)
        ->setHorizontal(Alignment::HORIZONTAL_CENTER);
    $start = 1;
    for ($z = 0;  $z < $cMaterial; $z++) {
      $oldValue = $start;
      $start += 2;
      $sheet->mergeCells($columns[$oldValue].($rowsTotal + $oldRows).':'.$columns[$start-1].($rowsTotal + $oldRows));
      $sheet->getStyle($columns[$oldValue].($rowsTotal + $oldRows).':'.$columns[$start-1].($rowsTotal + $oldRows))
        ->getAlignment()
        ->setVertical(Alignment::VERTICAL_CENTER)
        ->setHorizontal(Alignment::HORIZONTAL_CENTER);
  }

    $endColumn = $columns[$start] . ($rowsTotal + $oldRows);
    $sheet->getStyle("A".($rowsTotal + $oldRows).":$endColumn")
      ->applyFromArray(
        [
          'borders' => [
            'allBorders' => [
              'borderStyle' => Border::BORDER_THIN,
                'color' => ['rgb' => '000000'],
              ],
            ]
        ]
      );

    // Define report format.
    if ($type == 'csv') {
      $writer = new Csv($spreadsheet);
      $writer->setDelimiter(';');
      $fileName = 'reporte_'.time().'.csv';
    }
    else {
      $writer = new Xlsx($spreadsheet);
      $fileName = 'reporte_'.time().'.xlsx';
    }
    $fullName = $this->base.'/reports/'.$fileName;

    $writer->save($fullName);

    // Clear variables.
    unset(
      $writer,
      $fileName,
      $columns,
      $sheet,
      $key,
      $spreadsheet,
      $data,
      $headers,
      $start,
      $endColumn,
      $oldValue,
      $x,
      $rowsTotal,
      $cMaterial,
      $mSum,
      $mSumT,
      $material,
      $aux
    );

    return $fullName;
  }

  /**
   * Generate report by advance.
   * 
   * @param array $filter
   * @param string $type
   * 
   * @return string file route with name.
   */
  public function getReportByAdvance($filter, $type) {
    $toPush = [];
  
    // Get data with or without filter.
    $usr = $this->service->getClientData($filter['nit']);
    $data = $this->service->getReportAdvanceData($filter);

    // Define table headers.
    $headers = [
      'FECHA',
      'VALOR ANTICIPO',
      '# VALE',
      'MATERIAL',
      'VOLUMEN (M3)',
      'SALDO (M3)',
      'OBSERVACIONES',
    ];
    
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $spreadsheet->getDefaultStyle()->applyFromArray([
      'font' => [
        'name' => 'Cambria'
      ]
    ]);

    $img = $this->moduleHandler->getModule('sand_core')->getPath();
    $img .= '/images/sandxlsx.png';
    $sheeti = new Drawing();
    $sheeti->setName('name');
    $sheeti->setDescription('description');
    $sheeti->setPath($img);
    $sheeti->setHeight(40);
    $sheeti->setCoordinates("A1");
    $sheeti->setOffsetX(4);
    $sheeti->setOffsetY(6);
    $sheeti->setWorksheet($sheet);

    $sheet->fromArray($headers, NULL, 'A7');
    $sheet->setCellValue('C1', 'CONTROL DE DESPACHOS CON ANTICIPO');
    $sheet->setCellValue('A4', 'Empresa: ' . $usr->razon_social);
    $sheet->setCellValue('F4', 'NIT: ' . $usr->id);
    $sheet->mergeCells('A1:B3');
    $sheet->mergeCells('C1:F3');
    $sheet->mergeCells('A4:E6');
    $sheet->mergeCells('F4:H6');

    $sheet->getStyle("A1:H6")
        ->applyFromArray(
          [
            'borders' => [
              'allBorders' => [
                'borderStyle' => Border::BORDER_THIN,
                  'color' => ['rgb' => '000000'],
                ],
              ]
          ]);

    $sheet->getStyle('C1:F3')
      ->getAlignment()
      ->setVertical(Alignment::VERTICAL_CENTER)
      ->setHorizontal(Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle('A4:E6')
      ->getAlignment()
      ->setVertical(Alignment::VERTICAL_CENTER)
      ->setHorizontal(Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle('F4:H6')
      ->getAlignment()
      ->setVertical(Alignment::VERTICAL_CENTER)
      ->setHorizontal(Alignment::HORIZONTAL_CENTER);

    $sheet->setCellValue('G1', 'CODIGO');
    $sheet->setCellValue('G2', 'VERSION');
    $sheet->setCellValue('G3', 'FECHA');
    $sheet->setCellValue('H1', 'FR-PL-29');
    $sheet->setCellValue('H2', '2');
    $sheet->setCellValue('H3', '24/05/19');

    $key = 0;
    $saveKeys = [];

    // Load data on export file.
    foreach($data as $register) {
      if ($register->anticipo == 1) {
        $saveKeys[] = [
          'key' => $key,
          'register' => $register,
        ];
        $toPush[$key] = [];
      }
      else {
        $material = $this->service->getMaterials($register->material);
        if (isset($register->saldo_material) && !empty($register->saldo_material)) {
          $newSaldo = $register->saldo_material;
        }
        else {
          $newSaldo = ((float) $register->saldo/(float)$material->valor_unitario);
        }

        $description = "";
        if ($register->anulado == 1) {
          $description = "ANULADO";
        }
        if ($register->tipo_salida == 'anulado') {
          $description = "SALDO MODIFICADO POR ANULACIÓN";
        }

        $toPush[$key] = [
          date('d-m-Y', strtotime($register->fecha)),
          '',
          $register->id_vale,
          strtoupper($register->material),
          $register->volumen,
          $newSaldo,
          $description
        ];
      }
      $key++;
    }

    if (!empty($saveKeys)) {
      foreach ($saveKeys as $ks => $value) {

        if (isset($value['register']->saldo_material) && !empty($value['register']->saldo_material)) {
          $salD = $value['register']->saldo_material;
        }
        else {
          $salD = ((float) $value['register']->saldo/(float)$value['register']->valor_unitario);
        }

        $material = $this->service->getMaterials($value['register']->material);

        $toPush[$value['key']] = [
          date('d-m-Y', strtotime($value['register']->fecha)),
          $value['register']->valor_anticipo,
          '--',
          strtoupper($material->material),
          isset($value['register']->volumen_anticipo) ? $value['register']->volumen_anticipo : 0,
          $value['register']->saldo_material,
          '',
        ];

        unset($saveKeys[$ks]);
      }
    }

    $sheet->fromArray($toPush, NULL, 'A8');
    $count = count($toPush)+7;
    $sheet->getStyle('B8:B120')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD);

    for ($x = 8; $x <= $count; $x++) {
      $sheet->mergeCells("G$x:H$x");
    }

    $sheet->getStyle("A7:H$count")
        ->applyFromArray(
          [
            'borders' => [
              'allBorders' => [
                'borderStyle' => Border::BORDER_THIN,
                  'color' => ['rgb' => '000000'],
                ],
              ]
          ]);

    // Define report format.
    if ($type == 'csv') {
      $writer = new Csv($spreadsheet);
      $writer->setDelimiter(';');
      $fileName = 'reporte_'.time().'.csv';
    }
    else {
      $writer = new Xlsx($spreadsheet);
      $fileName = 'reporte_'.time().'.xlsx';
    }
    $fullName = $this->base.'/reports/'.$fileName;

    $writer->save($fullName);

    unset(
      $writer,
      $fileName,
      $type,
      $spreadsheet,
      $sheet,
      $count,
      $saveKeys,
      $key,
      $toPush,
      $newSaldo,
      $material,
      $salD,
      $toPush,
      $usr,
      $data,
      $headers,
      $img
    );

    // Return report.
    return $fullName;
  }

  /**
   * Generate report by client.
   * 
   * @param array $filter
   * @param string $type
   * 
   * @return string file route with name.
   */
  public function getReportByClient($filter, $type) {
    $toPush = [];

    // Get data with or without filter.
    $data = $this->service->getClientReportData($filter);

    $spreadsheet = new Spreadsheet();
    $spreadsheet->getDefaultStyle()->applyFromArray([
      'font' => [
        'name' => 'Cambria'
      ]
    ]);
    $sheet = $spreadsheet->getActiveSheet();

    $sheet->setCellValue('A1', $data[0]->razon_social);
    $sheet->mergeCells('A1:G2');
    $sheet->getStyle('A1:G2')
      ->getAlignment()
      ->setVertical(Alignment::VERTICAL_CENTER)
      ->setHorizontal(Alignment::HORIZONTAL_CENTER);

    $sheet->getStyle('A1:G3')->applyFromArray(
      [
        'borders' => [
          'allBorders' => [
            'borderStyle' => Border::BORDER_THIN,
              'color' => ['rgb' => '000000'],
            ],
          ]
      ]);

    // Define table headers.
    $headers = [
      'FECHA',
      '# VALE',
      'MATERIAL',
      'VOLUMEN (M3)',
      'VALOR VIAJE',
      'ANTICIPO',
      'SALDO',
    ];

    $sheet->fromArray($headers, NULL, 'A3');
    $lastValue = $saldo = $key = 0;
    $saveKeys = [];

    // Load data on export file.
    foreach($data as $register) {
      $fecha = date('d-m-Y', strtotime($register->fecha));
      $toPush[$key] = [
        'fecha' => $fecha,
        'id_vale' => $register->id_vale,
        'material' => $register->material,
        'volumen' => $register->volumen,
        'v_total' => $register->valor_total,
        'v_anticipo' => $register->anticipo != 1 ? '' : $register->valor_anticipo,
        'saldo' => $register->saldo,
        'anticipo' => $register->anticipo,
        'tipo_salida' => $register->tipo_salida,
        'saldo_material' => $register->saldo_material,
        'descripcion' => "",
      ];

      $material = $this->service->getMaterials($register->material);

      if ($register->anticipo != 1) {
        if ($register->tipo_salida == 'efectivo') {
          $saldo = $register->saldo_material;
        }
        else {
          if (isset($register->saldo_material) && !empty($register->saldo_material)) {
            $saldo = $register->saldo_material;
          }
          else {
            $saldo = (float) $register->saldo/(float)$material->valor_unitario;
          }
          $lastValue = $saldo;
        }

        $toPush[$key]['saldo'] = $saldo;
      }
      else {
        $saveKeys[$key] = $key;
      }

      if ($register->anulado == 1) {
        $toPush[$key]['descripcion'] = "ANULADO";
      }
      if ($register->tipo_salida == 'anulado') {
        $toPush[$key]['descripcion'] = "SALDO MODIFICADO POR ANULACIÓN";

      }

      if (!empty($saveKeys) && $register->anticipo != 1) {
        foreach ($saveKeys as $k => $value) {
          $toPush[$value]['saldo'] = $toPush[$value]['saldo_material'];
          unset($saveKeys[$k]);
        }
      }

      $key++;
    }

    foreach ($toPush as $key => $value) {
      $cell = $key +4;
      $sheet->setCellValue("A$cell", $value['fecha']);
      if ($value['anticipo'] == 1) {
        $sheet->setCellValue("B$cell", 'Anticipo');
        $sheet->mergeCells("B$cell:E$cell");
        $sheet->getStyle("B$cell:E$cell")
          ->getAlignment()
          ->setVertical(Alignment::VERTICAL_CENTER)
          ->setHorizontal(Alignment::HORIZONTAL_CENTER);
      }
      else {
        $sheet->setCellValue("B$cell", $value['id_vale']);
        $sheet->setCellValue("C$cell", $value['material']);
        $sheet->setCellValue("D$cell", $value['volumen']);
        $sheet->setCellValue("E$cell", $value['v_total']);
      }
      $sheet->setCellValue("F$cell", $value['v_anticipo']);
      $sheet->setCellValue("G$cell", $value['saldo']);
      $sheet->setCellValue("H$cell", $value['descripcion']);
    }

    $count = count($toPush)+3;
    $sheet->getStyle("A4:G$count")->applyFromArray(
      [
        'borders' => [
          'allBorders' => [
            'borderStyle' => Border::BORDER_THIN,
              'color' => ['rgb' => '000000'],
            ],
          ]
      ]);

    // Define report format.
    if ($type == 'csv') {
      $writer = new Csv($spreadsheet);
      $writer->setDelimiter(';');
      $fileName = 'reporte_'.time().'.csv';
    }
    else {
      $writer = new Xlsx($spreadsheet);
      $fileName = 'reporte_'.time().'.xlsx';
    }
    $fullName = $this->base.'/reports/'.$fileName;

    $writer->save($fullName);

    // Clear variables.
    unset(
      $writer,
      $fileName,
      $count,
      $sheet,
      $saveKeys,
      $key,
      $lastValue,
      $toPush,
      $fecha,
      $spreadsheet,
      $data,
      $headers
    );

    return $fullName;
  }

}