<?php

namespace Drupal\sand_core\Services;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;

class AuditServices {

  const LOGS = 'core_logs';
  const HISTORY = 'sand_core_cert_report_history_data';

  protected $user;
  protected $db;

  /**
   * Get module configuration.
   * 
   * @var ConfigFactoryInterface $config
   */
  protected $config;


  /**
   * {@inheritdoc}
   */
  public function __construct(AccountInterface $user, 
                              Connection $db,
                              ConfigFactoryInterface $config) {
    $this->user = $user;
    $this->db = $db;
    $this->config = $config->get('sand_core.config');
  }


  public function pushLog($type, $description, $cOperation) {
    try {
      $userId = $this->user->id();
      $date = new \DateTime();
      $date->setTimeStamp(\Drupal::time()->getCurrentTime());
      $date->setTimeZone(new \DateTimeZone('America/Bogota'));

      $this->db->insert(self::LOGS)
        ->fields([
          'type' => $type,
          'description' => $description,
          'user_id' => $userId,
          'client_operation' => $cOperation,
          'fecha_registro' => $date->format('Y-m-d H:i:s')
        ])
        ->execute();
      $return = TRUE;
    }
    catch (\Exception $e) {
      $return = FALSE;
    }

    return $return;
  }

  public function getAuditInfo(array $filters = []) {
    try {
      $range = $this->config->get('maxRegisters') !== NULL 
        ? $this->config->get('maxRegisters') : 60;

      $query = $this->db->select(self::LOGS, 'cl')
        ->fields('cl');

      if (isset($filters['type']) && !empty($filters['type'])) {
        $query->condition('type', $filters['type'], '=');
      }
  
      if (isset($filters['client_operation']) && !empty($filters['client_operation'])) {
        $query->condition('client_operation', $filters['client_operation'], '=');
      }

      if (isset($filters['initTime']) || isset($filters['endTime'])) {
        if (isset($filters['initTime']) && !empty($filters['endTime'])
            && isset($filters['endTime']) && !empty($filters['endTime'])) {
          $query->condition('fecha_registro', [$filters['initTime'], $filters['endTime']], 'BETWEEN');
        }
        elseif (isset($filters['initTime']) && !empty($filters['initTime'])
                && (!isset($filters['endTime']) || empty($filters['endTime']))) {
          $query->condition('fecha_registro', [$filters['initTime'], date('Y-m-d')], 'BETWEEN');
        }
        elseif (isset($filters['endTime']) && !empty($filters['endTime'])
                && (!isset($filters['initTime']) || empty($filters['initTime']))) {
          $query->condition('fecha_registro', [date('Y-m-d'), $filters['endTime']], 'BETWEEN');
        }
      }

      $query->range(0, $range);
      $query->orderBy('fecha_registro', 'DESC');
      
      $data = $query->execute()->fetchAll();
      $data = (array) $data;
      
      foreach ($data as $key => $register) {
        $data[$key] = (array) $register;
        $account = \Drupal\user\Entity\User::load($register->user_id);

        switch ($register->type) {
          case 'insert':
            $data[$key]['type'] = 'Inserto';
            break;
          case'update':
            $data[$key]['type'] = 'Actualizo';
            break;
        }

        switch ($register->client_operation) {
          case 'create_client':
            $data[$key]['client_operation'] = 'Crear cliente';
            break;
          case 'update_client':
            $data[$key]['client_operation'] = 'Actualizar cliente';
            break;
          case'outs':
            $data[$key]['client_operation'] = 'Salida de materiales';
            break;
          case'pay':
            $data[$key]['client_operation'] = 'Pagos y anticipos';
            break;
          case'create_material':
            $data[$key]['client_operation'] = 'Crear material';
            break;
          case'update_material':
            $data[$key]['client_operation'] = 'Actualizar material';
            break;
        }
        $data[$key]['userName'] = $account->getUserName();
      }
    }
    catch (\Exception $e) {
      $data = [
        'error' => $e->getMessage(),
        'code' => $e->getCode(),
      ];
    }

    return $data;
  }

  public function getHistoryData(array $filters = [], int $page = 1) {
    try {
      $range = $this->config->get('maxRegisters') !== NULL 
        ? $this->config->get('maxRegisters') : 20;

      $queryCount = $this->db->select(self::HISTORY);

      $start = $page == 1 ? 0 : ($range * ($page - 1)) + 1;
      $end = $page == 1 ? $range : $range * $page;

      $query = $this->db->select(self::HISTORY, 'sch')
        ->fields('sch');

      if (isset($filters['initTime']) || isset($filters['endTime'])) {
        if (isset($filters['initTime']) && !empty($filters['endTime'])
            && isset($filters['endTime']) && !empty($filters['endTime'])) {
          $query->condition('date_creation', [$filters['initTime'], $filters['endTime']], 'BETWEEN');
          $queryCount->condition('date_creation', [$filters['initTime'], $filters['endTime']], 'BETWEEN');
        }
        elseif (isset($filters['initTime']) && !empty($filters['initTime'])
                && (!isset($filters['endTime']) || empty($filters['endTime']))) {
          $query->condition('date_creation', [$filters['initTime'], date('Y-m-d')], 'BETWEEN');
          $queryCount->condition('date_creation', [$filters['initTime'], date('Y-m-d')], 'BETWEEN');
        }
        elseif (isset($filters['endTime']) && !empty($filters['endTime'])
                && (!isset($filters['initTime']) || empty($filters['initTime']))) {
          $query->condition('date_creation', [date('Y-m-d'), $filters['endTime']], 'BETWEEN');
          $queryCount->condition('date_creation', [date('Y-m-d'), $filters['endTime']], 'BETWEEN');
        }
      }

      $query->orderBy('id', 'DESC');
      $query->range($start, $range);
      $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
      $countData = $queryCount->countQuery()
        ->execute()
        ->fetchField();

      $data = [
        'count' => $countData,
        'totalPages' => round((float)$countData / $range, 0, PHP_ROUND_HALF_UP),
        'data' => $data,
      ];
    }
    catch (\Exception $e) {
      $data = [
        'error' => $e->getMessage(),
        'code' => $e->getCode(),
      ];
    }

    return $data;
  }

}