<?php

namespace Drupal\sand_core\Services;

use Drupal\Core\Database\Connection;

/**
 * Generate service to use DB querys.
 */
class QueryServices {

  // Define data base tables names;
  const CLIENTES = 'core_clientes';
  const TELEFONOS = 'core_telefonos';
  const CONDUCTORES = 'core_conductores';

  /**
   * Save DB query's.
   * 
   * @var Object $query
   */
  private $query;

  /**
   * Save DB connection to create query's.
   * 
   * @var Connection $connection
   */
  private $connection;

  /**
   * 
   * 
   * @var Array $pushStatus
   */
  private $pushStatus;

  /**
   * Generate DB connection and initialize pushStatus propertie.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
    $this->pushStatus = [
      'error' => FALSE,
      'ok' => FALSE,
      'description' => [],
    ];
  }

  /**
   * Execute query propertie.
   */
  public function execute() {
    return $this->query->execute()->fetch();
  }

  /**
   * Get client information form client form.
   * 
   * @param string $clientId
   */
  public function getAllClientInformation($clientId) {
    try {
      $userData = $this->connection->select(self::CLIENTES, 'cc')
      ->fields('cc', ['id', 'razon_social', 'direccion', 'saldo', 'email', 'cantidad_ant', 'doc_type'])
      ->condition('id', $clientId, '=')
      ->execute()
      ->fetch();

    $phones = $this->connection->select(self::TELEFONOS, 'ct')
      ->fields('ct', ['id', 'telefono', 'id_cliente'])
      ->condition('id_cliente', $clientId, '=')
      ->execute()
      ->fetchAll();

    $conductores = $this->connection->select(self::CONDUCTORES, 'cdt')
      ->fields('cdt', ['id', 'nombres_conductor', 'apellidos_conductor', 'placa'])
      ->condition('id_cliente', $clientId, '=')
      ->execute()
      ->fetchAll();
    }
    catch(\Exception $e) {
    }
    


    return [
      'clientData' => $userData,
      'phones' => $phones,
      'conductores' => $conductores,
    ];
  }

  /**
   * Update or save new client data.
   * 
   * @param array $data
   * @param bool $type
   */
  public function pushClientInformation(array $data, string $type) {
    try {
      // Insert or update client.
      $clientId = $data['id'];

      if (isset($data['razon_social'])) {
        $data['razon_social'] = strtoupper($data['razon_social']);
      }

      if (isset($data['razon_social'])) {
        $data['razon_social'] = strtoupper($data['razon_social']);
      }

      if (isset($data['email'])) {
        $data['email'] = strtoupper($data['email']);
      }

      if (isset($data['direccion'])) {
        $data['direccion'] = strtoupper($data['direccion']);
      }

      
      unset($data['more_phone'], $data['more_drivers']);
      unset($data['saldo'], $data['cantidad_ant']);
      if ($type != 'new') {
        unset($data['id']);

        $query = $this->connection->update(self::CLIENTES)
          ->condition('id', $clientId, '=');
        $this->pushStatus['type'] = 'update';
        $this->pushStatus['client_operation'] = 'update_client';
      }
      else {
        $query = $this->connection->insert(self::CLIENTES);
        $this->pushStatus['type'] = 'insert';
        $this->pushStatus['client_operation'] = 'create_client';
      }
  
      // Perare data of driver's to save in DB.
      $telefonos = $data['telefonos'];
      $conductores = $data['conductores'];
      unset($data['telefonos'], $data['conductores']);
      $data['en_deuda'] = 0;
      $query->fields($data);
      $query->execute();
      unset($query);
  
      // Save or update driver's.
      foreach ($telefonos as $tel) {
        if (!empty($tel['telefono'])) {
          $tel['id_cliente'] = $clientId;
          if ($tel['id'] == 'new') {
            unset($tel['id'], $tel['delete']);
            $this->connection->insert(self::TELEFONOS)
              ->fields($tel)
              ->execute(); 
          }
          else {
            $telId = $tel['id'];
            unset($tel['id'], $tel['delete']);
            $this->connection->update(self::TELEFONOS)
              ->fields($tel)
              ->condition('id', $telId, '=')
              ->condition('id_cliente', $clientId, '=')
              ->execute();
          }
        }
      }

      // Save or update driver's.
      foreach ($conductores as $conductor) {

        if (isset($conductor['placa'])) {
          $conductor['placa'] = strtoupper($conductor['placa']);
        }

        if (isset($conductor['nombres_conductor'])) {
          $conductor['nombres_conductor'] = strtoupper($conductor['nombres_conductor']);
        }

        if (isset($conductor['apellidos_conductor'])) {
          $conductor['apellidos_conductor'] = strtoupper($conductor['apellidos_conductor']);
        }

        if (!empty($conductor['nombres_conductor'])) {
          $conductor['id_cliente'] = $clientId;
          if ($conductor['id'] == 'new') {
            unset($conductor['id']);
            
            $this->connection->insert(self::CONDUCTORES)
              ->fields($conductor)
              ->execute(); 
          }
          else {
            $conductorId = $conductor['id'];
            unset($conductor['id']);
            $this->connection->update(self::CONDUCTORES)
              ->fields($conductor)
              ->condition('id', $conductorId, '=')
              ->condition('id_cliente', $clientId, '=')
              ->execute();
          }
        }
      }

      // Set status when the process is succesful.
      $this->pushStatus['ok'] = TRUE;
      $this->pushStatus['description'] = [
        'message' => 'Push completed',
        'code' => 100,
      ];
    }
    catch (\Exception $e) {
      // Set status when the process failure.
      $this->pushStatus['error'] = TRUE;
      $this->pushStatus['description'] = [
        'message' => $e->getMessage(),
        'code' => $e->getCode(),
      ];
    }
  }

  /**
   * Get push status.
   */
  public function getPushStatus() {
    return $this->pushStatus;
  }

}