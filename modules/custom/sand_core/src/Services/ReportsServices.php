<?php

namespace Drupal\sand_core\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Datetime\TimeInterface;

class ReportsServices {

  /**
   * Define table names.
   */
  const CLIENTES = 'core_clientes',
        OUT = 'core_despachos',
        MATERIALES = 'core_materiales',
        PAGOS = 'core_pagos',
        MOVEMENTS = 'core_movements';

  /**
   * Get data base connection.
   * 
   * @var \Drupal\Core\Database\Connection $connection
   */
  private $connection;

  /**
   * Get module configuration.
   * 
   * @var ConfigFactoryInterface $config
   */
  protected $config;

  /**
   * Get time.
   * 
   * @var TimeInterface $time
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, 
                              ConfigFactoryInterface $config,
                              TimeInterface $time) {
    $this->connection = $connection;
    $this->config = $config->get('sand_core.config');
    $this->time = $time;
  }

  /**
   * Get report information.
   * 
   * @param array $filters
   */
  public function getDataToShow(array $filters = [], int $page = 1) {
    \Drupal::logger('getDataToShow')->notice(print_r([$filters, $page], 1));

    $range = $this->config->get('maxRegisters') !== NULL 
        ? $this->config->get('maxRegisters') : 20;
    $start = $page == 1 ? 0 : ($range * ($page - 1)) + 1;

    $query = $this->connection->select(self::OUT, 'cd')
      ->fields('cd');

    $queryCount = $this->connection->select(self::OUT);

    if (isset($filters['nit']) && !empty($filters['nit'])) {
      $query->condition('id_cliente', $filters['nit'], '=');
      $queryCount->condition('id_cliente', $filters['nit'], '=');
    }

    if (isset($filters['material']) && !empty($filters['material'])) {
      $query->condition('material', $filters['material'], 'LIKE');
      $queryCount->condition('material', $filters['material'], 'LIKE');
    }

    if (isset($filters['tipo_pago']) && !empty($filters['tipo_pago'])) {
      $query->condition('tipo_pago', $filters['tipo_pago'], '=');
      $queryCount->condition('tipo_pago', $filters['tipo_pago'], '=');
    }

    if (isset($filters['iva']) && !empty($filters['iva'])) {
      $iva = $filters['iva'] == 'sin iva' ? 0 : 1;
      $query->condition('iva', $iva, '=');
      $queryCount->condition('iva', $iva, '=');
    }

    if (isset($filters['initTime']) || isset($filters['endTime'])) {
      if (isset($filters['initTime']) && !empty($filters['endTime'])
          && isset($filters['endTime']) && !empty($filters['endTime'])) {
        $query->condition('fecha', [date('Y-m-d 00:00:00', strtotime($filters['initTime'])), date('Y-m-d 23:59:59', strtotime($filters['endTime']))], 'BETWEEN');
        $queryCount->condition('fecha', [date('Y-m-d 00:00:00', strtotime($filters['initTime'])), date('Y-m-d 23:59:59', strtotime($filters['endTime']))], 'BETWEEN');
      }
      elseif (isset($filters['initTime']) && !empty($filters['initTime'])
              && (!isset($filters['endTime']) || empty($filters['endTime']))) {
        $query->condition('fecha', [date('Y-m-d 00:00:00', strtotime($filters['initTime'])), date('Y-m-d 23:59:59')], 'BETWEEN');
        $queryCount->condition('fecha', [date('Y-m-d 00:00:00', strtotime($filters['initTime'])), date('Y-m-d 23:59:59')], 'BETWEEN');
      }
      elseif (isset($filters['endTime']) && !empty($filters['endTime'])
              && (!isset($filters['initTime']) || empty($filters['initTime']))) {
        $query->condition('fecha', [date('Y-m-d 00:00:00') , date('Y-m-d 23:59:59', strtotime($filters['endTime']))], 'BETWEEN');
        $queryCount->condition('fecha', [date('Y-m-d 00:00:00') , date('Y-m-d 23:59:59', strtotime($filters['endTime']))], 'BETWEEN');
      }
    }

    $query->orderBy('fecha', 'DESC');
    $query->range($start, $range);
    \Drupal::logger('query string')->notice(print_r($query->__toString(), 1));

    $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    $countData = $queryCount->countQuery()
        ->execute()
        ->fetchField();

    return [
      'count' => $countData,
      'totalPages' => round((float)$countData / $range, 0, PHP_ROUND_HALF_UP),
      'data' => $data,
    ];
  }

  /**
   * Get report information.
   * 
   * @param array $filters
   */
  public function getData(array $filters = []) {
    $query = $this->connection->select(self::OUT, 'cd')
      ->fields('cd');
      
    if (isset($filters['nit']) && !empty($filters['nit'])) {
      $query->condition('id_cliente', $filters['nit'], '=');
    }

    if (isset($filters['material']) && !empty($filters['material'])) {
      $query->condition('material', $filters['material'], 'LIKE');
    }

    if (isset($filters['tipo_pago']) && !empty($filters['tipo_pago'])) {
      $query->condition('tipo_pago', $filters['tipo_pago'], '=');
    }

    if (isset($filters['iva']) && !empty($filters['iva'])) {
      $iva = $filters['iva'] == 'sin iva' ? 0 : 1;
      $query->condition('iva', $iva, '=');
    }

    if (isset($filters['initTime']) || isset($filters['endTime'])) {
      if (isset($filters['initTime']) && !empty($filters['endTime'])
          && isset($filters['endTime']) && !empty($filters['endTime'])) {
        $query->condition('fecha', [date('Y-m-d 00:00:00', strtotime($filters['initTime'])), date('Y-m-d 23:59:59', strtotime($filters['endTime']))], 'BETWEEN');
      }
      elseif (isset($filters['initTime']) && !empty($filters['initTime'])
              && (!isset($filters['endTime']) || empty($filters['endTime']))) {
        $query->condition('fecha', [date('Y-m-d 00:00:00', strtotime($filters['initTime'])), date('Y-m-d 23:59:59')], 'BETWEEN');
      }
      elseif (isset($filters['endTime']) && !empty($filters['endTime'])
              && (!isset($filters['initTime']) || empty($filters['initTime']))) {
        $query->condition('fecha', [date('Y-m-d 00:00:00') , date('Y-m-d 23:59:59', strtotime($filters['endTime']))], 'BETWEEN');
      }
    }
    $range = $this->config->get('maxRegisters') !== NULL 
      ? $this->config->get('maxRegisters') : 60;
    $query->range(0, $range);

    $query->leftJoin('sand_core_cert_report_history_data', 'scr', 'scr.id_reg = cd.id_remision');
    $query->fields('scr', ['status']);

    $query->orderBy('fecha', 'DESC');
    
    $data = $query->execute()->fetchAll();
    return $data;
  }

  public function getMaterials($material = NULL) {
    $materiales = $this->connection->select(self::MATERIALES, 'cm')
      ->fields('cm')
      ->execute()
      ->fetchAll();

    if ($material) {
      foreach ($materiales as $fMaterial) {
        if ($fMaterial->material == $material) {
          return $fMaterial;
        }
      }
    }

    return $materiales;
  }

  public function getClientReportData(array $filters = []) {
    $query = $this->connection->select(self::MOVEMENTS, 'cm');
    $query->fields('cm');

    if (isset($filters['initTime']) || isset($filters['endTime'])) {
      if (isset($filters['initTime']) && !empty($filters['endTime'])
          && isset($filters['endTime']) && !empty($filters['endTime'])) {
        $query->condition('fecha', [date('Y-m-d 00:00:00', strtotime($filters['initTime'])), date('Y-m-d 23:59:59', strtotime($filters['endTime']))], 'BETWEEN');
      }
      elseif (isset($filters['initTime']) && !empty($filters['initTime'])
              && (!isset($filters['endTime']) || empty($filters['endTime']))) {
        $query->condition('fecha', [date('Y-m-d 00:00:00', strtotime($filters['initTime'])), date('Y-m-d 23:59:59')], 'BETWEEN');
      }
      elseif (isset($filters['endTime']) && !empty($filters['endTime'])
              && (!isset($filters['initTime']) || empty($filters['initTime']))) {
        $query->condition('fecha', [date('Y-m-d 00:00:00') , date('Y-m-d 23:59:59', strtotime($filters['endTime']))], 'BETWEEN');
      }
    }

    $query->condition('id_cliente', $filters['nit'], '=');
    $query->orderBy('id', 'asc');
    return $query->execute()->fetchAll();
  }

  public function getClientData($nit) {
    return $this->connection->select(self::CLIENTES, 'cc')
      ->fields('cc', ['id', 'razon_social'])
      ->condition('id', $nit, '=')
      ->execute()
      ->fetch();
  }

  public function materialsReport(array $filters = []) {
    $query = $this->connection->select(self::OUT, 'cd')
      ->fields('cd', ['iva', 'valor_total', 'material', 'fecha', 'volumen']);

    if (isset($filters['initTime']) || isset($filters['endTime'])) {
      if (isset($filters['initTime']) && !empty($filters['endTime'])
          && isset($filters['endTime']) && !empty($filters['endTime'])) {
        $query->condition('fecha', [date('Y-m-d 00:00:00', strtotime($filters['initTime'])), date('Y-m-d 23:59:59', strtotime($filters['endTime']))], 'BETWEEN');
      }
      elseif (isset($filters['initTime']) && !empty($filters['initTime'])
              && (!isset($filters['endTime']) || empty($filters['endTime']))) {
        $query->condition('fecha', [date('Y-m-d 00:00:00', strtotime($filters['initTime'])), date('Y-m-d 23:59:59')], 'BETWEEN');
      }
      elseif (isset($filters['endTime']) && !empty($filters['endTime'])
              && (!isset($filters['initTime']) || empty($filters['initTime']))) {
        $query->condition('fecha', [date('Y-m-d 00:00:00') , date('Y-m-d 23:59:59', strtotime($filters['endTime']))], 'BETWEEN');
      }
    }

    $query->condition('tipo_pago', 'efectivo', '=');

    if (!isset($filters['initTime']) && !isset($filters['endTime'])) {
      $date = new \DateTime();
      $date->setTimeStamp($this->time->getCurrentTime());
      $date->setTimeZone(new \DateTimeZone('America/Bogota'));
      $subDate = $date->format('Y-m');
      $subDate .= '-1 00:00:00';

      $query->condition('fecha', [$subDate, $date->format('Y-m-d 23:59:59')], 'BETWEEN');
    }

    $query->leftJoin('sand_core_cert_report_history_data', 'scr', 'scr.id_reg = cd.id_remision');
    // $query->fields('scr', ['status']);
    $query->condition('scr.status', 1, '=');

    $primitive = [];
    $outsData = $query->execute()->fetchAll();
    $sum = 0;
    $materials =[];

    foreach ($outsData as $row) {
      $materials[$row->material] += (float) $row->volumen;
      $sum += (float) $row->valor_total;

      $type = 'iva';
      $otherType = 'contado';
      $fecha = date('d-m-Y', strtotime($row->fecha));


      if (!isset($primitive[$fecha])) {
        $primitive[$fecha] = [];
        $primitive[$fecha]['valor_total'] = (float) $row->valor_total;
        if ($row->iva == 1) {
          $primitive[$fecha][$row->material][$type] = $row->volumen;
          $primitive[$fecha][$row->material][$otherType] = 0;
        }
        else {
          $primitive[$fecha][$row->material][$otherType] = $row->volumen;
          $primitive[$fecha][$row->material][$type] = 0;
        }
      }
      else {
        if (!isset($primitive[$fecha][$row->material])) {
          if ($row->iva == 1) {
            $primitive[$fecha][$row->material][$type] += $row->volumen;
            $primitive[$fecha][$row->material][$otherType] = 0;
          }
          else {
            $primitive[$fecha][$row->material][$otherType] += $row->volumen;
            $primitive[$fecha][$row->material][$type] = 0;
          }
        }
        else {
          if ($row->iva == 1) {
            $primitive[$fecha][$row->material][$type] += $row->volumen;
            $primitive[$fecha][$row->material][$otherType] += 0;
          }
          else {
            $primitive[$fecha][$row->material][$otherType] += $row->volumen;
            $primitive[$fecha][$row->material][$type] += 0;
          }
        }
        $primitive[$fecha]['valor_total'] += (float) $row->valor_total;
      }
    }

    return [
      'materials' => $materials,
      'report' => $primitive,
      'sum' => $sum
    ];
  }

  public function getReportAdvanceData(array $filters = []) {
    $query = $this->connection->select(self::MOVEMENTS, 'cm');
    $query->fields('cm');

    if (isset($filters['initTime']) || isset($filters['endTime'])) {
      if (isset($filters['initTime']) && !empty($filters['endTime'])
          && isset($filters['endTime']) && !empty($filters['endTime'])) {
        $query->condition('fecha', [date('Y-m-d 00:00:00', strtotime($filters['initTime'])), date('Y-m-d 23:59:59', strtotime($filters['endTime']))], 'BETWEEN');
      }
      elseif (isset($filters['initTime']) && !empty($filters['initTime'])
              && (!isset($filters['endTime']) || empty($filters['endTime']))) {
        $query->condition('fecha', [date('Y-m-d 00:00:00', strtotime($filters['initTime'])), date('Y-m-d 23:59:59')], 'BETWEEN');
      }
      elseif (isset($filters['endTime']) && !empty($filters['endTime'])
              && (!isset($filters['initTime']) || empty($filters['initTime']))) {
        $query->condition('fecha', [date('Y-m-d 00:00:00') , date('Y-m-d 23:59:59', strtotime($filters['endTime']))], 'BETWEEN');
      }
    }

    if (isset($filters['material']) && !empty($filters['material'])) {
      $group = $query->orConditionGroup()
        ->condition('material', $filters['material'], '=')
        ->condition('material', '', '=');

      $query->condition($group);
      unset($group);
    }

    $query->condition('id_cliente', $filters['nit'], '=');
    $group = $query->orConditionGroup()
        ->condition('tipo_salida', 'debito', '=')
        ->condition('anticipo', '1', '=')
        ->condition('tipo_salida', 'anulado', '=');

    $query->condition($group);
    $query->orderBy('id');
    
    return $query->execute()->fetchAll();
  }

  public function getTotalEnterpriseData(array $filters = [], array $states = []) {
    $query = $this->connection->select(self::OUT, 'cd')
      ->fields('cd', ['razon_social', 'material', 'tipo_pago']);
    $query->addExpression('SUM(cd.valor_total)', 'total');
    $query->addExpression('SUM(cd.volumen)', 'volumen_total');

    if (isset($filters['initTime']) || isset($filters['endTime'])) {
      if (isset($filters['initTime']) && !empty($filters['endTime'])
          && isset($filters['endTime']) && !empty($filters['endTime'])) {
        $query->condition('fecha', [date('Y-m-d 00:00:00', strtotime($filters['initTime'])), date('Y-m-d 23:59:59', strtotime($filters['endTime']))], 'BETWEEN');
      }
      elseif (isset($filters['initTime']) && !empty($filters['initTime'])
              && (!isset($filters['endTime']) || empty($filters['endTime']))) {
        $query->condition('fecha', [date('Y-m-d 00:00:00', strtotime($filters['initTime'])), date('Y-m-d 23:59:59')], 'BETWEEN');
      }
      elseif (isset($filters['endTime']) && !empty($filters['endTime'])
              && (!isset($filters['initTime']) || empty($filters['initTime']))) {
        $query->condition('fecha', [date('Y-m-d 00:00:00') , date('Y-m-d 23:59:59', strtotime($filters['endTime']))], 'BETWEEN');
      }
    }

    if (!isset($filters['initTime']) && !isset($filters['endTime'])) {
      $date = new \DateTime();
      $date->setTimeStamp($this->time->getCurrentTime());
      $date->setTimeZone(new \DateTimeZone('America/Bogota'));
      $subDate = $date->format('Y-m');
      $subDate .= '-1 00:00:00';
      $query->condition('fecha', [$subDate, $date->format('Y-m-d 23:59:59')], 'BETWEEN');
    }

    $query->leftJoin('sand_core_cert_report_history_data', 'scr', 'scr.id_reg = cd.id_remision');
    // $query->fields('scr', ['status']);
    $query->condition('scr.status', 1, '=');

    // $expresion = 'if((select status from `sand_core_cert_report_history_data` where id_reg = cd.id_remision AND status = 1), TRUE, FALSE)';

    $query->groupBy('cd.material');
    $query->groupBy('cd.razon_social');
    $query->groupBy('cd.tipo_pago');

    // $query->addExpression($expresion, 'status');


    $data = $query->execute()->fetchAll();

    return $data;
  }

}