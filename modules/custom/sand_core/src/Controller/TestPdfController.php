<?php

namespace Drupal\sand_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Mpdf\Mpdf;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\Request;

class TestPdfController extends ControllerBase {
  const HISTORY = 'sand_core_cert_report_history_data';
  const OUT = 'core_despachos';
  const CLIENTES = 'core_clientes';
  const MOVEMENTS = 'core_movements';

  public function view() {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $form_class = '\Drupal\sand_core\Form\CertificadoOrigenForm';
    $render = \Drupal::formBuilder()->getForm($form_class);
    unset($render['returnContainer'], $render['wrapper-print']);
    $render['table']['eighth']['codeInput']['#type'] = 'label';
    $render['#attached']['library'][] = 'sand_core/print_pdf';
    $pdfContent = drupal_render($render);
    
    $mpdf = new Mpdf();
    $mpdf->CSSselectMedia = 'print';
    $stylesheet = file_get_contents(drupal_get_path('module', 'sand_core').'/css/pdf.css');
    $mpdf->WriteHTML($stylesheet, 1);
    $mpdf->AddPage();
    $mpdf->WriteHTML($pdfContent);
    return $mpdf->Output();
    /*return [
			'#markup' => "hola mundo"
		];*/
  }

  public function saveType(Request $request) {
    $_SESSION['anmType'] = $_POST['type'];
		$response = ['status' => 'OK'];
		$params = Json::encode($response);
		$response = new Response();
		$response->setContent($params);
    return $response;
  }

  public function saveData(Request $request) {
    $data = $_POST['data'];

    $tempstore = \Drupal::service('tempstore.private');
    $store = $tempstore->get('certificado');
    $sRemision = $tempstore->get('data_remision');
    $store->set('data', [
      'remision' => $sRemision->get('remision'),
      'day' => $data['day'],
      'year' => $data['year'],
      'month' => $data['month'],
      'razonSocial' => strtoupper($data['razonSocial']),
      'material' => strtoupper($data['material']),
      'cantMaterial' => $data['cantidad'],
      'nit' => $data['nit'],
      'cc' => $data['cc'],
      'ce' => $data['ce'],
      'rut' => $data['rut'],
      'type' => $_SESSION['anmType'],
      'document' => $data['document'],
      'rucom' => $data['rucom'],
      'id_cert' => $data['consecutive'],
      'anulado' => $store->get('anulado'),
    ]);

		$response = ['status' => 'OK'];
		$params = Json::encode($response);
		$response = new Response();
		$response->setContent($params);
    return $response;
  }

  public function disableOrder(Request $request) {
    try {
      # get order from history table.
      $data = Json::decode($request->getContent());
      $db = \Drupal::service('database');
      $db->update(self::HISTORY)
        ->fields([
          'status' => 0
        ])
        ->condition('id', $data['order'])
        ->execute();
      unset($db);

      # Get volume, value and client id form out table.
      $db = \Drupal::service('database');
      $remisionInfo = $db->select(self::OUT, 'cd')
        ->fields('cd', ['volumen', 'valor_total', 'id_cliente', 'tipo_pago'])
        ->condition('id_remision', $data['remision'])
        ->execute()->fetchAll()[0];
      unset($db);
      
      # Get of client data.
      $db = \Drupal::service('database');
      $userData = $db->select(self::CLIENTES, 'cc')
        ->fields('cc', ['saldo', 'cantidad_ant', 'en_deuda','razon_social'])
        ->condition('id', $remisionInfo->id_cliente)
        ->execute()->fetchAll()[0];

      # Restore balance.
      $nuevoSaldo = (int)$userData->saldo + (int)$remisionInfo->valor_total;
      $nuevaCantidad = (int)$userData->cantidad_ant + (int)$remisionInfo->volumen;

      # Calculate if user is debt.
      $enDeuda = (int)$userData->saldo > 0 && $userData->en_deuda !== NULL 
        ? 0 
        : $userData->en_deuda;
      
      if ($remisionInfo->tipo_pago == 'debito' || $remisionInfo->tipo_pago == 'credito') {
        $db->update(self::CLIENTES)
        ->fields([
          'saldo' => $nuevoSaldo,
          'cantidad_ant' => $nuevaCantidad,
          'en_deuda' => $enDeuda,
        ])
        ->condition("id", $remisionInfo->id_cliente)
        ->execute();
      }

      unset($db);
      $db = \Drupal::service('database');
      $db->update(self::MOVEMENTS)
      ->condition('id_vale', $data['remision'])
      ->fields([
        'anulado' => 1
      ])
      ->execute();

      unset($db);
      $db = \Drupal::service('database');
      $db->insert(self::MOVEMENTS)
      ->fields([
        'id_cliente' => $remisionInfo->id_cliente,
        'razon_social' => $userData->razon_social,
        'fecha' =>date("Y-m-d"),
        'id_vale' => 0,
        'material' => 'ARENA',
        'volumen' => 0,
        'tipo_salida' => 'anulado',
        'saldo' => $nuevoSaldo,
        'saldo_material' => $nuevaCantidad
      ])->execute();
		  $response = [
        'status' => 'OK',
        '$userData' => $userData,
        '$remisionInfo' => $remisionInfo,
        'remisionInfoExtra' => [
          'type' => gettype($remisionInfo),
        ],
        'valor_monetario' => [
          'saldo' => $userData->saldo,
          'valor_total' => $remisionInfo->valor_total,
          'nuevo_saldo' => $nuevoSaldo,
        ],
        'volumen' => [
          'anticipo_actual' => $userData->cantidad_ant,
          'valor_remision' => $remisionInfo->valor_total,
          'nuevo_valor' => $nuevaCantidad,
        ],
        'en_deuda' => $enDeuda,
        'order' => $data['order'],
        'remision' => $data['remision'],
      ];
      //drupal_set_message('Certificados anulados correctamente.');
    }
    catch (\Exception $e) {
      $response = [
        'status' => 'error',
        'error_message' => $e->getMessage(),
        'error_code' => $e->getCode(),
      ];
      //drupal_set_message(t('Error al anular el certificado @num', ['@num', $_POST['order']]), 'error');
    }

		$params = Json::encode($response);
		$response = new Response();
		$response->setContent($params);
    return $response;
  }

  public function printCert() {
    $tempstore = \Drupal::service('tempstore.private');
    $store = $tempstore->get('certificado');
    $data = $store->get('data');
    $sRemision = $tempstore->get('data_remision');

    $response = [];
    $response = [
      '#theme' => 'cert-print',
      '#cache' =>  [
        'max-age' => 0,
      ],
      '#attached' => [
        'library' => [
          'sand_core/sand_core',
          'sand_core/print_remision'
        ],
      ],
      '#type' => $data['type'],
      '#cc' => $data['cc'],
      '#ce' => $data['ce'],
      '#nit' => $data['nit'],
      '#rut' => $data['rut'],
      '#id_cert' => $data['id_cert'],
      '#day' => $data['day'],
      '#month' => $data['month'],
      '#year' => $data['year'],
      '#material' => $data['material'],
      '#cantMaterial' => $data['cantMaterial'],
      '#razonSocial' => $data['razonSocial'],
      '#document' => $data['document'],
      '#rucom' => $data['rucom'],
      '#remision' => $sRemision->get('remision'),
      '#anulado' => $store->get('anulado'),
      '#sign' => $sRemision->get('sign'),
    ];

    return $response;
  }
}