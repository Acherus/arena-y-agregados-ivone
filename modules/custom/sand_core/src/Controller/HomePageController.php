<?php

namespace Drupal\sand_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class HomePageController extends ControllerBase {

  /**
   * Get user interface to validate if the user is logged or not.
   * 
   * @var AccountInterface $user
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  public function __construct(AccountInterface $user) {
    $this->user = $user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user')
    );
  }

  public function view() {
    return [
      '#theme' => 'home-page',
      '#isAnonymous' => $this->user->isAnonymous(),
      '#userName' => $this->user->getAccountName(),
    ];
  }

}