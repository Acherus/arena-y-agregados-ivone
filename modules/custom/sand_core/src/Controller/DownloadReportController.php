<?php

namespace Drupal\sand_core\Controller;

use Drupal\sand_core\Services\ReportsServices;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class DownloadReportController extends ControllerBase {

  /**
   * Base directory.
   * 
   * @var string $base
   */
  protected $base;

  /**
   * Get report services.
   * 
   * @var ReportsServices $service
   */
  protected $service;

  /**
   * Get module handler.
   * 
   * @var ModuleHandler $moduleHandler
   */
  protected $moduleHandler;

  /**
   * Connection to data base.
   * 
   * @var ReportsServices $connection
   */
  protected $kill;

  /**
   * {@inheritdoc}
   */
  public function __construct(FileSystem $file,
                              ConfigFactory $config,
                              ReportsServices $service,
                              ModuleHandler $moduleHandler,
                              KillSwitch $kill) {
    $systemSchema = $config->get('system.file');
    $this->base = $file->realpath($systemSchema->get('default_scheme') . "://");
    $this->service = $service;
    $this->moduleHandler = $moduleHandler;
    $this->kill = $kill;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('config.factory'),
      $container->get('sand_core.repot_query'),
      $container->get('module_handler'),
      $container->get('page_cache_kill_switch')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function view(Request $request) {
    $this->kill->trigger();
    $img = $this->moduleHandler->getModule('sand_core')->getPath();
    $img .= '/images/sandxlsx.png';
    $filter = $request->query->get('filter');
    $type = $request->query->get('type') !== NULL 
      ? $request->query->get('type')
      : 'xls';
    $toPush = [];

    // Get data with or without filter.
    if (isset($filter) && !empty($filter)) {
      $filter = (array) json_decode(base64_decode(urldecode($filter)));
      $data = $this->service->getData($filter);
    }
    else {
      $data = $this->service->getData();
    }

    $spreadsheet = new Spreadsheet();
    $spreadsheet->getDefaultStyle()->applyFromArray([
      'font' => [
        'name' => 'Cambria'
      ]
    ]);
    $sheet = $spreadsheet->getActiveSheet();

    // Define table headers.
    $headers[] = [
      'FECHA',
      'No. VALE',
      'CLIENTE',
      'TIPO DE MATERIAL',
      'PLACA',
      'M3',
      'TIPO DE PAGO',
      'V.UNITARIO',
      'V. TOTAL',
    ];
    $sheet->fromArray($headers, NULL, 'A4');

    $sheeti = new Drawing();
    $sheeti->setName('name');
    $sheeti->setDescription('description');
    $sheeti->setPath($img);
    $sheeti->setHeight(40);
    $sheeti->setCoordinates("A1");
    $sheeti->setOffsetX(4);
    $sheeti->setOffsetY(6);
    $sheeti->setWorksheet($sheet);
    $sheet->setCellValue('C1', 'CONTROL DE SALIDA DE MATERIALES');
    $sheet->mergeCells('A1:B3');
    $sheet->mergeCells('C1:G3');

    $sheet->setCellValue('H1', 'CODIGO');
    $sheet->setCellValue('H2', 'VERSION');
    $sheet->setCellValue('H3', 'FECHA');
    $sheet->setCellValue('I1', 'FR-PL-15');
    $sheet->setCellValue('I2', '2');
    $sheet->setCellValue('I3', '17/01/17');

    $sheet->getStyle('A1:I4')->applyFromArray(
      [
        'borders' => [
          'allBorders' => [
            'borderStyle' => Border::BORDER_THIN,
              'color' => ['rgb' => '000000'],
            ],
          ]
      ]);


    $sheet->getStyle('C1:I3')
      ->getAlignment()
      ->setVertical(Alignment::VERTICAL_CENTER)
      ->setHorizontal(Alignment::HORIZONTAL_CENTER);

    $totalValue = 0;
    // Load data on export file.
    foreach($data as $register) {
      $fecha = date('d-m-Y', strtotime($register->fecha));
      if ($register->tipo_pago == 'DEBITO' 
          || $register->tipo_pago == 'CREDITO'
          || $register->tipo_pago == 'debito'
          || $register->tipo_pago == 'credito') {
        $vTotal = '';
      }
      else {
        $vTotal = $register->valor_total;
      }

      $toPush[] = [
        $fecha,
        $register->id_remision,
        strtoupper($register->razon_social),
        strtoupper($register->material),
        strtoupper($register->placa),
        $register->volumen,
        strtoupper($register->tipo_pago),
        $register->valor_unidad,
        $register->status == 0 ? 0 : $vTotal,
        $register->status == 0 ? 'ANULADO' : '',
        $register->iva == 1 ? 'Con IVA' : '',
      ];
    }

    $sheet->fromArray($toPush, NULL, 'A5');

    $count = count($toPush) + 4;
    $sheet->getStyle("A5:I$count" )->applyFromArray(
      [
        'borders' => [
          'allBorders' => [
            'borderStyle' => Border::BORDER_THIN,
              'color' => ['rgb' => '000000'],
            ],
          ]
      ]);

    $totalCount = $count+1;
    $sheet->setCellValue("H$totalCount", 'TOTAL');
    $sheet->setCellValue("I$totalCount", 0);
    $sheet->getStyle("H$totalCount:I$totalCount")->applyFromArray(
      [
        'borders' => [
          'allBorders' => [
            'borderStyle' => Border::BORDER_THIN,
              'color' => ['rgb' => '000000'],
            ],
          ]
      ]);
    // Define report format.
    if ($type == 'csv') {
      $writer = new Csv($spreadsheet);
      $writer->setDelimiter(';');
      $fileName = 'reporte_'.time().'.csv';
    }
    else {
      $writer = new Xlsx($spreadsheet);
      $fileName = 'reporte_'.time().'.xlsx';
    }
    $fullName = $this->base.'/reports/'.$fileName;

    $writer->save($fullName);

    // Generate HTTP download file headers.
    $headers = [
      'Content-Type' => 'application/excel; charset=utf-8',
      'Content-Disposition' => 'attachment;filename="'.$fileName.'"',
    ];

    // Return report.
    return new BinaryFileResponse($fullName, 200, $headers, TRUE);
  }

}