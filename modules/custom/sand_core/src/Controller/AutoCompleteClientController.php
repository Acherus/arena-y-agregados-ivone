<?php

namespace Drupal\sand_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AutoCompleteClientController extends ControllerBase {
  
  // Clients table.
  const CLIENTES = 'core_clientes',
        CONDUCTORES = 'core_conductores';

  /**
   * Connection to data base.
   * 
   * @var \Drupal\Core\Database\Connection $db
   */
  protected $db;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection) {
    $this->db = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * 
   */
  public function handleAutocomplete(Request $request) {
    $response = [];

    if ($input = $request->query->get('q')) {
      $query = $this->db->select(self::CLIENTES, 'cc');
      $query->fields('cc', ['id', 'razon_social', 'state']);
      $query->condition('state', 1, '=');

      if (is_numeric($input)) {
        $query->condition('id', '%'. $query->escapeLike($input).'%', 'LIKE');
      }
      else {
        $query->condition('razon_social', '%'. $query->escapeLike($input).'%', 'LIKE');
      }

      $finder = $query->execute()->fetchAll();

      if (!empty($finder)) {
        foreach ($finder as $find) {
          if ($find->state == 1) {
            $response[] = [
              'value' => $find->id,
              'label' => $find->razon_social . ' (' . $find->id . ')'
            ];
          }
        }
      }
    }

    return new JsonResponse($response);
  }

  /**
   * 
   */
  public function handleAutocompletePlaca(Request $request) {
    $response = [];

    if ($input = $request->query->get('q')) {
      $query = $this->db->select(self::CONDUCTORES, 'cc');
      $query->fields('cc', ['id', 'placa', 'nombres_conductor', 'apellidos_conductor']);

      $orGroup = $query->orConditionGroup()
        ->condition('placa', '%'. $query->escapeLike($input).'%', 'LIKE')
        ->condition('nombres_conductor', '%'. $query->escapeLike($input).'%', 'LIKE');

      $query->condition($orGroup);

      $finder = $query->execute()->fetchAll();

      if (!empty($finder)) {
        foreach ($finder as $find) {
          $response[] = [
            'value' => $find->placa,
            'label' => $find->nombres_conductor . ' ' . $find->apellidos_conductor . ' (' . $find->placa . ')'
          ];
        }
      }
    }

    return new JsonResponse($response);
  }

  /**
   * 
   */
  public function handleAutocompleteConductor(Request $request) {
    $response = [];

    if ($input = $request->query->get('q')) {
      $query = $this->db->select(self::CONDUCTORES, 'cc');
      $query->fields('cc', ['id', 'placa', 'nombres_conductor', 'apellidos_conductor']);

      $orGroup = $query->orConditionGroup()
        ->condition('placa', '%'. $query->escapeLike($input).'%', 'LIKE')
        ->condition('nombres_conductor', '%'. $query->escapeLike($input).'%', 'LIKE');

      $query->condition($orGroup);

      $finder = $query->execute()->fetchAll();

      if (!empty($finder)) {
        foreach ($finder as $find) {
          $response[] = [
            'value' => $find->nombres_conductor . ' ' . $find->apellidos_conductor .' - ' . $find->id,
            'label' => $find->nombres_conductor . ' ' . $find->apellidos_conductor . ' (' . $find->placa . ')'
          ];
        }
      }
    }

    return new JsonResponse($response);
  }

}