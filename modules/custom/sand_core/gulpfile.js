const babel = require("gulp-babel"),
      uglify = require("gulp-uglify"),
      rename = require("gulp-rename"),
      changed = require('gulp-changed');
var {
  src,
  dest,
  parallel,
  series,
  watch
} = require('gulp');

function js() {
    const source = './js/expand/libraries/*.js';

    return src(source)
        .pipe(changed(source))
        .pipe(babel(
            {
                presets:["es2015"]
            }
        ))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(dest("js/"));
}

function watchFiles() {
    watch('./js/expand/libraries/*', js)
}

exports.watch = parallel(watchFiles)
exports.default = series(parallel(js))