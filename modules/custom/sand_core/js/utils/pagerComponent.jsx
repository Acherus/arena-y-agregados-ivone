const React = require('react');
const { useState, useEffect } = React;
const PropTypes = require('prop-types');

const PagerComponent = ({totalPages, page, setPage}) => {
  //const [page, setPage] = useState(1);

  // Next page.
  const nextPage = (e) => {
    e.preventDefault();
    setPage(page + 1);
  };

  const renderNextPage = () => (
    page < totalPages &&
    <button onClick={nextPage}
      className='pager--unit-control'>
      Siguiente página
    </button>
  );

  // Previus page.
  const prevPage = (e) => {
    e.preventDefault();
    setPage(page - 1);
  };

  const renderPrevPage = () => (
    page > 1 &&
    <button onClick={prevPage}
      className='pager--unit-control'>
      Anterior página
    </button>
  );

  // First Page.
  const firstPage = (e) => {
    e.preventDefault();
    setPage(1);
  };

  const renderFirstPage = () => (
    page > 1 &&
    <button onClick={firstPage}
      className='pager--unit-control'>
      Primera página
    </button>
  );

  // Last page.
  const lastPage = (e) => {
    e.preventDefault();
    setPage(totalPages);
  };

  const renderLastPage = () => (
    page < totalPages &&
    <button onClick={lastPage}
      className='pager--unit-control'>
      Última página
    </button>
  );

  // Render pages.
  const setFromPage = (e, value) => {
    e.preventDefault();
    setPage(value);
  }

  const renderPage = (index) => (
    <button key={`page-${index}`}
      onClick={(e) => setFromPage(e, index)}
      className={ `pager--page-item ${page == index && 'selected'}` }
      >
        {index}
    </button>
  )

  const renderPages = () => {
    const count = totalPages - page;
    let render = [];

    if (count == 0 && totalPages <= 9) {
      for (let index = 1; index <= totalPages; index++ ) {
        render.push(renderPage(index));
      }
    }

    if (count == 0 && totalPages > 9) {
      for (let index = (page - 9); index <= totalPages; index++) {
        render.push(renderPage(index));
      }
    }

    if (count > 0 && count < 9 && totalPages > 9) {
      for (let index = (page - 9); index <= page; index++) {
        render.push(renderPage(index));
      }
    }

    if (count >= 9 && totalPages > 9 && (page - 9) > 0) {
      for (let index = (page - 9); index <= page; index++) {
        render.push(renderPage(index));
      }
    }

    if (count >= 10 && totalPages > 10 && (page - 9) <= 0 ) {
      for (let index = 1; index <= 10; index++) {
        render.push(renderPage(index));
      }
    }

    return render;
  };

  return (
    <section className='pager-container'>
      <section className='pager-items--container-controls left'>
        { renderFirstPage() }
        { renderPrevPage() }
      </section>

      <section className='pager-items--container-pages'>
        { renderPages() }
      </section>

      <section className='pager-items--container-controls right'>
        { renderNextPage() }
        { renderLastPage() }
      </section>
      
    </section>
  );
}

PagerComponent.propTypes = {
  page: PropTypes.number.isRequired,
  totalPages: PropTypes.number.isRequired,
  setPage: PropTypes.func.isRequired,
}

export default PagerComponent;
