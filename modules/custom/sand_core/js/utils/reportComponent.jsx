const React = require('react');
const Pager = require('./pagerComponent').default;
const TableComponent = require('./tableComponent').default;
const ReportContainer = require('./returnContainer').default;
const DownloadComponent = require('./DonwloadComponent').default;

const ReportComponent = (props) => {
  return (
    <section id={props.id ? props.id : null}>
      {
        props.toReturn &&
        <ReportContainer>
          { props.toReturn }
        </ReportContainer>
      }
      {
        props.filters &&
        props.filters
      }
      {
        props.downloadConfig &&
        <DownloadComponent linksConfig={props.downloadConfig} />
      }
      {
        props.data &&
        props.data?.length > 0 &&
        <TableComponent 
          tableHeaders={props.tableHeaders} 
          data={props.data} 
        />
      }
      { 
        props.totalPages &&
        props.page &&
        <Pager 
          totalPages={props.totalPages} 
          page={props.page}
          setPage={props.setPage}
        />
      }
      { props.children }
    </section>
  );
}

module.exports = ReportComponent;
