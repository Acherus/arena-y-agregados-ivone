import React from "react";

const ReturnContainer = ({children}) => {
  return (
    <>
      <section id='return-container'>
        { children }
      </section>
    </>
  );
}

export default ReturnContainer;