import React, {useState, useEffect} from 'react';

export const Select = React.forwardRef((props, ref) => {
  const {fieldName, optionsUrl, onChangeVal, options, optionsFetch, rest} = props;
  const [selectOptions, setOptions] = useState({});
  const fetchData = async (url, options = {}) => {
    const request = await fetch(url, options);
    const data = await request.json();
    setOptions(data);
    onChangeVal(fieldName, Object.keys(data)[0]);
  }

  if (optionsFetch) {
    useEffect(
      () => {
        try {
          fetchData(optionsUrl);
        }
        catch(e) {
          console.error(e)
        }
      }, []
    );
  }
  else {
    useEffect(() => {setOptions(options)}, []);
  }

  const handleOnChange = (field, value) => {
    props.onChangeVal(field, value)
  }

  return (
    <>
      <select 
        name={fieldName}
        onChange={(e) => handleOnChange(fieldName, e.target.value)}
        {...rest}
        ref={ref}
      >
        {
          Object.keys(selectOptions).map(
            (index, key) => 
              <option key={`${key}-item`} value={index == 'none' ? '' : index }>
                {selectOptions[index]}
              </option>
          )
        }
      </select>
    </>
  )
});

export const Autocomplete = React.forwardRef((props, ref) => {
  const {
    url,
    searchParam,
    onChangeVal,
    required,
    fieldName,
    rest,
  } = props;
  const [optionsList, setOptions] = useState([]);
  const [isOpen, setIsOpen] = useState(false);
  const [searchLabel, setLabel] = useState('');
  const [searchValue, setValue] = useState('');

  const fetchValues = async () => {
    const request = await fetch(`${url}?${new URLSearchParams({[searchParam]: searchLabel})}`);
    const data = await request.json();
    setOptions(data);
    setIsOpen(true);
  };

  const handleOnClick = ({value, label}) => {
    setIsOpen(false);
    setLabel(label);
    setValue(value);
    setOptions([]);
    onChangeVal(fieldName, value);
  }

  const handleOnChange = e => {
    setLabel(e.target.value);
  }

  useEffect(
    () => {
      fetchValues();
    },
    [searchLabel]
  );

  return (
    <>
      <div className='ui-autocomplete'>
        <input
          type='text'
          value={searchLabel}
          onChange={handleOnChange}
          required={required}
          className='ui-autocomplete-input'
          name={fieldName} 
          {...rest} 
          ref={ref}
          autoComplete='off'
        />
        <input type='hidden' value={searchValue} />
        {
          optionsList.length >= 1 &&
          isOpen &&
          <ul className={`ui-autocomplete-list`}>
            {
              optionsList.map(
                (opt, key) => (
                  <li
                    className='ui-autocomplete-item'
                    key={`autocomplete-${key}`}
                    onClick={() => handleOnClick(opt)}
                  >
                    {opt.label}
                  </li>
                )
              )
            }
          </ul>
        }
      </div>
    </>
  );
});

export const Label = ({ text, handleOnClick, className, id }) =>  (
  <>
    <label
      className={className} 
      onClick={ handleOnClick ? handleOnClick : null }
      id={ id ? id : null }
    >
      {text}
    </label>
  </>
);

export const Link = ({ href, text, handleOnClick, className, id }) =>  (
  <>
    <a
      className={className}
      href={ href ? href : null }
      onClick={ handleOnClick ? handleOnClick : null }
      id={ id ? id : null }
    >
      {text}
    </a>
  </>
);