import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { Select, Autocomplete } from './fields';

const FiltersComponent = ({ filters, setPage, setFilterValues }) => {
  const { register, handleSubmit, reset, setValue } = useForm();

  const onSubmit = data => {
    setFilterValues(data);
    setPage(1);
  };

  const clearFilters = (e) => {
    e.preventDefault();
    reset({
      endTime: "",
      initTime: "",
      iva: "",
      material: "",
      nit: "",
      tipo_pago: "",
    });
    setFilterValues(null);
    setPage(1);
  };

  const handleOnChange = (field, value) => {
    setValue(field, value);
  }

  const resolveField = (key, filter) => {
    let field = null;
    switch(filter.type) {
      case 'select':
        field = <Select
            {...filter}
            fieldName={key}
            onChangeVal={handleOnChange}
            {...register(key)}
          />
        break;
      case 'autocomplete':
        field = <Autocomplete
          {...filter}
          fieldName={key}
          onChangeVal={handleOnChange}
          {...register(key)}
        />
        break;
      case 'text':
      case 'number':
      case 'date':
        field = <input
          {...register(key)}
          type={ filter.type }
          name={ key }
          required={ filter.required }
        />;
        break;
      default:
        field = <p>lol</p>
        break;
    }

    return field;
  }

  return (
    <form 
      id='filters-wrapper'
      onSubmit={handleSubmit(onSubmit)}
    >
      {
        Object.keys(filters).map(
          filterName => (
            <div 
              key={filterName} 
              className='filter form-item'
            >
              <label>
                { filters[filterName].label }
              </label>
              {resolveField(filterName, filters[filterName])}              
            </div>
          )
        )
      }
      <input
        type='submit'
        value='Filtrar'
        className='btn-basic list-table filter-it'
      />
      <button
        onClick={(e) => clearFilters(e)}
        className='btn-basic list-table clear-filters'
      >
        Limpiar filtros
      </button>
    </form>
  );
};

export default FiltersComponent;
