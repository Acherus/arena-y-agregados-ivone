import React from "react";

const ReturnLink = ({link, label}) => (
  <>
    <a 
      className='btn-basic list-table'
      href={link}
    >
      {label}
    </a>
  </>
);

export default ReturnLink;