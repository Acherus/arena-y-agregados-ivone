const config = {
  filters: {
    initTime: {
      type: 'date',
      label: 'Fecha de inicio',
      required: false,
    },
    endTime: {
      type: 'date',
      label: 'Fecha de fin',
      required: false,
    },
    nit: {
      type: 'autocomplete',
      label: 'Cliente',
      url: '/autocomplete/clientes',
      searchParam: 'q',
      required: false,
    },
    material: {
      type: 'select',
      label: 'Material',
      optionsFetch: true,
      optionsUrl: '/api/core/get/front-reports/getMaterials',
      required: false,
    },
    tipo_pago: {
      type: 'select',
      label: 'Tipo de pago',
      options: {
        '': '',
        'efectivo': 'Efectivo',
        'credito': 'Crédito',
        'debito': 'Debito',
      },
      required: false,
    },
    iva: {
      type: 'select',
      label: 'IVA',
      options: {
        '': '',
        'sin iva': 'Sin IVA',
        'con iva': 'Con IVA',
      },
      required: false,
    },
  },
  extraLinks: {
    
  }
};

export default config;
