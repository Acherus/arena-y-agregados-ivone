const config = {
  filters: {
    initTime: {
      type: 'date',
      label: 'Fecha de inicio',
      required: true,
    },
    endTime: {
      type: 'date',
      label: 'Fecha de fin',
      required: true,
    },
  }
};

export default config;
