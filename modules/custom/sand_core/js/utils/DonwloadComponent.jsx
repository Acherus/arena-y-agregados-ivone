import React from 'react';
import { Link, Label } from './fields';

const DownloadComponent = ({ linksConfig }) => (
  <>
    <div id='download-wrapper'>
      {
        linksConfig.map(
          (item) => {
            let component = null;
            if (item.type == 'label') {
              component = <Label {...item} />
            }
            else {
              component = <Link {...item} />
            }
            return component;
          }
        )
      }
    </div>
  </>
);

export default DownloadComponent;
