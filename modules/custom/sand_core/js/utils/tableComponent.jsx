const React = require('react');

const TableComponent = ({data, tableHeaders}) => {
  const renderHeaders = () => (
    Object.keys(tableHeaders).map(
      key => (
        <th key={`th-${key}`}>
          { tableHeaders[key] }
        </th>
      )
    )
  );

  const renderData = () => (
    data.map(
      row => (
        <tr key={`tr-${row.id}`}>
          {Object.keys(tableHeaders).map(
            col => (
              <td key={`td-${row.id}-${col}`}>
                { row[col] }
              </td>
            )
          )}
        </tr>
      )
    )
  );

  return (
    <>
      <table>
        <thead>
          <tr>
            { renderHeaders() }
          </tr>
        </thead>
        <tbody>
          { renderData() }
        </tbody>
      </table>
    </>
  );
}

export default TableComponent;
