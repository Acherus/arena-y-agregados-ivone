const React = require('react');
const client = require('react-dom/client');
const { createRoot } = client;

const initApp = (Component, selector) => {
  const container = document.querySelector(selector);
  const root = createRoot(container)
  root.render(<Component/>);
};

module.exports = initApp;
