import React, {useState, useEffect} from "react";

const MessageComponent = ({ message, status = 'notice', setShowMessage }) => {
  const [classes, setClasses] = useState([
    'ui-message',
    status
  ]);

  useEffect(
    () => {
      setTimeout(() => {
        setClasses([...classes, 'hide-message']);
      }, 4000);
      setTimeout(() => {
        setShowMessage(false);
      }, 6000);
    }, []
  );

  return (
    <>
      {
        message &&
        <div className={classes.join(' ')}>
          <span className='ui-message-icon'></span>
          <p>
            {message}
          </p>
        </div>
      }
    </>
  );
}

export default MessageComponent;

export const NOTICE = 'notice';
export const WARNING = 'warning';
export const ERROR = 'error';