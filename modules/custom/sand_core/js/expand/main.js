const el = {
  signIn: 'sign-in',
  form: '.login-form-wrapper',
  close: '.close-pop-up',
  icon: document.querySelector('#icon-header'),
  menu: document.querySelector('#initial-menu'),
  content: document.querySelector('.home-page-non-logged'),
  certOpt: document.querySelectorAll('.select-option')
};

(
  () => {
    "use strict";
        let signInBtn = document.getElementById(el.signIn)
        if (signInBtn) {
          var form = document.querySelector(el.form)
          signInBtn.onclick = () => {
            form.classList.remove('hidden')
            el.icon.classList.add('blur')
            el.menu.classList.add('blur')
            el.content.classList.add('blur')
          }

          let close = document.querySelector(el.close)
          close.onclick = () => {
            form.classList.add('hidden')
            el.icon.classList.remove('blur')
            el.menu.classList.remove('blur')
            el.content.classList.remove('blur')
          }
        }

  }
)();