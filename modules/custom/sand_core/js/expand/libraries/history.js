var disableReg = (order, remision) => {
  jQuery.ajax({
    type: 'POST',
    url: '/disbale-order',
    dataType: 'json',
    data: {order, remision},
    success: (res) => {
      window.location.reload()
    }
  })
};

const clases = {
  links: '.link-disabled',
  close: '.close'
};

const ids = {
  pop: 'pop-disable',
  text: 'dis-description',
  dis: 'dis-cont',
  cancel: 'cancel-dis'
};

(
  () => {
    "use strict";

        window.onload = () => {

          // Save id to disable and unhide popup.
          document.querySelectorAll(clases.links).forEach(el => {
            el.onclick = function () {
              let value = this.id.replace('cert_', '')
              const remision = this.getAttribute('remision');
              window.sessionStorage.setItem('toDelete', value);
              window.sessionStorage.setItem('obtainRemision', remision);
              let replace = document.getElementById(ids.text).innerText.replace('@id', value)
              document.getElementById(ids.text).innerText = replace
              document.getElementById(ids.pop).classList.remove('hidden')
            }
          });

          // Close popup.
          document.getElementById(ids.cancel).onclick = () => {
            document.getElementById(ids.pop).classList.add('hidden')
          }

          document.querySelector(clases.close).onclick = () => {
            document.getElementById(ids.pop).classList.add('hidden')
          }

          // Disable cert.
          document.getElementById(ids.dis).onclick = () => {
            const order = window.sessionStorage.getItem('toDelete')
            const remision = window.sessionStorage.getItem('obtainRemision')
            disableReg(order, remision)
          }

        }
      }
)();