(
  () => {
    let print = document.getElementById('print_page')
    if (print) {
      window.print()
    }

    document.getElementById('ptr-button').onclick = () => {
      window.print()
    }
  }
)()