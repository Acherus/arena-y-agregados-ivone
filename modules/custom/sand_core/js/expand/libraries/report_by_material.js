function downloadReport(ev) {
  ev.preventDefault()

  var type = ev.target.id

  let settings = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  };

  fetch('/rest/session/token', settings)
    .then(resp => {
      return resp.text().then(text => generateReport(text, type));
    })
}

function generateReport(token, type) {
  const initTime = document.getElementById('edit-wrapperfilters-inittime').value
  const endTime = document.getElementById('edit-wrapperfilters-endtime').value
  let url = '/api/core/generate-report?_format=json'
  let settings = {
    method: 'POST',
    body: JSON.stringify({
      type,
      report: 'report_by_material',
      filter: {
        initTime,
        endTime
      }
    }),
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'X-CSRF-Token': token
    }
  };
  fetch(url, settings)
    .then(
      res => {
        res.json().then( response => {
          window.open(response.file, '_blank')
        })
      }
    )
}

// Execute Script.
(
  (Drupal) => {
    "use strict";

    const xls = document.getElementById('xlsx')
    const csv = document.getElementById('csv')

    xls.onclick = downloadReport
    csv.onclick = downloadReport
  }
)(Drupal)