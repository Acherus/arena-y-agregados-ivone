var elements = {
  printCert: 'print-wrapper',
  printCertBtn: 'print-cert-btn',
  certOpt: document.querySelectorAll('.select-option')
};

var saveType = type => {
  jQuery.ajax({
    type: 'POST',
    url: '/save-type',
    dataType: 'json',
    data: {"type": type}
  })
}

var saveData = () => {
  let cantidad = document.getElementById('edit-table-fifteenth-quantityinput').value
  let material = document.getElementById('edit-table-fourteenth-materialinput').value
  let razonSocial = document.getElementById('edit-table-eighteenth-nameoutinput').value
  let nit = document.getElementById('doc-client-nit').checked ? 1 : null
  let cc = document.getElementById('doc-client-cc').checked ? 1 : null
  let ce = document.getElementById('doc-client-ce').checked ? 1 : null
  let rut = document.getElementById('doc-client-rut').checked ? 1 : null
  let cdocument = document.getElementById('client-doc-input').value
  let rucom = document.getElementById('edit-table-twentysecond-rucominput').value
  let day = document.getElementById('day-input').value
  let month = document.getElementById('month-input').value
  let year = document.getElementById('year-input').value
  let consecutive = document.getElementById('consecutive-input').value

  let data = {
    cantidad,
    material,
    razonSocial,
    nit,
    cc,
    ce,
    rut,
    "document": cdocument,
    rucom,
    year,
    day,
    month,
    consecutive
  };

  jQuery.ajax({
    type: 'POST',
    url: '/save-data',
    dataType: 'json',
    data: {data},
    success: () => {
      window.open('/print/cert', '_blank')
    }
  })
}

(
  () => {
    "use strict";

        window.onload = () => {
          let certPage = document.querySelector('.path-cer')

            let printCert = document.getElementById(elements.printCert),
                printCertBtn = document.getElementById(elements.printCertBtn),
                clientDocNit = document.getElementById('doc-client-nit'),
                clientDocCc = document.getElementById('doc-client-cc'),
                clientDocCe = document.getElementById('doc-client-ce'),
                clientDocRut = document.getElementById('doc-client-rut'),
                clientReazonCustomer = document.getElementById('client-reazon-customer'),
                clientReazonDealer = document.getElementById('client-reazon-dealer'),
                clientDocNum = document.getElementById('client-doc-input'),
                toWatch = document.querySelectorAll('.to-watch');

            const validateData = () => {
              let document = clientDocNum.value
              if (
                (clientDocNit.checked == true 
                || clientDocCc.checked == true
                || clientDocCe.checked == true
                || clientDocRut.checked == true)
                && (
                  clientReazonCustomer.classList.contains('selected')
                  || clientReazonDealer.classList.contains('selected')
                )
                && document !== undefined 
                && document !== ''
              ) {
                if (printCert) {
                  printCert.classList.remove('disabled')
                }
                printCertBtn.classList.remove('disabled')
              }
              else {
                if (printCert) {
                  printCert.classList.add('disabled')
                }
                printCertBtn.classList.add('disabled')
              }
            };

            toWatch.forEach((target) => {
              target.addEventListener('input', validateData)
            })

            clientDocNit.onclick = validateData();
            clientDocCc.onclick = validateData();
            clientDocCe.onclick = validateData();
            clientDocRut.onclick = validateData();

            if (printCert) {
              printCert.onclick = (el) => {
                if (!printCert.classList.contains('disabled')) {
                  var dealer = document.getElementById('client-reazon-dealer')
                  let type = dealer.classList.contains('selected') ? 'dealer' : 'customer';
                  saveType(type)
                  saveData()
                }
              };
            }
            
            printCertBtn.onclick = (el) => {
              if (!el.srcElement.classList.contains('disabled')) {
                var dealer = document.getElementById('client-reazon-dealer')
                let type = dealer.classList.contains('selected') ? 'dealer' : 'customer';
                saveType(type)
                saveData()
              }
            };

            if (elements.certOpt) {
              var dealer = document.getElementById('client-reazon-dealer')
              var customer = document.getElementById('client-reazon-customer')
              dealer.onclick = () => {
                dealer.classList.add('selected')
                customer.classList.remove('selected')
                validateData()
                saveType('dealer')
              }
    
              customer.onclick = () => {
                customer.classList.add('selected')
                dealer.classList.remove('selected')
                validateData()
                saveType('customer')
              }
            } 
          
        }
      }
)();