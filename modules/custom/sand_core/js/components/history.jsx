import React, {useState, useEffect} from 'react';
const initApp = require('../utils/init');
const configs = require('../utils/configs/reports/history').default;
const Report = require('../utils/reportComponent');
const CancelCert = require('./cancelCert').default;
const Filters = require('../utils/filtersComponent').default;
import ReturnLink from '../utils/returnComponent';

// Component.
const History = () => {
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState();
  const [data, setData] = useState([]);
  const [popUpIsOpen, setPopUpIsOpen] = useState(false);
  const [popUpData, setPopUpData] = useState({});
  const [filterValues, setFilterValues] = useState();

  const params = {
    page,
  };

  const tableHeaders = { 
    id: 'No.', 
    placa: 'Placa', 
    id_reg: 'Remisión', 
    id_cert: 'Certificado', 
    date_creation: 'Fecha', 
    status: ''
  };
  const remisionPath = '/remision?';
  const certificadoPath = '/cert?';

  const fetchToken = async () => {
    const request = await fetch('/session/token/');
    const token = await request.text();

    return token;
  }

  let headers = {
    'Content-Type': 'application/json'
  };

  const fetchData = async () => {
    const token = await fetchToken();
    headers['X-CSRF-Token'] = token;

    const config = {
      method: 'POST', 
      body: JSON.stringify(filterValues),
      headers,
    };

    const request = await fetch(
      `/api/core/get/data/getHistory?${new URLSearchParams(params)}`,
      config,
    );
    const data = await request.json();

    const newData = data.data.map(
      (row, rowKey) => {
        const remisionURL = `${remisionPath}${new URLSearchParams({
          data: row.data_reg,
          status: row.status,
        })}`;
        const certURL = `${certificadoPath}${new URLSearchParams({
          data: row.data_cert,
          status: row.status,
        })}`;
        const lauchCancelCert = (e, id, cert, key) => {
          e.preventDefault();
          setPopUpData({
            order: id,
            remision: cert,
            key,
          });
          setPopUpIsOpen(true);
        }
        const renderCancelCertAction = () => (
          row.status == 1 ?
          <a href="#" onClick={(e) => lauchCancelCert(e, row.id, row.id_reg, rowKey)}>
            Anular certificados
          </a>
          : null
        );
        return {
          id: row.id,
          placa: row.placa,
          id_reg: <a href={remisionURL}>Remisión {row.id_reg}</a>,
          id_cert: <a href={certURL}>Certificado {row.id_cert}</a>,
          date_creation: row.date_creation,
          status: renderCancelCertAction(),
        }
      }
    );
    setData(newData);
    setTotalPages(data.totalPages);
  }

  useEffect(() => {
    fetchData();
  }, [page, filterValues]);


  const reportProps = {
    page,
    data,
    totalPages,
    setPage,
    tableHeaders,
  };

  return (
    <>
      <Report 
        {...reportProps}
        filters={
          <Filters 
            filters={configs.filters}
            setPage={setPage}
            setFilterValues={setFilterValues}
          />
        }
        toReturn={
          <ReturnLink 
            label='Página principal' 
            link='/'
          />
        }
        id='sand-core-logs'
      >
        <CancelCert 
          popUpIsOpen={popUpIsOpen} 
          popUpData={popUpData} 
          setPopUpIsOpen={setPopUpIsOpen}
          data={data}
          setData={setData}
        />
      </Report>
    </>
  );
}

// Define mount point.
const selector = '.region-content';

initApp(History, selector);
