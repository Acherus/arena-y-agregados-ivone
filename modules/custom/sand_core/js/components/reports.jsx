import React, {useState, useEffect} from 'react';
import Report from '../utils/reportComponent';
import Filters from '../utils/filtersComponent';
import initApp from '../utils/init';
import ReturnLink from '../utils/returnComponent';
const configs = require('../utils/configs/reports/reports').default;

const ReportsPage = () => {
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState();
  const [data, setData] = useState([]);
  const [filterValues, setFilterValues] = useState();

  const ROUTE_CSV = 'buildReportCSV';
  const ROUTE_XLS = 'buildReportXLS';

  const params = {
    page,
  };

  const tableHeaders = { 
    id_despacho: 'NO.',
    id_cliente: 'NIT',
    razon_social: 'Razón social',
    fecha: 'Fecha',
    placa: 'Placa',
    responsable: 'Responsable',
    material: 'Material',
    volumen: 'Cantidad',
    valor_unidad: 'Valor unidad',
    tipo_pago: 'Tipo de pago',
    valor_total: 'Total'
  };

  let headers = {
    'Content-Type': 'application/json'
  };

  const fetchToken = async () => {
    const request = await fetch('/session/token/');
    const token = await request.text();

    return token;
  }

  const fetchPost = async () => {
    const token = await fetchToken();
    headers['X-CSRF-Token'] = token;

    const config = {
      method: 'POST', 
      body: JSON.stringify(filterValues),
      headers,
    };

    const getData = await fetch(`/api/core/get/data/getReports?${new URLSearchParams(params)}`, config);
    const resData = await getData.json();

    setData(
      resData.data.map(
        row => ({
          id_despacho: row.id_despacho,
          id_cliente: row.id_cliente,
          razon_social: row.razon_social,
          fecha: row.fecha,
          placa: row.placa,
          responsable: row.responsable,
          material: row.material,
          volumen: row.volumen,
          valor_unidad: row.valor_unidad,
          tipo_pago: row.tipo_pago,
          valor_total: row.valor_total
        })
      )
    );
    setTotalPages(resData.totalPages);
  }

  const fetchRoute = async (type) => {
    const token = await fetchToken();

    headers['X-CSRF-Token'] = token;

    const config = {
      method: 'POST', 
      body: JSON.stringify(filterValues),
      headers,
    };

    const request = await fetch(`/api/core/get/data/${type}`, config);

    const data = await request.json();

    return data.route;
  };

  const downloadData = async (e, type) => {
    e.preventDefault();
    const route = await fetchRoute(type);
    window.open(route, '_blank');
  }

  const downloadConfig = [
    {
      type: 'label',
      text: 'Descargar reporte:',
      className: 'download-label'
    },
    {
      type: 'link',
      text: 'XLS',
      className: 'btn-basic list-table',
      handleOnClick: (e) => downloadData(e, ROUTE_XLS),
    },
    {
      type: 'link',
      text: 'CSV',
      className: 'btn-basic list-table',
      handleOnClick: (e) => downloadData(e, ROUTE_CSV),
    }
  ];

  useEffect(
    () => {
      fetchPost();
    },
    [page, filterValues]
  );

  const reportProps = {
    page,
    data,
    totalPages,
    setPage,
    tableHeaders,
  };

  return (
    <>
      <Report 
        {...reportProps}
        id='sand-core-reports'
        filters={
          <Filters 
            filters={configs.filters}
            setPage={setPage}
            setFilterValues={setFilterValues}
          />
        }
        downloadConfig= {downloadConfig}
        toReturn={
          <ReturnLink 
            label='Menú de reportes' 
            link='/home-reports'
          />
        }
      />
    </>
  );
}

// Define mount point.
const selector = '.region-content';

// Init React App.
initApp(ReportsPage, selector);