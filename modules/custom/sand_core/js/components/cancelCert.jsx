const React = require('react');
const { useState } = React;
import Message, { ERROR, NOTICE } from '../utils/messageComponent';

const CancelCert = ({popUpIsOpen, popUpData, setPopUpIsOpen, setData, data}) => {
  const [message, setMessage] = useState(null);
  const [messageStatus, setMessageStatus] = useState(null);
  const [showMessage, setShowMessage] = useState(false);
  const closePopUp = (e) => {
    e.preventDefault();
    setPopUpIsOpen(false);
  }

  const cancelCert = (e) => {
    e.preventDefault();
    const configObj = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(popUpData),
    };
    fetch('/disbale-order', configObj)
      .then(response => response.json())
      .then(resData => {
        data[popUpData.key].status = null;
        setData(data);
        setPopUpIsOpen(false);
        if (resData.status == 'OK') {
          setMessage('Certificados anulados correctamente.');
          setMessageStatus(NOTICE);
        }
        else {
          setMessage(`Error al anular el certificado ${popUpData.order}`);
          setMessageStatus(ERROR);
        }
        setShowMessage(true);
      })
      .catch(error => {
        setPopUpIsOpen(false);
        setMessage(`Error al anular el certificado ${popUpData.order}`);
        setMessageStatus(ERROR);
        setShowMessage(true);
      })
  }

  const renderPopUp = () => (
    popUpIsOpen &&
    <div className='pop-wrapper' id='pop-disable'>
      <div className='pop-wrapper' id='pop-form'>
        <div 
          className="close"
          onClick={closePopUp}>
        </div>
        <h3 className='no-link title'>
          Anular certificado
        </h3>
        <p className='no-link'>
          ¿Esta seguro de anular los certificados con el id: {popUpData.order} ?
        </p>

        <div className='btn-wrapper'>
          <button 
            className='btn-basic'
            id="dis-cont"
            onClick={cancelCert}
          >
            CONTINUAR
          </button>
          <button
            id='cancel-dis'
            className='btn-basic'
            onClick={closePopUp}
          >
            CANCELAR
          </button>
        </div>
      </div>
    </div>
  );
  return (
    <>
      { renderPopUp() }
      {
        showMessage &&
        <Message 
          message={message} 
          status={messageStatus} 
          setShowMessage={setShowMessage}
        />
      }
    </>
  );
}

export default CancelCert;
