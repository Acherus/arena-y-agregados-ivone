(
  function ($, Drupal) {
    "use strict";

    $.fn.updateSaldo = function (saldo) {
      const saldoInput = document.querySelector('.form-item-wrapper-saldo input')
      saldoInput.value = saldo
    }
  }
)(jQuery, Drupal);